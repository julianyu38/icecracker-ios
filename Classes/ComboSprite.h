//
//  OptionScene.h
//  Ice_Cracker
//
//  Created by mac on 5/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface ComboSprite : CCSprite {
	CCSprite *_prizeSprite[20];
    
	BOOL _isActive;
	ccTime _appearTime;
	int _comboNumber;
	BOOL _isiPhone;
	
	int _randomNum;
}

+(id) sharedComboSprite;

-(void) initEffect;

-(BOOL) appearEffect:(int)number X:(float)posX Y:(float)posY;

-(void) update:(ccTime)deltaTime;

@property(nonatomic) BOOL _isActive;
@end

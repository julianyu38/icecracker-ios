//
//  OptionScene.m
//  Ice_Cracker
//
//  Created by mac on 5/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ComboSprite.h"
#import "GameData.h"
#import "GameSound.h"
#import "IceSprite.h"
#import "PlayScene.h"

@implementation ComboSprite

@synthesize _isActive = isActive;

#define APPEAR_TIME		(2)

#define PRIZE_WIDTH		140
#define PRIZE_HEIGHT	140

#define PRIZE_X			50
#define PRIZE_Y			0

NSString *Prizes_Name[] = {
	@"prize_01",
	@"prize_02",
	@"prize_03",
	@"prize_04",
	@"prize_05",
	@"prize_06",
	@"prize_07",
	@"prize_08",
	@"prize_09",
	@"prize_10",
	@"prize_11",
	@"prize_12",
	@"prize_13",
	@"prize_14",
	@"prize_15",
	@"prize_16",
	@"prize_17",
	@"prize_18",
	@"prize_19",
	@"prize_20",
};

enum {
	enum_tag_prize = 0,
};

+(id) sharedComboSprite
{
	ComboSprite *sprite = [ComboSprite node];

	// return the scene
	return sprite;
}

-(id) init
{
	if( (self=[super init] )) {
		self.opacity = 0;
		isActive = FALSE;
        _isiPhone = [GameData isiPhone];

		CCSprite *tmp = [[CCSprite alloc] initWithFile:@"play_fire.png"];
		[self setTexture:tmp.texture];
		[self setTextureRect:tmp.textureRect];
		self.opacity = 0;
        [tmp release];
		
		for (int i=0; i<20; i++) {
			_prizeSprite[i] = [[CCSprite alloc] initWithFile:[NSString stringWithFormat:@"%@.png", Prizes_Name[i]]];
			[_prizeSprite[i] setTextureRect:CGRectMake(0, 0, PRIZE_WIDTH, PRIZE_HEIGHT)];
			_prizeSprite[i].position = ccp(self.contentSize.width / 2 + PRIZE_X, self.contentSize.height / 2 + PRIZE_Y);
			[self addChild:_prizeSprite[i] z:enum_tag_prize+i tag:enum_tag_prize+i];
			_prizeSprite[i].opacity = 0;
		}
	}
	return self;
}

-(void) initEffect
{
	self.opacity = 0;
	for (int i=0; i<20; i++) {
		((CCSprite*)[self getChildByTag:enum_tag_prize+i]).opacity = 0;
	}
	isActive = FALSE;
}

-(void) createExplosion:(int)x Y:(int)y
{
	CCParticleSystemQuad *emitter = [[CCParticleSystemQuad alloc] initWithTotalParticles:1500];
	emitter.texture = [[CCTextureCache sharedTextureCache] addImage:@"play_fire.png"];
	emitter.duration = 0.1f;
	emitter.emitterMode = kCCParticleModeRadius;
	
	emitter.startRadius = 30;
	emitter.startRadiusVar = 15;
	emitter.endRadius = 45 + 5 * _comboNumber / 10;
	emitter.endRadiusVar = 20 + 2 * _comboNumber / 10;
	
	emitter.rotatePerSecond = 0;
	emitter.rotatePerSecondVar = 0;
	
	emitter.angle = 90;
	emitter.angleVar = 360;
	
	emitter.life = 0.3f;
	emitter.lifeVar = 0.5f;
	
	emitter.startSpin = 0;
	emitter.startSpinVar = 0;
	emitter.endSpin = 0;
	emitter.endSpinVar = 0;
	
	emitter.startColor = (ccColor4F){1.0f, 0.8f, 0.2f, 1.0f};
	emitter.startColorVar = (ccColor4F){0.0f, 0.2f, 0.0f, 0.2f};
	emitter.endColor = (ccColor4F){1.0f, 0.8f, 0.2f, 0.5f};
	emitter.endColorVar = (ccColor4F){0.0f, 0.2f, 0.0f, 0.2f};
	
    emitter.startSize = 8.0f;
    emitter.startSizeVar = 5.0f;
	emitter.endSize = kCCParticleStartSizeEqualToEndSize;
	
	emitter.emissionRate = emitter.totalParticles / emitter.duration;
	emitter.position = ccp(x, y);
	emitter.posVar = CGPointZero;
	[self addChild:emitter];
	emitter.blendAdditive = NO;
	emitter.autoRemoveOnFinish = YES;
    [emitter release];
}

-(BOOL) appearEffect:(int)number X:(float)posX Y:(float)posY;
{
	isActive = TRUE;
	_comboNumber = number;
	if (_comboNumber > 90) 
        _comboNumber = 90;
	
	self.position = ccp(posX, posY);
	_randomNum = rand() % 20;
	((CCSprite*)[self getChildByTag:enum_tag_prize+_randomNum]).opacity = 255;
	self.opacity = 255;
	self.visible = TRUE;
	[self createExplosion:30 Y:15];
	[self schedule:@selector(update:) interval:TIME_TICKS];
	_appearTime = 0;
	return FALSE;
}

-(void) update:(ccTime)deltaTime
{
	_appearTime += deltaTime;
	if (_appearTime > APPEAR_TIME) {
		isActive = FALSE;
		self.visible = FALSE;
		self.opacity = 0;
		((CCSprite*)[self getChildByTag:enum_tag_prize+_randomNum]).opacity = 0;
		[self unschedule:@selector(update:)];
		[((PlayScene*)[self parent]) addScore:_comboNumber];
		return;
	}
	float opp = 255 - _appearTime * 255.0 / APPEAR_TIME;
	if (opp < 0) {
        opp = 0;
    }
	self.opacity = (int)opp;
	((CCSprite*)[self getChildByTag:enum_tag_prize+_randomNum]).opacity = (int)opp;
}

-(void) isActive:(BOOL)value
{
	isActive = value;
}

-(BOOL) isActive
{
	return isActive;
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	for (int i=0; i<20; i++) {
		[_prizeSprite[i] release];
	}
	[super dealloc];
}
@end

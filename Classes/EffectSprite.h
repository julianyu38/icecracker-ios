//
//  OptionScene.h
//  Insect_Samurai
//
//  Created by mac on 5/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface EffectSprite : CCSprite {
	BOOL isActive;
	ccTime appearTime;
}

+(id) sharedEffectSprite;

-(void) initEffect;

-(BOOL) appearEffect:(int)kindInsect X:(float)posX Y:(float)posY;

-(void) update:(ccTime)deltaTime;

@property(nonatomic) BOOL isActive;
@end

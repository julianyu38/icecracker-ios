//
//  OptionScene.m
//  Insect_Samurai
//
//  Created by mac on 5/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "EffectSprite.h"
#import "GameData.h"
#import "GameSound.h"
#import "InsectSprite.h"

@implementation EffectSprite

@synthesize isActive = isActive;

#define APPEARTIME		(1)

static BOOL isiPhone;

+(id) sharedEffectSprite
{
	EffectSprite *sprite = [EffectSprite node];

	isiPhone = [GameData isiPhone];
	// return the scene
	return sprite;
}

-(id) init
{
	if( (self=[super init] )) {
		self.opacity = 0;
		isActive = FALSE;
		
		CCSprite *tmp = [CCSprite spriteWithFile:@"res/Insects/chr_104.png"];
		[self setTexture:tmp.texture];
		[self setTextureRect:tmp.textureRect];
		self.opacity = 0;

	}
	return self;
}

-(void) initEffect
{
	self.opacity = 0;
	isActive = FALSE;
}

-(BOOL) appearEffect:(int)kindInsect X:(float)posX Y:(float)posY;
{
	isActive = TRUE;
	CCSprite *tmp = [CCSprite spriteWithFile:[NSString stringWithFormat: @"res/Insects/chr_%d04.png", kindInsect]];
	[self setTexture:tmp.texture];
	[self setTextureRect:tmp.textureRect];

	if (!isiPhone) self.scale = SCALE_MIN;
	
	self.position = ccp(posX, posY);

	self.opacity = 255;
	self.visible = TRUE;
	[self schedule:@selector(update:) interval:TIME_TICKS];
	appearTime = 0;
	return FALSE;
}

-(void) update:(ccTime)deltaTime
{
	appearTime += deltaTime;
	if (appearTime > APPEARTIME) {
		isActive = FALSE;
		self.visible = FALSE;
		self.opacity = 0;
		[self unschedule:@selector(update:)];
		return;
	}
	float opp = 255 - appearTime * 255.0 / APPEARTIME;
	if (opp < 0) opp = 0;
	self.opacity = (int)opp;
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}
@end

//
//  GameData.h
//  Ice_Cracker
//
//  Created by mac on 5/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CCSprite.h"

#define SCALE_MAX_IPAD      (768.0 / 320.0)
#define SCALE_MIN_IPAD      (1024.0 / 480.0)
#define SCALE_MAX_IPHONE    (320.0 / 768.0)
#define SCALE_MIN_IPHONE    (480.0 / 1024.0)

#define TIME_TICKS			(1.0f/30.0f)

@interface GameData : NSObject
{
	
}

+ (void) initGameData;

+ (BOOL) isiPhone;
+ (void) setScaleIphone:(CCSprite *)sprite;

+ (BOOL) isMultiSplit;
+ (void) setisMultiSplit:(BOOL)value;

+ (BOOL) getSound;
+ (void) setSound:(BOOL)value;
+ (BOOL) getMusic;
+ (void) setMusic:(BOOL)value;
+ (BOOL) getVibration;
+ (void) setVibration:(BOOL)value;

+ (void) setBestScore:(int)value;
+ (int) getBestScore;

+ (float) getScaleX;
+ (float) getScaleY;
+ (float) getScaleMin;

+ (void) setScale:(float)scaleX scaleY:(float)scaleY scaleMin:(float)scaleMin;
@end

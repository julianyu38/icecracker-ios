//
//  GameData.m
//  Ice_Cracker
//
//  Created by mac on 5/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GameData.h"

@implementation GameData


+(BOOL) isiPhone
{
	if ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)) {
        return TRUE;
    }
	else {
        return FALSE;
    }
}

+ (void) setScaleIphone:(CCSprite *)sprite
{
    sprite.scaleX = SCALE_MIN_IPHONE;
    sprite.scaleY = SCALE_MAX_IPHONE;
}

+(BOOL) isMultiSplit
{
	return [[NSUserDefaults standardUserDefaults] boolForKey:@"isMultiSplit"];
}

+ (void) setisMultiSplit:(BOOL)value
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setBool:value forKey:@"isMultiSplit"];
	[defaults synchronize];	
}

+ (BOOL) getSound
{
	return [[NSUserDefaults standardUserDefaults] boolForKey:@"sound"];
}

+ (void) setSound:(BOOL)value
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setBool:value forKey:@"sound"];
	[defaults synchronize];	
}

+ (BOOL) getMusic
{
	return [[NSUserDefaults standardUserDefaults] boolForKey:@"music"];
}

+ (void) setMusic:(BOOL)value
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setBool:value forKey:@"music"];
	[defaults synchronize];	
}

+ (BOOL) getVibration
{
	return [[NSUserDefaults standardUserDefaults] boolForKey:@"vibration"];
}

+ (void) setVibration:(BOOL)value
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setBool:value forKey:@"vibration"];
	[defaults synchronize];	
}

+ (int) getBestScore
{
    if ([self isMultiSplit]) {
        return [[NSUserDefaults standardUserDefaults] integerForKey:@"bestScoreGame4"];
    }
    else {
        return [[NSUserDefaults standardUserDefaults] integerForKey:@"bestScoreGame"];
    }
}

+ (void) setBestScore:(int)value
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([self isMultiSplit]) {
        [defaults setInteger:value forKey:@"bestScoreGame4"];
    }
    else { 
        [defaults setInteger:value forKey:@"bestScoreGame"];
    }
	[defaults synchronize];
}

+ (float) getScaleX
{
	return [[NSUserDefaults standardUserDefaults] floatForKey:@"scaleX"];
}

+ (float) getScaleY
{
	return [[NSUserDefaults standardUserDefaults] floatForKey:@"scaleY"];
}

+ (float) getScaleMin
{
	return [[NSUserDefaults standardUserDefaults] floatForKey:@"scaleMin"];
}

+ (void) setScale:(float)scaleX scaleY:(float)scaleY scaleMin:(float)scaleMin
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setFloat:scaleX forKey:@"scaleX"];
	[defaults setFloat:scaleY forKey:@"scaleY"];
	[defaults setFloat:scaleMin forKey:@"scaleMin"];
	[defaults synchronize];		
}

+ (void) initGameData
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	BOOL isiPhone = TRUE;
	if ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)) {
        isiPhone = TRUE;
    }
	else {
        isiPhone = FALSE;
    }

	[defaults setBool: isiPhone forKey:@"isiPhone"];

	if ([defaults integerForKey:@"initIceflag"] != 2) {
		[defaults setInteger: 2 forKey:@"initIceflag"];
		[defaults setBool: TRUE forKey:@"sound"];
		[defaults setBool: TRUE forKey:@"music"];
		[defaults setBool: TRUE forKey:@"vibration"];
		[defaults setBool: TRUE forKey:@"isClassic"];
		[defaults setBool: TRUE forKey:@"isMultiSplit"];
		[defaults setBool: TRUE forKey:@"isFrozen"];
		[defaults setBool: TRUE forKey:@"isBomb"];
		[defaults setFloat:1.0 forKey:@"scaleX"];
		[defaults setFloat:1.0 forKey:@"scaleY"];
		[defaults setFloat:1.0 forKey:@"scaleMin"];
		[defaults setInteger:0 forKey:@"bestScoreClassic"];
		[defaults setInteger:0 forKey:@"bestScoreClassicBomb"];
		[defaults setInteger:0 forKey:@"bestScoreClassic4"];
		[defaults setInteger:0 forKey:@"bestScoreClassic4Bomb"];
		[defaults setInteger:0 forKey:@"bestScoreArcade"];
		[defaults setInteger:0 forKey:@"bestScoreArcadeBomb"];
		[defaults setInteger:0 forKey:@"bestScoreArcade4"];
		[defaults setInteger:0 forKey:@"bestScoreArcade4Bomb"];
	}
	[defaults synchronize];
}
@end

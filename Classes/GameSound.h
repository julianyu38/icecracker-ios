//
//  GameData.h
//  Ice_Cracker
//
//  Created by mac on 5/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SimpleAudioEngine.h"
#import <AudioToolbox/AudioServices.h>

@interface GameSound : NSObject
{
	
}
+ (void) effectPenguin;
+ (void) unloadPenguin;
+ (void) effectBtnMenu;
+ (void) unloadBtnMenu;
+ (void) effectBtnClick;
+ (void) unloadBtnClick;
+ (void) effectPopOption;
+ (void) unloadPopOption;
+ (void) effectPushOption;
+ (void) unloadPushOption;
+ (void) effectThrowIce:(int)value;
+ (void) unloadThrowIce;
+ (void) effectIceSlashed:(int)value;
+ (void) unloadIceSlashed;
+ (ALuint) effectSwardHiss;
+ (void) unloadSwardHiss;
+ (void) stopSwardHiss:(ALuint)swardHissID;
+ (void) effectGameEnd;
+ (void) unloadGameEnd;
+ (void) effectKnifeSlashed;
+ (void) unloadKnifeSlashed;
+ (void) effectKnife;
+ (void) unloadKnife;
+ (void) effectBeep;
+ (void) unloadBeep;
+ (void) playMainBG;
+ (void) playGameBG;
+ (void) stopBG;
+ (void) vib:(int)length;
@end

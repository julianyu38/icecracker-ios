//
//  GameData.m
//  Ice_Cracker
//
//  Created by mac on 5/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GameSound.h"
#import "SimpleAudioEngine.h"
#import <AudioToolbox/AudioServices.h>
#import "GameData.h"

@implementation GameSound

+ (void) effectPenguin
{
	if ([GameData getSound]) {
		[[SimpleAudioEngine sharedEngine] playEffect:@"penguin.mp3"];
    }
}

+ (void) unloadPenguin
{
	if ([GameData getSound]) {
		[[SimpleAudioEngine sharedEngine] unloadEffect:@"penguin.mp3"];
    }
}

+ (void) effectBtnMenu
{
	if ([GameData getSound]) {
		[[SimpleAudioEngine sharedEngine] playEffect:@"menu_Btn.mp3"];
    }
}

+ (void) unloadBtnMenu
{
	if ([GameData getSound]) {
		[[SimpleAudioEngine sharedEngine] unloadEffect:@"menu_Btn.mp3"];
    }
}

+ (void) effectBtnClick
{
	if ([GameData getSound]) {
		[[SimpleAudioEngine sharedEngine] playEffect:@"btn_Click.mp3"];
    }
}

+ (void) unloadBtnClick
{
	if ([GameData getSound]) {
		[[SimpleAudioEngine sharedEngine] unloadEffect:@"btn_Click.mp3"];
    }
}

+ (void) effectPopOption
{
	if ([GameData getSound]) {
		[[SimpleAudioEngine sharedEngine] playEffect:@"pop_Btn.mp3"];
    }
}

+ (void) unloadPopOption
{
	if ([GameData getSound]) {
		[[SimpleAudioEngine sharedEngine] unloadEffect:@"pop_Btn.mp3"];
    }
}

+ (void) effectPushOption
{
	if ([GameData getSound]) {
		[[SimpleAudioEngine sharedEngine] playEffect:@"push_Btn.mp3"];
    }
}

+ (void) unloadPushOption
{
	if ([GameData getSound]) {
		[[SimpleAudioEngine sharedEngine] unloadEffect:@"push_Btn.mp3"];
    }
}

+ (void) effectThrowIce:(int)value
{
    int num = 1;
	switch (value) {
        case 3:	
        case 5:	
            num = 1;	
            break;
        case 0:
        case 1:	
        case 2:	
            num = 2;	
            break;
        case 4:
        case 6:	
        case 7:	
        case 8:	
            num = 3;	
            break;
		default:
            num = 2;
            break;
	}
	if ([GameData getSound]) {
		[[SimpleAudioEngine sharedEngine] playEffect:[NSString stringWithFormat:@"ice_throw%d.mp3", num]];
    }
}

+ (void) unloadThrowIce
{
	if ([GameData getSound]) {
		[[SimpleAudioEngine sharedEngine] unloadEffect:@"ice_throw1.mp3"];
		[[SimpleAudioEngine sharedEngine] unloadEffect:@"ice_throw2.mp3"];
		[[SimpleAudioEngine sharedEngine] unloadEffect:@"ice_throw3.mp3"];
    }
}

+ (void) effectIceSlashed:(int)value
{
	int num = 1;
	switch (value) {
		case 0:
		case 3:
            num = 5;	
            break;
		case 1:
		case 7:
            num = 1;	
            break;
		case 2:
		case 8:
            num = 4;	
            break;
		case 4:
		case 6:
            num = 2;	
            break;
		case 5:
            num = 3;	
            break;
		default:
            num = 4;
            break;
	}
	if ([GameData getSound]) {
		[[SimpleAudioEngine sharedEngine] playEffect:[NSString stringWithFormat:@"ice_%d.mp3", num]];
    }
}

+ (void) unloadIceSlashed
{
	if ([GameData getSound]) {
		[[SimpleAudioEngine sharedEngine] unloadEffect:@"ice_1.mp3"];
		[[SimpleAudioEngine sharedEngine] unloadEffect:@"ice_2.mp3"];
		[[SimpleAudioEngine sharedEngine] unloadEffect:@"ice_3.mp3"];
		[[SimpleAudioEngine sharedEngine] unloadEffect:@"ice_4.mp3"];
		[[SimpleAudioEngine sharedEngine] unloadEffect:@"ice_5.mp3"];
    }
}

+ (ALuint) effectSwardHiss
{
	if ([GameData getSound]) {
		ALuint unit = [[SimpleAudioEngine sharedEngine] playEffect:@"swardhiss.mp3"];
		return unit;
    }

	return (ALuint)nil;
}

+ (void) stopSwardHiss:(ALuint)swardHissID
{
	if ([GameData getSound]) {
		[[SimpleAudioEngine sharedEngine] stopEffect:swardHissID];
    }
}

+ (void) unloadSwardHiss
{
	if ([GameData getSound]) {
		[[SimpleAudioEngine sharedEngine] unloadEffect:@"swardhiss.mp3"];
    }
}

+ (void) effectGameEnd
{
	if ([GameData getSound]) {
        [[SimpleAudioEngine sharedEngine] playEffect:@"game_End.mp3"];
	}
}

+ (void) unloadGameEnd
{
	if ([GameData getSound]) {
        [[SimpleAudioEngine sharedEngine] unloadEffect:@"game_End.mp3"];
	}
}

+ (void) effectKnifeSlashed
{
	if ([GameData getSound]) {
		[[SimpleAudioEngine sharedEngine] playEffect:@"knife_Slashed.mp3"];
    }
}

+ (void) unloadKnifeSlashed
{
	if ([GameData getSound]) {
		[[SimpleAudioEngine sharedEngine] unloadEffect:@"knife_Slashed.mp3"];
    }
}

+ (void) effectKnife
{
	int i = rand() % 3;
	if ([GameData getSound]) {
		[[SimpleAudioEngine sharedEngine] playEffect:[NSString stringWithFormat:@"knife_%d.mp3", i]];
    }
}

+ (void) unloadKnife
{
	if ([GameData getSound]) {
		[[SimpleAudioEngine sharedEngine] unloadEffect:@"knife_0.mp3"];
		[[SimpleAudioEngine sharedEngine] unloadEffect:@"knife_1.mp3"];
		[[SimpleAudioEngine sharedEngine] unloadEffect:@"knife_2.mp3"];
    }
}

+ (void) effectBeep
{
	if ([GameData getSound]) {
		[[SimpleAudioEngine sharedEngine] playEffect:@"beep.mp3"];
    }
}

+ (void) unloadBeep
{
	if ([GameData getSound]) {
		[[SimpleAudioEngine sharedEngine] unloadEffect:@"beep.mp3"];
    }
}

+ (void) playMainBG
{
	if ([GameData getMusic]) {
		[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"menu_Background.mp3"];
        [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:.5f];
	}
}

+ (void) playGameBG
{
	if ([GameData getMusic]) {
		[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"game_Background.mp3"];
        [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:.5f];
	}
}

+ (void) stopBG
{
	if ([[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying] == TRUE) {
		[[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    }
}

+ (void) vib:(int)length
{
	if ([GameData getVibration]) {
		AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
	}
}

@end

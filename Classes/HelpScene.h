//
//  HelpScene.h
//  Ice_Cracker
//
//  Created by mac on 5/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface HelpScene : CCLayer {
    CCSprite *_background;
    CCSprite *_textPanel;
    CCSprite *_backBtnSprite;
    CCSprite *_scrollBtnSprite;
    CCSprite *_scrollBarSprite;
    CCSprite *_textSprite;
    
	BOOL _isiPhone;
	BOOL _isPressed;
	BOOL _clickState;
	BOOL _movingDirection;

	int _delta;
	int _deltaPrev;
	CGPoint _touchPrev;
	CGPoint _touchFirst;
	CGPoint _currentTouches;
	int _velocity;
}

+(id) scene;

@end

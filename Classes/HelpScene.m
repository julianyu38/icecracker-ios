//
//  HelpScene.m
//  Ice_Cracker
//
//  Created by mac on 5/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "HelpScene.h"
#import "GameData.h"
#import "GameSound.h"


@implementation HelpScene

enum {
	enum_tag_help_background = 0,
	enum_tag_help_panel,
	enum_tag_text,
	enum_tag_help_back_btn,
	enum_tag_scroll_bar,
	enum_tag_scroll_btn,
};

enum {
	enum_click_none = 0,
	enum_click_back,
	enum_click_text_panel,
	enum_click_scroll_btn,
	enum_click_up_btn,
	enum_click_down_btn,
};

enum {
	enum_moving_none = 0,
	enum_moving_up,
	enum_moving_down,
};

#define BTN_BACK_X              241.0f
#define BTN_BACK_Y              53.0f
#define BTN_BACK_R              40.0f

#define TEXT_PANEL_X            0.0f
#define TEXT_PANEL_Y            16.0f

#define SCROLL_BAR_X			128.0f
#define SCROLL_BAR_Y			6.0f
#define SCROLL_BAR_WIDTH		16.0f
#define SCROLL_BAR_HEIGHT		154.0f

#define SCROLL_BTN_START_Y		55.0f
#define SCROLL_BTN_END_Y		-43.0f
#define SCROLL_BTN_VARIENCE     (SCROLL_BTN_START_Y - SCROLL_BTN_END_Y)
#define SCROLL_BTN_MOVE_STEP	0.0f
#define SCROLL_BTN_JUMP_STEP	50.0f
#define SCROLL_BTN_WIDTH		160.0f
#define SCROLL_BTN_HEIGHT		50.0f

#define SCROLL_TEXT_X			5.0f
#define SCROLL_TEXT_Y			5.0f
#define SCROLL_TEXT_START_Y     130.0f
#define SCROLL_TEXT_END_Y		0.0f
#define SCROLL_TEXT_VARIENCE	(SCROLL_TEXT_START_Y - SCROLL_TEXT_END_Y)
#define SCROLL_TEXT_MOVE_STEP	70.0f
#define SCROLL_TEXT_WIDTH		238.0f
#define SCROLL_TEXT_HEIGHT      154.0f

#define PRESS_OPACITY           255
#define RELEASE_OPACITY         200
#define OPTION_OPACITY          80

+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	HelpScene *layer = [HelpScene node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

-(id) init
{
	if( (self=[super init] )) {
		
		self.isTouchEnabled = YES;
		
		_isiPhone = [GameData isiPhone];
		_clickState = NO;
		_isPressed = NO;
		
		// ask director the the window size
		CGSize size = [[CCDirector sharedDirector] winSize];

		_background = [[CCSprite alloc] initWithFile:@"help_background@2x.png"];
		_textPanel = [[CCSprite alloc] initWithFile:@"panel_background@2x.png"];
		_backBtnSprite = [[CCSprite alloc] initWithFile:@"panel_btn_back_normal@2x.png"];
		_scrollBtnSprite = [[CCSprite alloc] initWithFile:@"help_scroll_point@2x.png"];
		_scrollBarSprite = [[CCSprite alloc] initWithFile:@"help_scroll_bar@2x.png"];
		
		_textSprite = [[CCSprite alloc] initWithFile:@"help_text_all@2x.png"];
		
		if (_isiPhone) {
			_background.position = ccp(size.width / 2, size.height / 2);
            [GameData setScaleIphone:_background];
			_textPanel.position = ccp(size.width / 2 + TEXT_PANEL_X, size.height / 2 + TEXT_PANEL_Y);
            [GameData setScaleIphone:_textPanel];
			_backBtnSprite.position = ccp(BTN_BACK_X, BTN_BACK_Y);
            _backBtnSprite.scale = .5f;
			_scrollBarSprite.position = ccp(size.width / 2 + SCROLL_BAR_X, size.height / 2 + SCROLL_BAR_Y);
            [GameData setScaleIphone:_scrollBarSprite];
			_scrollBtnSprite.position = ccp(size.width / 2 + SCROLL_BAR_X, size.height / 2 + SCROLL_BTN_START_Y);
            [GameData setScaleIphone:_scrollBtnSprite];
			_textSprite.position = ccp(size.width / 2 + SCROLL_TEXT_X, size.height / 2 + SCROLL_TEXT_Y);
            [GameData setScaleIphone:_textSprite];
		}
		else {
			_background.position = ccp(size.width / 2, size.height / 2);
			_textPanel.position = ccp(size.width / 2 + (float)SCALE_MIN_IPAD * (float)TEXT_PANEL_X, size.height / 2 + (float)SCALE_MIN_IPAD * (float)TEXT_PANEL_Y);
			_backBtnSprite.position = ccp((float)SCALE_MIN_IPAD * (float)BTN_BACK_X, (float)SCALE_MIN_IPAD * (float)BTN_BACK_Y);
			_scrollBarSprite.position = ccp(size.width / 2 + (float)SCALE_MIN_IPAD * (float)SCROLL_BAR_X, size.height / 2 + (float)SCALE_MIN_IPAD * (float)SCROLL_BAR_Y);
			_scrollBtnSprite.position = ccp(size.width / 2 + (float)SCALE_MIN_IPAD * (float)SCROLL_BAR_X, size.height / 2 + (float)SCALE_MIN_IPAD * (float)SCROLL_BTN_START_Y);
			_textSprite.position = ccp(size.width / 2 + (float)SCALE_MIN_IPAD * (float)SCROLL_TEXT_X, size.height / 2 + (float)SCALE_MIN_IPAD * (float)SCROLL_TEXT_Y);
		}

		[self addChild:_background z:enum_tag_help_background tag:enum_tag_help_background];
		[self addChild:_textPanel z:enum_tag_help_panel tag:enum_tag_help_panel];
		[self addChild:_backBtnSprite z:enum_tag_help_back_btn tag:enum_tag_help_back_btn];
		[self addChild:_scrollBarSprite z:enum_tag_scroll_bar tag:enum_tag_scroll_bar];
		[self addChild:_scrollBtnSprite z:enum_tag_scroll_btn tag:enum_tag_scroll_btn];
		[self addChild:_textSprite z:enum_tag_text tag:enum_tag_text];
		
		_scrollBtnSprite.opacity = RELEASE_OPACITY;
		_backBtnSprite.opacity = RELEASE_OPACITY;
        [_textSprite setTextureRect:CGRectMake(0, (float)SCALE_MIN_IPAD * 0, (float)SCALE_MIN_IPAD * SCROLL_TEXT_WIDTH, (float)SCALE_MIN_IPAD * SCROLL_TEXT_HEIGHT)];
		_delta = 0;
	}
	return self;
}

- (int) getClick:(int)x Y:(int)y
{
	int dx, dy;
	CCSprite *textSprite = (CCSprite*)[self getChildByTag:enum_tag_text];
	CCSprite *scrollBarSprite = (CCSprite*)[self getChildByTag:enum_tag_scroll_bar];
	CCSprite *scrollBtnSprite = (CCSprite*)[self getChildByTag:enum_tag_scroll_btn];
	
	if (_isiPhone) {
		dx = x - BTN_BACK_X;
		dy = y - BTN_BACK_Y;
		if (sqrt(dx * dx + dy * dy) < BTN_BACK_R) {
            return enum_click_back;
        }
		
		dx = x - textSprite.position.x;
		dy = y - textSprite.position.y;
		if (-SCROLL_TEXT_WIDTH / 2 < dx && dx < SCROLL_TEXT_WIDTH / 2 && 
			-SCROLL_TEXT_HEIGHT / 2 < dy && dy < SCROLL_TEXT_HEIGHT / 2) {
            return enum_click_text_panel;
        }
		
		dx = x - scrollBtnSprite.position.x;
		dy = y - scrollBtnSprite.position.y;
		if (-SCROLL_BTN_WIDTH / 2 < dx && dx < SCROLL_BTN_WIDTH / 2 && 
			-SCROLL_BTN_HEIGHT / 2 < dy && dy < SCROLL_BTN_HEIGHT / 2) {
            return enum_click_scroll_btn;
        }
		
		dx = x - scrollBarSprite.position.x;
		dy = y - scrollBarSprite.position.y;
		if (y > scrollBtnSprite.position.y && -SCROLL_BAR_WIDTH / 2 < dx && dx < SCROLL_BAR_WIDTH / 2 && 
			-SCROLL_BAR_HEIGHT / 2 < dy && dy < SCROLL_BAR_HEIGHT / 2) {
            return enum_click_up_btn;
        }
		
		dx = x - scrollBarSprite.position.x;
		dy = y - scrollBarSprite.position.y;
		if (y < scrollBtnSprite.position.y && -SCROLL_BAR_WIDTH / 2 < dx && dx < SCROLL_BAR_WIDTH / 2 && 
			-SCROLL_BAR_HEIGHT / 2 < dy && dy < SCROLL_BAR_HEIGHT / 2) {
            return enum_click_down_btn;
        }
	}
	else {
		dx = x - (float)SCALE_MIN_IPAD * (float)BTN_BACK_X;
		dy = y - (float)SCALE_MIN_IPAD * (float)BTN_BACK_Y;
		if (sqrt(dx * dx + dy * dy) < (float)SCALE_MIN_IPAD * (float)BTN_BACK_R) {
            return enum_click_back;
        }
		
		dx = (float)(x - textSprite.position.x) / (float)SCALE_MIN_IPAD;
		dy = (float)(y - textSprite.position.y) / (float)SCALE_MIN_IPAD;
		if (-SCROLL_TEXT_WIDTH / 2 < dx && dx < SCROLL_TEXT_WIDTH / 2 && 
			-SCROLL_TEXT_HEIGHT / 2 < dy && dy < SCROLL_TEXT_HEIGHT / 2) {
            return enum_click_text_panel;
        }
		
		dx = (float)(x - scrollBtnSprite.position.x) / (float)SCALE_MIN_IPAD;
		dy = (float)(y - scrollBtnSprite.position.y) / (float)SCALE_MIN_IPAD;
		if (-SCROLL_BTN_WIDTH / 2 < dx && dx < SCROLL_BTN_WIDTH / 2 && 
			-SCROLL_BTN_HEIGHT / 2 < dy && dy < SCROLL_BTN_HEIGHT / 2) {
            return enum_click_scroll_btn;
        }
		
		dx = (float)(x - scrollBarSprite.position.x) / (float)SCALE_MIN_IPAD;
		dy = (float)(y - scrollBarSprite.position.y) / (float)SCALE_MIN_IPAD;
		if (y > scrollBtnSprite.position.y && -SCROLL_BAR_WIDTH / 2 < dx && dx < SCROLL_BAR_WIDTH / 2 && 
			-SCROLL_BAR_HEIGHT / 2 < dy && dy < SCROLL_BAR_HEIGHT / 2) {
            return enum_click_up_btn;
        }
		
		if (y < scrollBtnSprite.position.y && -SCROLL_BAR_WIDTH / 2 < dx && dx < SCROLL_BAR_WIDTH / 2 && 
			-SCROLL_BAR_HEIGHT / 2 < dy && dy < SCROLL_BAR_HEIGHT / 2) {
            return enum_click_down_btn;
        }
	}
	return enum_click_none;
}

- (BOOL) jump
{
	switch ([self getClick:_currentTouches.x Y:_currentTouches.y]) {
		case enum_click_up_btn:	
            _movingDirection = enum_moving_up;	
            break;
		case enum_click_down_btn:	
            _movingDirection= enum_moving_down;	
            break;
		default:	
            _movingDirection = enum_moving_none;			
            break;
	}
	if (_movingDirection == enum_moving_up) {
		_delta -= SCROLL_TEXT_MOVE_STEP;
	}
	else if (_movingDirection == enum_moving_down) {
		_delta += SCROLL_TEXT_MOVE_STEP;
	}
	else {
        return FALSE;
    }

	if (_delta < 0) {
        _delta = 0;
    }
	if (_delta > SCROLL_TEXT_VARIENCE) {
        _delta = SCROLL_TEXT_VARIENCE;
    }
	
	CGSize size = [[CCDirector sharedDirector] winSize];
	CCSprite *scrollBtnSprite = (CCSprite*)[self getChildByTag:enum_tag_scroll_btn];
	CCSprite *scrollTextSprite = (CCSprite*)[self getChildByTag:enum_tag_text];
    [scrollTextSprite setTextureRect:CGRectMake(0, (float)SCALE_MIN_IPAD * _delta, (float)SCALE_MIN_IPAD * SCROLL_TEXT_WIDTH, (float)SCALE_MIN_IPAD * SCROLL_TEXT_HEIGHT)];
        
	if (_isiPhone) {
		scrollBtnSprite.position = ccp(scrollBtnSprite.position.x, size.height / 2 + SCROLL_BTN_START_Y - _delta * SCROLL_BTN_VARIENCE / SCROLL_TEXT_VARIENCE);
    }
	else { 
		scrollBtnSprite.position = ccp(scrollBtnSprite.position.x, size.height / 2 + (float)SCALE_MIN_IPAD * (float)(SCROLL_BTN_START_Y - _delta * SCROLL_BTN_VARIENCE / SCROLL_TEXT_VARIENCE));
    }
	return TRUE;
}

- (void) jumping:(ccTime)dd
{
	[self jump];
}

- (void) startJumping
{
	[self schedule:@selector(jumping:) interval:TIME_TICKS];
}

- (void) flowing:(ccTime)dd
{
	_delta += _velocity;
	if (_velocity > 0) {
		if (_isiPhone) {
            _velocity -= 1;
        }
		else {
            _velocity -= 1.0f * (float)SCALE_MIN_IPAD;
        }

		if (_velocity < 0) {
            _velocity = 0;
        }
	}
	else {
		if (_isiPhone) {
            _velocity += 1;
        }
		else {
            _velocity += 1.0f * (float)SCALE_MIN_IPAD;
        }

		if (_velocity > 0) {
            _velocity = 0;
        }
	}
	
	if (_delta < 0) {
        _delta = 0;
    }
	if (_delta > SCROLL_TEXT_VARIENCE) {
        _delta = SCROLL_TEXT_VARIENCE;
    }
	
	CGSize size = [[CCDirector sharedDirector] winSize];
	CCSprite *scrollBtnSprite = (CCSprite*)[self getChildByTag:enum_tag_scroll_btn];
	CCSprite *scrollTextSprite = (CCSprite*)[self getChildByTag:enum_tag_text];
    [scrollTextSprite setTextureRect:CGRectMake(0, (float)SCALE_MIN_IPAD * _delta, (float)SCALE_MIN_IPAD * SCROLL_TEXT_WIDTH, (float)SCALE_MIN_IPAD * SCROLL_TEXT_HEIGHT)];

	if (_isiPhone) {
		scrollBtnSprite.position = ccp(scrollBtnSprite.position.x, size.height / 2 + SCROLL_BTN_START_Y - _delta * SCROLL_BTN_VARIENCE / SCROLL_TEXT_VARIENCE);
    }
	else { 
		scrollBtnSprite.position = ccp(scrollBtnSprite.position.x, size.height / 2 + (float)SCALE_MIN_IPAD * (float)(SCROLL_BTN_START_Y - _delta * SCROLL_BTN_VARIENCE / SCROLL_TEXT_VARIENCE));
    }
	
	if (_velocity == 0 || _delta == 0 || _delta == SCROLL_TEXT_VARIENCE) {
        [self unschedule:@selector(flowing:)];
    }
 }

- (void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	if (_clickState) {
        return;
    }
    
	UITouch *touch = [touches anyObject];
	CGPoint location = [touch locationInView:[touch view]];
	location = [[CCDirector sharedDirector] convertToGL:location];
	
	_currentTouches = location;
	int touchSelect = [self getClick:location.x Y:location.y];
	
	if (touchSelect == enum_click_none) {
        return;
    }
	
	_clickState = touchSelect;
	
	_touchPrev = location;
	_touchFirst = location;
	_deltaPrev = _delta;
	
	@try{	[self unschedule:@selector(jumping:)];	}
	@catch(NSException *e) {}
	@try{	[self unschedule:@selector(flowing:)];	}
	@catch(NSException *e) {}
	switch (_clickState) {
		case enum_click_back:
			[GameSound effectBtnClick];
            CCSprite *tmp = [[CCSprite alloc] initWithFile:@"panel_btn_back_press@2x.png"];
            [((CCSprite*)[self getChildByTag:enum_tag_help_back_btn]) setTexture:tmp.texture];
            [((CCSprite*)[self getChildByTag:enum_tag_help_back_btn]) setTextureRect:tmp.textureRect];
            [tmp release];
			((CCSprite*)[self getChildByTag:enum_tag_help_back_btn]).opacity = PRESS_OPACITY;
			break;
		case enum_click_text_panel:
			break;
		case enum_click_scroll_btn:
			((CCSprite*)[self getChildByTag:enum_tag_scroll_btn]).opacity = PRESS_OPACITY;
			break;
		case enum_click_up_btn:
			_movingDirection = enum_moving_up;
			[self runAction:[CCSequence actions:
							 [CCCallFunc actionWithTarget:self selector:@selector(jump)],
							 [CCDelayTime actionWithDuration:.2],
							 [CCCallFunc actionWithTarget:self selector:@selector(startJumping)],
							 nil]];
			break;
		case enum_click_down_btn:
			_movingDirection = enum_moving_down;
			[self runAction:[CCSequence actions:
							 [CCCallFunc actionWithTarget:self selector:@selector(jump)],
							 [CCDelayTime actionWithDuration:.2],
							 [CCCallFunc actionWithTarget:self selector:@selector(startJumping)],
							 nil]];
			break;
		default: return;
	}
	_isPressed = TRUE;
}

- (void) ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	if (!_isPressed) {
        return;
    }

	CGSize size = [[CCDirector sharedDirector] winSize];
	UITouch *touch = [touches anyObject];
	CGPoint location = [touch locationInView:[touch view]];
	location = [[CCDirector sharedDirector] convertToGL:location];
	
	_currentTouches = location;
	int touchSelect = [self getClick:location.x Y:location.y];
	
	switch (_clickState) {
		case enum_click_back:
			if (touchSelect == enum_click_back)	{
                ((CCSprite*)[self getChildByTag:enum_tag_help_back_btn]).opacity = PRESS_OPACITY;
            }
			else {
                ((CCSprite*)[self getChildByTag:enum_tag_help_back_btn]).opacity = RELEASE_OPACITY;
            }
			break;
		case enum_click_text_panel:
			if (_isiPhone) {
				CCSprite *scrollBtnSprite = (CCSprite*)[self getChildByTag:enum_tag_scroll_btn];
				CCSprite *scrollTextSprite = (CCSprite*)[self getChildByTag:enum_tag_text];
				_delta = _deltaPrev - (_touchFirst.y - location.y);
				if (_delta < 0) {
                    _delta = 0;
                }
				if (_delta > SCROLL_TEXT_VARIENCE) {
                    _delta = SCROLL_TEXT_VARIENCE;
                }
				
				[scrollTextSprite setTextureRect:CGRectMake(0, (float)SCALE_MIN_IPAD * _delta, (float)SCALE_MIN_IPAD * SCROLL_TEXT_WIDTH, (float)SCALE_MIN_IPAD * SCROLL_TEXT_HEIGHT)];
				scrollBtnSprite.position = ccp(scrollBtnSprite.position.x, size.height / 2 + SCROLL_BTN_START_Y - _delta * SCROLL_BTN_VARIENCE / SCROLL_TEXT_VARIENCE);
				_velocity = (float)(location.y - _touchPrev.y);
			}
			else {
				CCSprite *scrollBtnSprite = (CCSprite*)[self getChildByTag:enum_tag_scroll_btn];
				CCSprite *scrollTextSprite = (CCSprite*)[self getChildByTag:enum_tag_text];
				_delta = _deltaPrev - (float)(_touchFirst.y - location.y) / (float)SCALE_MIN_IPAD;
				if (_delta < 0) {
                    _delta = 0;
                }
				if (_delta > SCROLL_TEXT_VARIENCE) {
                    _delta = SCROLL_TEXT_VARIENCE;
                }
				
				[scrollTextSprite setTextureRect:CGRectMake(0, (float)SCALE_MIN_IPAD * _delta, (float)SCALE_MIN_IPAD * SCROLL_TEXT_WIDTH, (float)SCALE_MIN_IPAD * SCROLL_TEXT_HEIGHT)];
				scrollBtnSprite.position = ccp(scrollBtnSprite.position.x, size.height / 2 + (float)SCALE_MIN_IPAD * (float)(SCROLL_BTN_START_Y - _delta * SCROLL_BTN_VARIENCE / SCROLL_TEXT_VARIENCE));
				_velocity = (float)(location.y - _touchPrev.y) / (float)SCALE_MIN_IPAD;
			}
			break;
		case enum_click_scroll_btn:
			if (_isiPhone) {
				CCSprite *scrollBtnSprite = (CCSprite*)[self getChildByTag:enum_tag_scroll_btn];
				CCSprite *scrollTextSprite = (CCSprite*)[self getChildByTag:enum_tag_text];
				_delta = _deltaPrev + (float)(_touchFirst.y - location.y) * SCROLL_TEXT_VARIENCE / SCROLL_BTN_VARIENCE;
				if (_delta < 0) {
                    _delta = 0;
                }
				if (_delta > SCROLL_TEXT_VARIENCE) {
                    _delta = SCROLL_TEXT_VARIENCE;
                }
				
				[scrollTextSprite setTextureRect:CGRectMake(0, (float)SCALE_MIN_IPAD * _delta, (float)SCALE_MIN_IPAD * SCROLL_TEXT_WIDTH, (float)SCALE_MIN_IPAD * SCROLL_TEXT_HEIGHT)];
				scrollBtnSprite.position = ccp(scrollBtnSprite.position.x, size.height / 2 + SCROLL_BTN_START_Y - _delta * SCROLL_BTN_VARIENCE / SCROLL_TEXT_VARIENCE);
				_velocity = -(float)(location.y - _touchPrev.y) * SCROLL_TEXT_VARIENCE / SCROLL_BTN_VARIENCE;
			}
			else {
				CCSprite *scrollBtnSprite = (CCSprite*)[self getChildByTag:enum_tag_scroll_btn];
				CCSprite *scrollTextSprite = (CCSprite*)[self getChildByTag:enum_tag_text];
				_delta = _deltaPrev + (float)(_touchFirst.y - location.y) * SCROLL_TEXT_VARIENCE / SCROLL_BTN_VARIENCE / (float)SCALE_MIN_IPAD;
				if (_delta < 0) {
                    _delta = 0;
                }
				if (_delta > SCROLL_TEXT_VARIENCE) {
                    _delta = SCROLL_TEXT_VARIENCE;
                }
				
				[scrollTextSprite setTextureRect:CGRectMake(0, (float)SCALE_MIN_IPAD * _delta, (float)SCALE_MIN_IPAD * SCROLL_TEXT_WIDTH, (float)SCALE_MIN_IPAD * SCROLL_TEXT_HEIGHT)];
				scrollBtnSprite.position = ccp(scrollBtnSprite.position.x, size.height / 2 + (float)SCALE_MIN_IPAD * (float)(SCROLL_BTN_START_Y - _delta * SCROLL_BTN_VARIENCE / SCROLL_TEXT_VARIENCE));
				_velocity = -(float)(location.y - _touchPrev.y) * SCROLL_TEXT_VARIENCE / SCROLL_BTN_VARIENCE / (float)SCALE_MIN_IPAD;
			}
			break;
		case enum_click_up_btn:
			if (touchSelect == enum_click_up_btn) {
                _movingDirection = enum_moving_up;
            }
			else if (touchSelect == enum_click_down_btn) {
                _movingDirection = enum_moving_down;
            }
			else {
                _movingDirection = enum_moving_none;
            }
			break;
		case enum_click_down_btn:
			if (touchSelect == enum_click_up_btn) {
                _movingDirection = enum_moving_up;
            }
			else if (touchSelect == enum_click_down_btn) {
                _movingDirection = enum_moving_down;
            }
			else {
                _movingDirection = enum_moving_none;
            }
			break;
		default: 
            return;
	}
	_touchPrev = location;
}

- (void) ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{	
	if (!_isPressed) {
        return;
    }

	UITouch *touch = [touches anyObject];
	CGPoint location = [touch locationInView:[touch view]];
	location = [[CCDirector sharedDirector] convertToGL:location];
	
	_currentTouches = location;
	int touchSelect = [self getClick:location.x Y:location.y];
	CCSprite *tmp;
    
	switch (_clickState) {
		case enum_click_back:
            tmp = [[CCSprite alloc] initWithFile:@"panel_btn_back_normal@2x.png"];
            [((CCSprite*)[self getChildByTag:enum_tag_help_back_btn]) setTexture:tmp.texture];
            [((CCSprite*)[self getChildByTag:enum_tag_help_back_btn]) setTextureRect:tmp.textureRect];
			((CCSprite*)[self getChildByTag:enum_tag_help_back_btn]).opacity = RELEASE_OPACITY;
            [tmp release];
			if (touchSelect == enum_click_back) {
				[[CCDirector sharedDirector] popSceneWithTransition:[CCTransitionFade class] duration:.6f];
            }
			break;
		case enum_click_text_panel:
		case enum_click_scroll_btn:
			[self schedule:@selector(flowing:) interval:TIME_TICKS];
			((CCSprite*)[self getChildByTag:enum_tag_scroll_btn]).opacity = RELEASE_OPACITY;
			break;
		case enum_click_up_btn:
		case enum_click_down_btn:
			_movingDirection = enum_moving_none;
			[self unschedule:@selector(jumping:)];
			break;
		default: 
            return;
	}
	_clickState = enum_click_none;
	_isPressed = NO;
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
    [GameSound unloadBtnClick];
    
    [_background release];
    [_textPanel release];
    [_backBtnSprite release];
    [_scrollBtnSprite release];
    [_scrollBarSprite release];
    [_textSprite release];
	[super dealloc];
}
@end

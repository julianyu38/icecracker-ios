//
//  OptionScene.h
//  Ice_Cracker
//
//  Created by mac on 5/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "GameSound.h"

enum {
	enum_split_none = 0,
	enum_first_split = 1,
	enum_second_first_split = 2,
	enum_second_second_split = 4,
};

#define Y_START		-160
#define MAX_ICE		11

@interface IceSprite : CCSprite {
	BOOL _isActive;
	int _iceKind;
	int _iceType;

	BOOL _isSplit;
	BOOL _isSplitFirst;
	BOOL _isSplitSecond;
	BOOL _isiPhone;
	BOOL _isMultiSplit;
	
	CCSprite *_oneSprite;
	CCSprite *_firstPartSprite;
	CCSprite *_firstPartFirstSprite;
	CCSprite *_firstPartSecondSprite;
	CCSprite *_secondPartSprite;
	CCSprite *_secondPartFirstSprite;
	CCSprite *_secondPartSecondSprite;
    CCSprite *_penguin1Oh1Sprite;
    CCSprite *_penguin1Oh2Sprite;
    CCSprite *_penguin1Oh3Sprite;
    CCSprite *_penguin2Oh1Sprite;
    CCSprite *_penguin2Oh2Sprite;
    CCSprite *_penguin2Oh3Sprite;
	
    int _penguin1SlashCount;
    int _penguin2SlashCount;
    
	float _vX;
	float _vY;
	float _vRotate;
	float _pX;
	float _pY;
	
	float _rotateOne;
	float _rotateFirstPart;
	float _rotateSecondPart;
	
	float _firstPartVX;
	float _firstPartVY;
	float _firstPartFirstVX;
	float _firstPartFirstVY;
	float _firstPartSecondVX;
	float _firstPartSecondVY;
	float _secondPartVX;
	float _secondPartVY;
	float _secondPartFirstVX;
	float _secondPartFirstVY;
	float _secondPartSecondVX;
	float _secondPartSecondVY;
	
	float _firstPartPX;
	float _firstPartPY;
	float _firstPartFirstPX;
	float _firstPartFirstPY;
	float _firstPartSecondPX;
	float _firstPartSecondPY;
	float _secondPartPX;
	float _secondPartPY;
	float _secondPartFirstPX;
	float _secondPartFirstPY;
	float _secondPartSecondPX;
	float _secondPartSecondPY;
	
	ccTime _flyingTime;
	ccTime _splitTime;
	ccTime _delayTime;
}

+(id) sharedIceSprite;

-(void) initIce;

-(void) settingOpacity:(int)value;

-(void) setIceKind:(int)kind;

-(void) setPosition:(int)posX Y:(int)posY;

-(void) setVelocity:(int)vx VY:(int)vy;

-(int) splitAction:(float)posX1 Y1:(float)posY1 X2:(int)posX2 Y2:(int)posY2;

-(void) startFlying:(float)delay;
-(void) startFlyingWithPenguin:(float)delay rate:(int)penguinRate;

-(void) update:(ccTime)deltaTime;
-(BOOL) isFullSplit;

-(float) getWidth:(int)width Height:(int)height Alpha:(float)alpha;
-(float) getHeight:(int)width Height:(int)height Alpha:(float)alpha;

@property(nonatomic) int _iceKind;
@property(nonatomic) BOOL _isSplit;
@property(nonatomic) BOOL _isActive;

@end

//
//  OptionScene.m
//  Ice_Cracker
//
//  Created by mac on 5/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "IceSprite.h"
#import "PlayScene.h"
#import "GameData.h"
#import "GameSound.h"

@implementation IceSprite

@synthesize _isSplit = isSplit;
@synthesize _isActive = isActive;
@synthesize _iceKind = iceKind;

enum ENUM_SPRITE_PANELS {
	enum_tag_one_ice = 0,
	enum_tag_first_part,
	enum_tag_second_part,
	enum_tag_first_part_first,
	enum_tag_first_part_second,
	enum_tag_second_part_first,
	enum_tag_second_part_second,
    enum_tag_penguin1_oh_1,
    enum_tag_penguin1_oh_2,
    enum_tag_penguin1_oh_3,
    enum_tag_penguin2_oh_1,
    enum_tag_penguin2_oh_2,
    enum_tag_penguin2_oh_3,
};

NSString *Ices_Name[] = {
	@"ice1",
	@"ice2",
	@"ice3",
	@"ice4",
	@"ice5",
	@"ice6",
	@"ice7",
	@"ice8",
	@"ice9",
	@"ice10",
	@"penguin1",
	@"penguin2",
};

#define FLYING_HEIGHT_MIN	6
#define FLYING_HEIGHT_MAX	9
#define GRAVITY				0.4f

#define MAX_STEP			30

#define PI					(3.1415927f)

+(id) sharedIceSprite
{
	IceSprite *sprite = [IceSprite node];

	// return the scene
	return sprite;
}

-(id) init
{
	if( (self=[super init] )) {
		self.visible = FALSE;
		isSplit = FALSE;
		isActive = FALSE;
		_isiPhone = [GameData isiPhone];
		_isMultiSplit = [GameData isMultiSplit];
		
		_oneSprite = [[CCSprite alloc] initWithFile:@"ice1@2x.png"];
		[self addChild:_oneSprite z:enum_tag_one_ice tag:enum_tag_one_ice];
		
		_firstPartSprite = [[CCSprite alloc] initWithFile:@"ice1_2_pieces_part1@2x.png"];
		[self addChild:_firstPartSprite z:enum_tag_first_part tag:enum_tag_first_part];
		
		_firstPartFirstSprite = [[CCSprite alloc] initWithFile:@"ice1_4_pieces_part1@2x.png"];
		[self addChild:_firstPartFirstSprite z:enum_tag_first_part_first tag:enum_tag_first_part_first];
		
		_firstPartSecondSprite = [[CCSprite alloc] initWithFile:@"ice1_4_pieces_part2@2x.png"];
		[self addChild:_firstPartSecondSprite z:enum_tag_first_part_second tag:enum_tag_first_part_second];
		
		_secondPartSprite = [[CCSprite alloc] initWithFile:@"ice1_2_pieces_part2@2x.png"];
		[self addChild:_secondPartSprite z:enum_tag_second_part tag:enum_tag_second_part];
		
		_secondPartFirstSprite = [[CCSprite alloc] initWithFile:@"ice1_4_pieces_part3@2x.png"];
		[self addChild:_secondPartFirstSprite z:enum_tag_second_part_first tag:enum_tag_second_part_first];
		
		_secondPartSecondSprite = [[CCSprite alloc] initWithFile:@"ice1_4_pieces_part4@2x.png"];
		[self addChild:_secondPartSecondSprite z:enum_tag_second_part_second tag:enum_tag_second_part_second];

        _penguin1Oh1Sprite = [[CCSprite alloc] initWithFile:@"penguin1_Oh1@2x.png"];
        [self addChild:_penguin1Oh1Sprite z:enum_tag_penguin1_oh_1 tag:enum_tag_penguin1_oh_1];

        _penguin1Oh2Sprite = [[CCSprite alloc] initWithFile:@"penguin1_Oh2@2x.png"];
        [self addChild:_penguin1Oh2Sprite z:enum_tag_penguin1_oh_2 tag:enum_tag_penguin1_oh_2];

        _penguin1Oh3Sprite = [[CCSprite alloc] initWithFile:@"penguin1_Oh3@2x.png"];
        [self addChild:_penguin1Oh3Sprite z:enum_tag_penguin1_oh_3 tag:enum_tag_penguin1_oh_3];

		_penguin2Oh1Sprite = [[CCSprite alloc] initWithFile:@"penguin2_Oh1@2x.png"];
        [self addChild:_penguin2Oh1Sprite z:enum_tag_penguin2_oh_1 tag:enum_tag_penguin2_oh_1];

        _penguin2Oh2Sprite = [[CCSprite alloc] initWithFile:@"penguin2_Oh2@2x.png"];
        [self addChild:_penguin2Oh2Sprite z:enum_tag_penguin2_oh_2 tag:enum_tag_penguin2_oh_2];

        _penguin2Oh3Sprite = [[CCSprite alloc] initWithFile:@"penguin2_Oh3@2x.png"];
        [self addChild:_penguin2Oh3Sprite z:enum_tag_penguin2_oh_3 tag:enum_tag_penguin2_oh_3];

		[self initIce];
	}
	return self;
}

-(void) initIce
{
	if (_isiPhone) {
        self.position = ccp(0, Y_START);
    }
	else {
        self.position = ccp(0, (float)SCALE_MIN_IPAD * (float)Y_START);
    }
	_oneSprite.position = CGPointZero;
	_firstPartSprite.position = CGPointZero;
	_firstPartFirstSprite.position = CGPointZero;
	_firstPartSecondSprite.position = CGPointZero;
	_secondPartSprite.position = CGPointZero;
	_secondPartFirstSprite.position = CGPointZero;
	_secondPartSecondSprite.position = CGPointZero;
    _penguin1Oh1Sprite.position = CGPointZero;
    _penguin1Oh2Sprite.position = CGPointZero;
    _penguin1Oh3Sprite.position = CGPointZero;
    _penguin2Oh1Sprite.position = CGPointZero;
    _penguin2Oh2Sprite.position = CGPointZero;
    _penguin2Oh3Sprite.position = CGPointZero;
	isActive = NO;
	isSplit = NO;
	_isSplitFirst = NO;
	_isSplitSecond = NO;
	_oneSprite.visible = NO;
	_firstPartSprite.visible = NO;
	_secondPartSprite.visible = NO;
	_firstPartFirstSprite.visible = NO;
	_firstPartSecondSprite.visible = NO;
	_secondPartFirstSprite.visible = NO;
	_secondPartSecondSprite.visible = NO;
    _penguin1Oh1Sprite.visible = NO;
    _penguin1Oh2Sprite.visible = NO;
    _penguin1Oh3Sprite.visible = NO;
    _penguin2Oh1Sprite.visible = NO;
    _penguin2Oh2Sprite.visible = NO;
    _penguin2Oh3Sprite.visible = NO;
	_vY = 0;
	_vX = 0;
    _penguin1SlashCount = 0;
    _penguin2SlashCount = 0;
}

-(void) settingOpacity:(int)value
{
	self.opacity = value;
	_oneSprite.opacity = value;
	_firstPartSprite.opacity = value;
	_firstPartFirstSprite.opacity = value;
	_firstPartSecondSprite.opacity = value;
	_secondPartSprite.opacity = value;
	_secondPartFirstSprite.opacity = value;
	_secondPartSecondSprite.opacity = value;
}

- (int) getRandomValue:(int)val_1 Last:(int)val_2
{
	int a;
	if (val_2 - val_1 != 0) {
        a = rand() % (val_2 - val_1);
    }
	else {
        a = 1;
    }
	return val_1 + a;
}

-(void) setIceKind:(int)kind
{
	iceKind = kind;
	
	_firstPartVX = 0;
	_firstPartVY = 0;
	_secondPartVX = 0;
	_secondPartVY = 0;
	
	_firstPartPX = 0;
	_firstPartPY = 0;
	_secondPartPX = 0;
	_secondPartPY = 0;

}

-(void) setPosition:(int)posX Y:(int)posY
{
	_pX = posX;
	_pY = posY;
	self.position = ccp(posX, posY);
}

-(void) setVelocity:(int)vx VY:(int)vy
{
	_vX = vx;
	_vY = vy;
}

-(void) scheduler
{
	_flyingTime = 0;
	_splitTime = _flyingTime;
	[self schedule:@selector(update:) interval:TIME_TICKS];
}

-(void) startFlying:(float)delay
{
	if (isActive) {
        return;
    }

	[self initIce];
	isActive = TRUE;
	float px2, height, time;
	iceKind = [self getRandomValue:0 Last:MAX_ICE-2];
    
	CCSprite *tmp1;
    tmp1 = [[CCSprite alloc] initWithFile:[NSString stringWithFormat:@"%@@2x.png", Ices_Name[iceKind]]];
	[_oneSprite setTexture:tmp1.texture];
	[_oneSprite setTextureRect:tmp1.textureRect];
	_oneSprite.opacity = 255;
	_oneSprite.visible = TRUE;
    [tmp1 release];

	CCSprite *tmp2;
    tmp2 = [[CCSprite alloc] initWithFile:[NSString stringWithFormat:@"%@_2_pieces_part1@2x.png", Ices_Name[iceKind]]];
    [_firstPartSprite setTexture:tmp2.texture];
	[_firstPartSprite setTextureRect:tmp2.textureRect];
	_firstPartSprite.opacity = 255;
	_firstPartSprite.visible = FALSE;
    [tmp2 release];
	
    CCSprite *tmp5;
    tmp5 = [[CCSprite alloc] initWithFile:[NSString stringWithFormat:@"%@_4_pieces_part1@2x.png", Ices_Name[iceKind]]];
	[_firstPartFirstSprite setTexture:tmp5.texture];
	[_firstPartFirstSprite setTextureRect:tmp5.textureRect];
	_firstPartFirstSprite.opacity = 255;
	_firstPartFirstSprite.visible = FALSE;
    [tmp5 release];

	CCSprite *tmp6;
    tmp6 = [[CCSprite alloc] initWithFile:[NSString stringWithFormat:@"%@_4_pieces_part2@2x.png", Ices_Name[iceKind]]];
	[_firstPartSecondSprite setTexture:tmp6.texture];
	[_firstPartSecondSprite setTextureRect:tmp6.textureRect];
	_firstPartSecondSprite.opacity = 255;
	_firstPartSecondSprite.visible = FALSE;
    [tmp6 release];

	CCSprite *tmp3;
    tmp3 = [[CCSprite alloc] initWithFile:[NSString stringWithFormat:@"%@_2_pieces_part2@2x.png", Ices_Name[iceKind]]];
	[_secondPartSprite setTexture:tmp3.texture];
	[_secondPartSprite setTextureRect:tmp3.textureRect];
	_secondPartSprite.opacity = 255;
	_secondPartSprite.visible = FALSE;
    [tmp3 release];

	CCSprite *tmp7;
    tmp7 = [[CCSprite alloc] initWithFile:[NSString stringWithFormat:@"%@_4_pieces_part3@2x.png", Ices_Name[iceKind]]];
	[_secondPartFirstSprite setTexture:tmp7.texture];
	[_secondPartFirstSprite setTextureRect:tmp7.textureRect];
	_secondPartFirstSprite.opacity = 255;
	_secondPartFirstSprite.visible = FALSE;
    [tmp7 release];
	
    CCSprite *tmp8;
    tmp8 = [[CCSprite alloc] initWithFile:[NSString stringWithFormat:@"%@_4_pieces_part4@2x.png", Ices_Name[iceKind]]];
	[_secondPartSecondSprite setTexture:tmp8.texture];
	[_secondPartSecondSprite setTextureRect:tmp8.textureRect];
	_secondPartSecondSprite.opacity = 255;
	_secondPartSecondSprite.visible = FALSE;
    [tmp8 release];
	
	CGSize size = [[CCDirector sharedDirector] winSize];
	_pX = [self getRandomValue:size.width / 10 Last:size.width * 9 / 10];
	if (_isiPhone) {
        _pY = Y_START;
    }
	else {
        _pY = (float)SCALE_MIN_IPAD * (float)Y_START;
    }
	px2 = [self getRandomValue:size.width / 10 Last:size.width * 9 / 10];
	height = [self getRandomValue:size.height * FLYING_HEIGHT_MIN / 10 Last:size.height * FLYING_HEIGHT_MAX / 10];
    if (_isiPhone) {
        _vY = sqrt((height - _pY) * 2.0 * GRAVITY);
        time = _vY / (float)GRAVITY;
    }
    else {
        _vY = sqrt((height - _pY) * 2.0 * GRAVITY*2.);
        time = _vY / (float)GRAVITY*2.;
    }
	_vX = (px2 - _pX) / time / 2;
	_vRotate = [self getRandomValue:-10 Last:10];
	self.rotation = 0;
	
	float delayT = (float)[self getRandomValue:0 Last:(int)(delay * 100)] / 100.0f;
	isSplit = FALSE;
	self.visible = TRUE;
	[self runAction:[CCSequence actions:
					 [CCDelayTime actionWithDuration:delayT],
					 [CCCallFunc actionWithTarget:self selector:@selector(scheduler)],
					 nil]];
}

-(void) startFlyingWithPenguin:(float)delay rate:(int)penguinRate
{
	if (isActive) {
        return;
    }

	[self initIce];
	isActive = TRUE;
	float px2, height, time;
	iceKind = [self getRandomValue:0 Last:MAX_ICE + penguinRate];
	if (iceKind > MAX_ICE) {
        iceKind = MAX_ICE;
    }

	CCSprite *tmp1;
	if (iceKind != MAX_ICE && iceKind != MAX_ICE-1) {
        tmp1 = [[CCSprite alloc] initWithFile:[NSString stringWithFormat:@"%@@2x.png", Ices_Name[iceKind]]];
		[_oneSprite setTexture:tmp1.texture];
		[_oneSprite setTextureRect:tmp1.textureRect];
        [tmp1 release];

		CCSprite *tmp2;
        tmp2 = [[CCSprite alloc] initWithFile:[NSString stringWithFormat:@"%@_2_pieces_part1@2x.png", Ices_Name[iceKind]]];
		[_firstPartSprite setTexture:tmp2.texture];
		[_firstPartSprite setTextureRect:tmp2.textureRect];
        [tmp2 release];

		CCSprite *tmp5;
        tmp5 = [[CCSprite alloc] initWithFile:[NSString stringWithFormat:@"%@_4_pieces_part1@2x.png", Ices_Name[iceKind]]];
		[_firstPartFirstSprite setTexture:tmp5.texture];
		[_firstPartFirstSprite setTextureRect:tmp5.textureRect];
        [tmp5 release];

		CCSprite *tmp6;
        tmp6 = [[CCSprite alloc] initWithFile:[NSString stringWithFormat:@"%@_4_pieces_part2@2x.png", Ices_Name[iceKind]]];
		[_firstPartSecondSprite setTexture:tmp6.texture];
		[_firstPartSecondSprite setTextureRect:tmp6.textureRect];
        [tmp6 release];

		CCSprite *tmp3;
        tmp3 = [[CCSprite alloc] initWithFile:[NSString stringWithFormat:@"%@_2_pieces_part2@2x.png", Ices_Name[iceKind]]];
		[_secondPartSprite setTexture:tmp3.texture];
		[_secondPartSprite setTextureRect:tmp3.textureRect];
        [tmp3 release];

		CCSprite *tmp7;
        tmp7 = [[CCSprite alloc] initWithFile:[NSString stringWithFormat:@"%@_4_pieces_part3@2x.png", Ices_Name[iceKind]]];
		[_secondPartFirstSprite setTexture:tmp7.texture];
		[_secondPartFirstSprite setTextureRect:tmp7.textureRect];
        [tmp7 release];

		CCSprite *tmp8;
        tmp8 = [[CCSprite alloc] initWithFile:[NSString stringWithFormat:@"%@_4_pieces_part4@2x.png", Ices_Name[iceKind]]];
		[_secondPartSecondSprite setTexture:tmp8.texture];
		[_secondPartSecondSprite setTextureRect:tmp8.textureRect];
        [tmp8 release];

    }
	else {
        tmp1 = [[CCSprite alloc] initWithFile:[NSString stringWithFormat:@"%@@2x.png", Ices_Name[iceKind]]];
		[_oneSprite setTexture:tmp1.texture];
		[_oneSprite setTextureRect:tmp1.textureRect];
        [tmp1 release];
	}

	_oneSprite.visible = TRUE;
	_firstPartSprite.visible = FALSE;
	_firstPartFirstSprite.visible = FALSE;
	_firstPartSecondSprite.visible = FALSE;
	_secondPartSprite.visible = FALSE;
	_secondPartSecondSprite.visible = FALSE;
	_secondPartSecondSprite.visible = FALSE;
	_oneSprite.opacity = 255;
	_firstPartSprite.opacity = 255;
	_firstPartFirstSprite.opacity = 255;
	_firstPartSecondSprite.opacity = 255;
	_secondPartSprite.opacity = 255;
	_secondPartFirstSprite.opacity = 255;
	_secondPartSecondSprite.opacity = 255;

	CGSize size = [[CCDirector sharedDirector] winSize];
	_pX = [self getRandomValue:size.width / 10 Last:size.width * 9 / 10];
	if (_isiPhone) {
        _pY = Y_START;
    }
	else {
        _pY = (float)SCALE_MIN_IPAD * (float)Y_START;
    }
	px2 = [self getRandomValue:size.width / 10 Last:size.width * 9 / 10];
	height = [self getRandomValue:size.height * FLYING_HEIGHT_MIN / 10 Last:size.height * FLYING_HEIGHT_MAX / 10];
    if (_isiPhone) {
        _vY = sqrt((height - _pY) * 2.0 * GRAVITY);
        time = _vY / (float)GRAVITY;
    }
    else {
        _vY = sqrt((height - _pY) * 2.0 * GRAVITY*2.);
        time = _vY / (float)GRAVITY*2.;
    }
	_vX = (px2 - _pX) / time / 2;
	_vRotate = [self getRandomValue:-10 Last:10];
	
	_delayTime = (float)[self getRandomValue:0 Last:(int)(delay * 100)] / 100.0f;
	if (iceKind == MAX_ICE && iceKind == MAX_ICE-1) {
        _delayTime = .6f;
    }
	isSplit = FALSE;
	self.visible = TRUE;
	[self runAction:[CCSequence actions:
					 [CCDelayTime actionWithDuration:_delayTime],
					 [CCCallFunc actionWithTarget:self selector:@selector(scheduler)],
					 nil]];
}

-(void) createExplosion:(int)x Y:(int)y
{
	CCParticleSystemQuad *emitter = [[CCParticleSystemQuad alloc] initWithTotalParticles:[self getRandomValue:5 Last:10]];
	
	switch (iceKind) {
		case 4:	
            emitter.texture = [[CCTextureCache sharedTextureCache] addImage:@"play_drop_1@2x.png"];	
            break;
		case 6:	
            emitter.texture = [[CCTextureCache sharedTextureCache] addImage:@"play_drop_2@2x.png"];	
            break;
		case 2:
		case 8:	
            emitter.texture = [[CCTextureCache sharedTextureCache] addImage:@"play_drop_3@2x.png"];		
            break;
		case 0:
		case 3:	
            emitter.texture = [[CCTextureCache sharedTextureCache] addImage:@"play_drop_4@2x.png"];	
            break;
		case 1:
		case 5:
		case 7:	
            emitter.texture = [[CCTextureCache sharedTextureCache] addImage:@"play_drop_5@2x.png"];	
            break;
		default: 
            emitter.texture = [[CCTextureCache sharedTextureCache] addImage:@"play_drop_3@2x.png"]; 
            break;
	}
	emitter.duration = 0.0f;
	emitter.emitterMode = kCCParticleModeRadius;
	
	if (_isiPhone) {
		emitter.startRadius = 10;
		emitter.startRadiusVar = 10;
		emitter.endRadius = 50;
		emitter.endRadiusVar = 20;
	}
	else {
		emitter.startRadius = 10.0f * (float)SCALE_MIN_IPAD;
		emitter.startRadiusVar = 10.0f * (float)SCALE_MIN_IPAD;
		emitter.endRadius = 50.0f * (float)SCALE_MIN_IPAD;
		emitter.endRadiusVar = 20.0f * (float)SCALE_MIN_IPAD;
	}
	
	emitter.rotatePerSecond = 0;
	emitter.rotatePerSecondVar = 0;
	
	emitter.angle = 90;
	emitter.angleVar = 360;
	
	emitter.life = 1.f;
	emitter.lifeVar = 0.2f;
	
	emitter.startSpin = 0;
	emitter.startSpinVar = 0;
	emitter.endSpin = 0;
	emitter.endSpinVar = 0;
	
	emitter.startColor = (ccColor4F){1.0f, 1.0f, 1.0f, 1.0f};
	emitter.startColorVar = (ccColor4F){0.0f, 0.0f, 0.0f, 0.0f};
	emitter.endColor = (ccColor4F){1.0f, 1.0f, 1.0f, 1.0f};
	emitter.endColorVar = (ccColor4F){0.0f, 0.0f, 0.0f, 0.0f};
	
	if (_isiPhone) {
		emitter.startSize = 25.0f;
		emitter.startSizeVar = 15.0f;
		emitter.endSize = 5.0f;
		emitter.endSizeVar = 1.0f;
	}
	else {
		emitter.startSize = 25.0f * (float)SCALE_MIN_IPAD;
		emitter.startSizeVar = 15.0f * (float)SCALE_MIN_IPAD;
		emitter.endSize = 5.0f * (float)SCALE_MIN_IPAD;
		emitter.endSizeVar = 1.0f * (float)SCALE_MIN_IPAD;
	}
	
	emitter.emissionRate = emitter.totalParticles / emitter.duration;
	emitter.position = ccp(self.position.x, self.position.y);
	emitter.posVar = CGPointZero;
	[(PlayScene*)[self parent] addChild:emitter];
	emitter.blendAdditive = NO;
	emitter.autoRemoveOnFinish = YES;
    [emitter release];
}

-(void) update:(ccTime)deltaTime
{
	if (!isActive) {
        return;
    }

	if (iceKind != MAX_ICE || iceKind != MAX_ICE-1) {
		if (_flyingTime < (float)TIME_TICKS) {
            [GameSound effectThrowIce:iceKind];
        }
	}

	_flyingTime += deltaTime;
	float time = deltaTime / (float)TIME_TICKS;
    if (_isiPhone) {
        _pY += _vY * time - time * time * (float)GRAVITY / 2.0;
        _vY -= time * (float)GRAVITY;
    }
    else {
        _pY += _vY * time - time * time * (float)GRAVITY*2. / 2.0;
        _vY -= time * (float)GRAVITY*2.;
    }
	_pX += _vX * time;
    self.position = ccp((int)_pX, (int)_pY);

	if (isSplit) {
		_firstPartPX += _firstPartVX;
		_firstPartPY += _firstPartVY;
		_secondPartPX += _secondPartVX;
		_secondPartPY += _secondPartVY;
		_firstPartSprite.position = ccp((int)_firstPartPX, (int)_firstPartPY);
		_secondPartSprite.position = ccp((int)_secondPartPX, (int)_secondPartPY);
		
		if (_isSplitFirst) {
			_firstPartFirstPX += _firstPartFirstVX;
			_firstPartFirstPY += _firstPartFirstVY;
			_firstPartSecondPX += _firstPartSecondVX;
			_firstPartSecondPY += _firstPartSecondVY;
			_firstPartFirstSprite.position = ccp(_firstPartPX + _firstPartFirstPX, _firstPartPY + _firstPartFirstPY);
			_firstPartSecondSprite.position = ccp(_firstPartPX + _firstPartSecondPX, _firstPartPY + _firstPartSecondPY);
		}
		
		if (_isSplitSecond) {
			_secondPartFirstPX += _secondPartFirstVX;
			_secondPartFirstPY += _secondPartFirstVY;
			_secondPartSecondPX += _secondPartSecondVX;
			_secondPartSecondPY += _secondPartSecondVY;
			_secondPartFirstSprite.position = ccp(_secondPartPX + _secondPartFirstPX, _secondPartPY + _secondPartFirstPY);
			_secondPartSecondSprite.position = ccp(_secondPartPX + _secondPartSecondPX, _secondPartPY + _secondPartSecondPY);
		}
		
		if (_isiPhone) {
			if (_vY < 0 && _pY < Y_START) {
				[self unschedule:@selector(update:)];
				[self initIce];
			}
		}
		else {
			if (_vY < 0 && _pY < (float)Y_START * (float)SCALE_MIN_IPAD) {
				[self unschedule:@selector(update:)];
				[self initIce];
			}
		}
	}
	else {
		if (_isiPhone) {
			if (_pY < Y_START) {
				[self unschedule:@selector(update:)];
				[self initIce];
			}
		}
		else {
			if (_pY < (float)SCALE_MIN_IPAD * (float)Y_START) {
				[self unschedule:@selector(update:)];
				[self initIce];
			}
		}
	}
}

-(int) splitAction:(float)posX1 Y1:(float)posY1 X2:(int)posX2 Y2:(int)posY2
{
	int retval = enum_split_none;
	int x, y;
	int x1, y1, x2, y2;
	float width, height;

    if (_isiPhone) {
        if (abs((int)posX2-(int)posX1) < 5 || abs((int)posY2-(int)posY1) < 5) {
            return retval;
        }
    }
    else {
        if (abs((int)posX2-(int)posX1) < 10 || abs((int)posY2-(int)posY1) < 10) {
            return retval;
        }
    }
	if (isActive == NO) {
        return retval;
    }
	if (_splitTime > _flyingTime - .2f) {
        return enum_split_none;
    }
	if (isSplit == NO) {
        if (_isiPhone) {
            width = [self getWidth:_oneSprite.contentSize.width/2 Height:_oneSprite.contentSize.height/2 Alpha:-self.rotation];
            height = [self getHeight:_oneSprite.contentSize.width/2 Height:_oneSprite.contentSize.height/2 Alpha:-self.rotation];
        }
        else {
            width = [self getWidth:_oneSprite.contentSize.width Height:_oneSprite.contentSize.height Alpha:-self.rotation];
            height = [self getHeight:_oneSprite.contentSize.width Height:_oneSprite.contentSize.height Alpha:-self.rotation];
        }
            
        x1 = self.position.x - width / 2;
        x2 = self.position.x + width / 2;
        y1 = self.position.y - height / 2;
        y2 = self.position.y + height / 2;

		for (int i = 0; i <= MAX_STEP; i++) {
			x = (posX1 * (MAX_STEP - i) + posX2 * i) / MAX_STEP;
			y = (posY1 * (MAX_STEP - i) + posY2 * i) / MAX_STEP;
			if (x1 < x && x < x2 && y1 < y && y < y2) {
				_splitTime = _flyingTime;
				if (iceKind == MAX_ICE) {
                    _penguin2SlashCount++;
                    if (_penguin2SlashCount == 1) {
                        _oneSprite.visible = FALSE;
                        _penguin2Oh1Sprite.visible = YES;
                        _penguin2Oh1Sprite.position = _oneSprite.position;
                    }
                    if (_penguin2SlashCount == 2) {
                        _penguin2Oh1Sprite.visible = FALSE;
                        _penguin2Oh2Sprite.visible = YES;
                        _penguin2Oh2Sprite.position = _penguin2Oh1Sprite.position;
                    }
                    if (_penguin2SlashCount == 3) {
                        _penguin2Oh2Sprite.visible = FALSE;
                        _penguin2Oh3Sprite.visible = YES;
                        _penguin2Oh3Sprite.position = _penguin2Oh2Sprite.position;

                        [(PlayScene *)[self parent] decreasePenguinLife];
                    }
                        
                    [GameSound effectPenguin];
                    [GameSound vib:1];
					return enum_split_none;
				}
				if (iceKind == MAX_ICE-1) {
                    _penguin1SlashCount++;
                    if (_penguin1SlashCount == 1) {
                        _oneSprite.visible = FALSE;
                        _penguin1Oh1Sprite.visible = YES;
                        _penguin1Oh1Sprite.position = _oneSprite.position;
                    }
                    if (_penguin1SlashCount == 2) {
                        _penguin1Oh1Sprite.visible = FALSE;
                        _penguin1Oh2Sprite.visible = YES;
                        _penguin1Oh2Sprite.position = _penguin1Oh1Sprite.position;
                    }
                    if (_penguin1SlashCount == 3) {
                        _penguin1Oh2Sprite.visible = FALSE;
                        _penguin1Oh3Sprite.visible = YES;
                        _penguin1Oh3Sprite.position = _penguin1Oh2Sprite.position;
                        
                        [(PlayScene *)[self parent] decreasePenguinLife];
                    }

                    [GameSound effectPenguin];
                    [GameSound vib:1];
					return enum_split_none;
				}
				
				_oneSprite.visible = FALSE;
				_firstPartSprite.visible = TRUE;
				_secondPartSprite.visible = TRUE;
				_vY /= 4.0f;
				_vX /= 4.0f;
				float alpha = atan2(posY2 - posY1, posX2 - posX1) * 180.0f / M_PI;
				switch (iceKind) {
					case 0:
					case 1:
					case 2:
					case 6:
					case 8:
						if (abs(((int)alpha % 360) - ((int)(-self.rotation + 90) % 360)) > 90) {
							self.rotation = -(alpha - 90.0f + 180.0f);
						}
						else {
                            self.rotation = -(alpha - 90.0f);
                        }
						_firstPartPY = 0;
						_secondPartPY = 0;
                        if (_isiPhone) {
                            _firstPartPX = -_firstPartSprite.contentSize.width / 4;
                            _secondPartPX = _secondPartSprite.contentSize.width / 4;
                        }
                        else {
                            _firstPartPX = -_firstPartSprite.contentSize.width / 2;
                            _secondPartPX = _secondPartSprite.contentSize.width / 2;
                        }
						_firstPartVX = -[self getRandomValue:1 Last:3];
						_firstPartVY = 0;
						_secondPartVX = -_firstPartVX;
						_secondPartVY = 0;
						break;
                    case 10:
                        break;
					default:
						if (abs((((int)alpha) % 360) - ((int)(-self.rotation) % 360)) > 90) {
							self.rotation = -(alpha + 180.0f);
						}
						else {
                            self.rotation = -alpha;
                        }
                        if (_isiPhone) {
                            _firstPartPY = _firstPartSprite.contentSize.height / 4;
                            _secondPartPY = -_secondPartSprite.contentSize.height / 4;
                        }
                        else {
                            _firstPartPY = _firstPartSprite.contentSize.height / 2;
                            _secondPartPY = -_secondPartSprite.contentSize.height / 2;
                        }
						_firstPartPX = 0;
						_secondPartPX = 0;
						_firstPartVX = 0;
						_firstPartVY = [self getRandomValue:1 Last:3];
						_secondPartVX = 0;
						_secondPartVY = -_firstPartVY;
						break;
				}
				_firstPartSprite.position = ccp((int)_firstPartPX, (int)_firstPartPY);
				_secondPartSprite.position = ccp((int)_secondPartPX, (int)_secondPartPY);
				
				isSplit = TRUE;
				
				[GameSound effectIceSlashed:iceKind];
                [GameSound vib:1];
				[self createExplosion:_oneSprite.position.x Y:_oneSprite.position.y];
				return enum_first_split;
			}
		}
	}
	else {
		if (!_isMultiSplit) {
            return retval;
        }
        if (abs((int)posX2-(int)posX1) <= 5 || abs((int)posY2-(int)posY1) <= 5) {
            return retval;
        }
		float alpha = -(float)self.rotation * M_PI / 180;
		if (_isSplitFirst == NO) {
            if (_isiPhone) {
                width = [self getWidth:_firstPartSprite.contentSize.width/2 Height:_firstPartSprite.contentSize.height/2 Alpha:-self.rotation];
                height = [self getHeight:_firstPartSprite.contentSize.width/2 Height:_firstPartSprite.contentSize.height/2 Alpha:-self.rotation];
            }
            else {
                width = [self getWidth:_firstPartSprite.contentSize.width Height:_firstPartSprite.contentSize.height Alpha:-self.rotation];
                height = [self getHeight:_firstPartSprite.contentSize.width Height:_firstPartSprite.contentSize.height Alpha:-self.rotation];
            }
                
			if (_isiPhone) {
				x1 = self.position.x + _firstPartPX * cosf(alpha) + _firstPartPY * sinf(alpha) - width / 2;
				x2 = x1 + width;
				y1 = self.position.y - _firstPartPX * sinf(alpha) + _firstPartPY * cosf(alpha) - height / 2;
				y2 = y1 + height;
			}
			else {
				x1 = self.position.x + (float)(_firstPartPX * cosf(alpha) + _firstPartPY * sinf(alpha) - width / 2) * self.scale;
				x2 = x1 + width * self.scale;
				y1 = self.position.y + (float)(-_firstPartPX * sinf(alpha) + _firstPartPY * cosf(alpha) - height / 2) * self.scale;
				y2 = y1 + height * self.scale;
			}
			
			for (int i = 0; i <= MAX_STEP; i++) {
				x = (posX1 * (MAX_STEP - i) + posX2 * i) / MAX_STEP;
				y = (posY1 * (MAX_STEP - i) + posY2 * i) / MAX_STEP;
				if (x1 < x && x < x2 && y1 < y && y < y2) {
					_splitTime = _flyingTime;
					_firstPartSprite.visible = FALSE;
					_firstPartFirstSprite.visible = TRUE;
					_firstPartSecondSprite.visible = TRUE;
					_vY /= 4.0f;
					_vX /= 4.0f;
					alpha = atan2(posY2 - posY1, posX2 - posX1) * 180.0 / M_PI;
					if (abs(((int)alpha % 360) - ((int)(-self.rotation + 90) % 360)) > 90) {
						_firstPartFirstSprite.rotation = -(alpha - 90.0f + 180.0f) - self.rotation;
					}
					else {
                        _firstPartFirstSprite.rotation = -(alpha - 90.0f) - self.rotation;
                    }
					alpha = -(float)_firstPartFirstSprite.rotation * M_PI / 180.0f;
					_firstPartSecondSprite.rotation = _firstPartFirstSprite.rotation;
					
                    if (_isiPhone) {
                        _firstPartFirstPX = -(float)_firstPartFirstSprite.contentSize.width / 4.0f * cosf(alpha);
                        _firstPartFirstPY = -(float)_firstPartFirstSprite.contentSize.width / 4.0f * sinf(alpha);
                        _firstPartSecondPX = (float)_firstPartSecondSprite.contentSize.width / 4.0f * cosf(alpha);
                        _firstPartSecondPY = (float)_firstPartSecondSprite.contentSize.width / 4.0f * sinf(alpha);
                    }
                    else {
                        _firstPartFirstPX = -(float)_firstPartFirstSprite.contentSize.width / 2.0f * cosf(alpha);
                        _firstPartFirstPY = -(float)_firstPartFirstSprite.contentSize.width / 2.0f * sinf(alpha);
                        _firstPartSecondPX = (float)_firstPartSecondSprite.contentSize.width / 2.0f * cosf(alpha);
                        _firstPartSecondPY = (float)_firstPartSecondSprite.contentSize.width / 2.0f * sinf(alpha);
                    }
					_firstPartFirstVX = -(float)[self getRandomValue:1 Last:3] * cosf(alpha);
					_firstPartFirstVY = -(float)[self getRandomValue:1 Last:3] * sinf(alpha);
					_firstPartSecondVX = (float)[self getRandomValue:1 Last:3] * cosf(alpha);
					_firstPartSecondVY = (float)[self getRandomValue:1 Last:3] * sinf(alpha);
					_firstPartFirstSprite.position = ccp((int)_firstPartFirstPX + _firstPartPX, (int)_firstPartFirstPY + _firstPartPY);
					_firstPartSecondSprite.position = ccp((int)_firstPartSecondPX + _firstPartPX, (int)_firstPartSecondPY + _firstPartPY);
					_isSplitFirst = TRUE;
					retval = enum_second_first_split;
					break;
				}
			}
		}
		alpha = -(float)self.rotation * M_PI / 180;
		if (_isSplitSecond == NO) {
            if (_isiPhone) {
                width = [self getWidth:_secondPartSprite.contentSize.width/2 Height:_secondPartSprite.contentSize.height/2 Alpha:-self.rotation];
                height = [self getHeight:_secondPartSprite.contentSize.width/2 Height:_secondPartSprite.contentSize.height/2 Alpha:-self.rotation];
            }
            else {
                width = [self getWidth:_secondPartSprite.contentSize.width Height:_secondPartSprite.contentSize.height Alpha:-self.rotation];
                height = [self getHeight:_secondPartSprite.contentSize.width Height:_secondPartSprite.contentSize.height Alpha:-self.rotation];
            }
			if (_isiPhone) {
				x1 = self.position.x + _secondPartPX * cosf(alpha) + _secondPartPY * sinf(alpha) - width / 2;
				x2 = x1 + width;
				y1 = self.position.y - _secondPartPX * sinf(alpha) + _secondPartPY * cosf(alpha) - height / 2;
				y2 = y1 + height;
			}
			else {
				x1 = self.position.x + (float)(_secondPartPX * cosf(alpha) + _secondPartPY * sinf(alpha) - width / 2) * self.scale;
				x2 = x1 + width * self.scale;
				y1 = self.position.y + (float)(-_secondPartPX * sinf(alpha) + _secondPartPY * cosf(alpha) - height / 2) * self.scale;
				y2 = y1 + height * self.scale;
			}
			
			for (int i = 0; i <= MAX_STEP; i++) {
				x = (posX1 * (MAX_STEP - i) + posX2 * i) / MAX_STEP;
				y = (posY1 * (MAX_STEP - i) + posY2 * i) / MAX_STEP;
				if (x1 < x && x < x2 && y1 < y && y < y2) {
					_splitTime = _flyingTime;
					_secondPartSprite.visible = FALSE;
					_secondPartFirstSprite.visible = TRUE;
					_secondPartSecondSprite.visible = TRUE;
					_vY /= 4.0f;
					_vX /= 4.0f;
					alpha = atan2(posY2 - posY1, posX2 - posX1) * 180.0 / M_PI;
					if (abs(((int)alpha % 360) - ((int)(-self.rotation + 90) % 360)) > 90) {
						_secondPartFirstSprite.rotation = -(alpha - 90.0f + 180.0f) - self.rotation;
					}
					else {
                        _secondPartFirstSprite.rotation = -(alpha - 90.0f) - self.rotation;
                    }
					alpha = -(float)_secondPartFirstSprite.rotation * M_PI / 180.0f;
					_secondPartSecondSprite.rotation = _secondPartFirstSprite.rotation;
					
                    if (_isiPhone) {
                        _secondPartFirstPX = -(float)_secondPartFirstSprite.contentSize.width / 4.0f * cosf(alpha);
                        _secondPartFirstPY = -(float)_secondPartFirstSprite.contentSize.width / 4.0f * sinf(alpha);
                        _secondPartSecondPX = (float)_secondPartSecondSprite.contentSize.width / 4.0f * cosf(alpha);
                        _secondPartSecondPY = (float)_secondPartSecondSprite.contentSize.width / 4.0f * sinf(alpha);
                    }
                    else {
                        _secondPartFirstPX = -(float)_secondPartFirstSprite.contentSize.width / 2.0f * cosf(alpha);
                        _secondPartFirstPY = -(float)_secondPartFirstSprite.contentSize.width / 2.0f * sinf(alpha);
                        _secondPartSecondPX = (float)_secondPartSecondSprite.contentSize.width / 2.0f * cosf(alpha);
                        _secondPartSecondPY = (float)_secondPartSecondSprite.contentSize.width / 2.0f * sinf(alpha);
                    }
					_secondPartFirstVX = -(float)[self getRandomValue:1 Last:3] * cosf(alpha);
					_secondPartFirstVY = -(float)[self getRandomValue:1 Last:3] * sinf(alpha);
					_secondPartSecondVX = (float)[self getRandomValue:1 Last:3] * cosf(alpha);
					_secondPartSecondVY = (float)[self getRandomValue:1 Last:3] * sinf(alpha);
					_secondPartFirstSprite.position = ccp((int)_secondPartFirstPX + _secondPartPX, (int)_secondPartFirstPY + _secondPartPY);
					_secondPartSecondSprite.position = ccp((int)_secondPartSecondPX + _secondPartPX, (int)_secondPartSecondPY + _secondPartPY);
					_isSplitSecond = TRUE;
					retval += enum_second_second_split;
					break;
				}
			}
		}
		if (retval != enum_split_none) {
            [GameSound effectIceSlashed:iceKind];
        }
	}
	return retval;
}

-(float) getWidth:(int)width Height:(int)height Alpha:(float)alpha
{
	alpha = alpha * M_PI / 180.0f;
	float x1 = (float)width * cosf(alpha) + (float)height * sinf(alpha);
	float x2 = -(float)width * cosf(alpha) + (float)height * sinf(alpha);
	float x3 = (float)width * cosf(alpha) - (float)height * sinf(alpha);
	float x4 = -(float)width * cosf(alpha) - (float)height * sinf(alpha);
	float dd = 0;
	if (dd < x1) {
        dd = x1;
    }
	if (dd < x2) {
        dd = x2;
    }
	if (dd < x3) {
        dd = x3;
    }
	if (dd < x4) {
        dd = x4;
    }
	return dd;
}

-(float) getHeight:(int)width Height:(int)height Alpha:(float)alpha
{
	alpha = alpha * M_PI / 180.0f;
	float x1 = (float)width * sinf(alpha) + (float)height * cosf(alpha);
	float x2 = -(float)width * sinf(alpha) + (float)height * cosf(alpha);
	float x3 = (float)width * sinf(alpha) - (float)height * cosf(alpha);
	float x4 = -(float)width * sinf(alpha) - (float)height * cosf(alpha);
	float dd = 0;
	if (dd < x1) {
        dd = x1;
    }
	if (dd < x2) {
        dd = x2;
    }
	if (dd < x3) {
        dd = x3;
    }
	if (dd < x4) {
        dd = x4;
    }
	return dd;
}

- (BOOL) isFullSplit
{
	return _isSplitFirst && isSplit && _isSplitSecond;
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
    [GameSound unloadIceSlashed];
    [GameSound unloadThrowIce];
    
	[_oneSprite release];
	[_firstPartSprite release];
	[_firstPartFirstSprite release];
	[_firstPartSecondSprite release];
	[_secondPartSprite release];
	[_secondPartFirstSprite release];
	[_secondPartSecondSprite release];
    [_penguin1Oh1Sprite release];
    [_penguin1Oh2Sprite release];
    [_penguin1Oh3Sprite release];
    [_penguin2Oh1Sprite release];
    [_penguin2Oh2Sprite release];
    [_penguin2Oh3Sprite release];
	[super dealloc];
}
@end

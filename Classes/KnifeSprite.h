//
//  OptionScene.h
//  Ice_Cracker
//
//  Created by mac on 5/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

enum {
	enum_knife_0 = 0,
	enum_knife_1,
	enum_knife_2,
	enum_knife_3,
	enum_knife_none,
};

@interface KnifeSprite : CCSprite {
	BOOL _isActive;
	ccTime _appearTime;
	ccTime _visibleTime;
	int _knifeColor;
}


+(id) sharedKnifeSprite;

-(void) initKnife;

-(BOOL) appearAction:(int)posX1 Y1:(int)posY1 X2:(int)posX2 Y2:(int)posY2 Color:(int)selectColor Sound:(BOOL)soundEffect;
-(BOOL) splitAction:(int)posX1 Y1:(int)posY1 X2:(int)posX2 Y2:(int)posY2 Color:(int)selectColor;

-(void) update:(ccTime)deltaTime;

@property(nonatomic) BOOL _isActive;
@end

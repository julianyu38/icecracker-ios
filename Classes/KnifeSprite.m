//
//  OptionScene.m
//  Ice_Cracker
//
//  Created by mac on 5/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "KnifeSprite.h"
#import "GameData.h"
#import "GameSound.h"
#import "IceSprite.h"

@implementation KnifeSprite

@synthesize _isActive = isActive;

#define PI	(3.1415927)

#define APPEAR_TIME		(1)

#define KNIFE_MAX_SCALEY	1.5f
#define KNIFE_MIN_SCALEY	0.8f

static BOOL isiPhone;

+(id) sharedKnifeSprite
{
	KnifeSprite *sprite = [KnifeSprite node];

    isiPhone = [GameData isiPhone];

	// return the scene
	return sprite;
}

-(id) init
{
	if( (self=[super init] )) {
		isActive = FALSE;

		CCSprite *tmp = [[CCSprite alloc] initWithFile:@"play_knife_0@2x.png"];
		[self setTexture:tmp.texture];
		[self setTextureRect:tmp.textureRect];
        [tmp release];
		self.opacity = 0;
	}
	return self;
}

-(void) initKnife
{
	self.opacity = 0;
	isActive = FALSE;
}

-(BOOL) splitAction:(int)posX1 Y1:(int)posY1 X2:(int)posX2 Y2:(int)posY2 Color:(int)selelctColor
{
	isActive = TRUE;
	self.opacity = 0;
	
	_visibleTime = APPEAR_TIME;
	CCSprite *tmp;
	_knifeColor = selelctColor;
	switch (selelctColor) {
		case enum_knife_0:
			tmp = [[CCSprite alloc] initWithFile:@"play_knife_0@2x.png"];
			[self setTexture:tmp.texture];
			[self setTextureRect:tmp.textureRect];
            [tmp release];
			break;
		case enum_knife_1:
			tmp = [[CCSprite alloc] initWithFile:@"play_knife_1@2x.png"];
			[self setTexture:tmp.texture];
			[self setTextureRect:tmp.textureRect];
            [tmp release];
			break;
		case enum_knife_2:
			tmp = [[CCSprite alloc] initWithFile:@"play_knife_2@2x.png"];
			[self setTexture:tmp.texture];
			[self setTextureRect:tmp.textureRect];
            [tmp release];
			break;
		case enum_knife_3:
			tmp = [[CCSprite alloc] initWithFile:@"play_knife_3@2x.png"];
			[self setTexture:tmp.texture];
			[self setTextureRect:tmp.textureRect];
            [tmp release];
			break;
		default:	return FALSE;
			break;
	}
	
	self.scaleX = 1;
	self.scaleY = 1;
	if (!isiPhone) {
		self.scaleX *= SCALE_MIN_IPAD;
		self.scaleY *= SCALE_MIN_IPAD;
	}
	
	float alpha = atan2(posY2 - posY1, posX2 - posX1);
	self.rotation = -alpha * 180.0f / PI;
	
	self.position = ccp((posX1 + posX2) / 2, (posY1 + posY2) / 2);
	
	self.opacity = 255;
	[self schedule:@selector(update:) interval:TIME_TICKS];
	_appearTime = 0;
	return FALSE;
}

-(BOOL) appearAction:(int)posX1 Y1:(int)posY1 X2:(int)posX2 Y2:(int)posY2 Color:(int)selectColor Sound:(BOOL)soundEffect
{
	float length = sqrt((posX1 - posX2) * (posX1 - posX2) + (posY1 - posY2) * (posY1 - posY2));
	if (isiPhone) {
		if (length < 20) {
            return FALSE;
        }
	}
	else {
		if (length < 20.0f * SCALE_MIN_IPAD) {
            return FALSE;
        }
	}
	
	isActive = TRUE;
	self.opacity = 0;

	_visibleTime = 0.3f;
	
	CCSprite *tmp;
	_knifeColor = selectColor;
	switch (selectColor) {
		case enum_knife_0:
			tmp = [[CCSprite alloc] initWithFile:@"play_knife_0@2x.png"];
			[self setTexture:tmp.texture];
			[self setTextureRect:tmp.textureRect];
            [tmp release];
			break;
		case enum_knife_1:
			tmp = [[CCSprite alloc] initWithFile:@"play_knife_1@2x.png"];
			[self setTexture:tmp.texture];
			[self setTextureRect:tmp.textureRect];
            [tmp release];
			break;
		case enum_knife_2:
			tmp = [[CCSprite alloc] initWithFile:@"play_knife_2@2x.png"];
			[self setTexture:tmp.texture];
			[self setTextureRect:tmp.textureRect];
            [tmp release];
			break;
		case enum_knife_3:
			tmp = [[CCSprite alloc] initWithFile:@"play_knife_3@2x.png"];
			[self setTexture:tmp.texture];
			[self setTextureRect:tmp.textureRect];
            [tmp release];
			break;
		default:	return FALSE;
	}

	self.scaleX = length / (float)(self.contentSize.width);
	self.scaleY = self.scaleX;
	if (self.scaleY < KNIFE_MIN_SCALEY && isiPhone) {
        self.scaleY = KNIFE_MIN_SCALEY;
    }
	if (self.scaleY < (float)SCALE_MIN_IPAD * (float)KNIFE_MIN_SCALEY && (!isiPhone)) {
        self.scaleY = (float)SCALE_MIN_IPAD * (float)KNIFE_MIN_SCALEY;
    }
	
	float alpha = atan2(posY2 - posY1, posX2 - posX1);
	self.rotation = -alpha * 180.0f / PI;
	
	self.position = ccp((posX1 + posX2) / 2, (posY1 + posY2) / 2);
	
	self.opacity = 255;
	[self schedule:@selector(update:) interval:TIME_TICKS];
	_appearTime = 0;
	return TRUE;
}

-(void) update:(ccTime)deltaTime
{
	_appearTime += deltaTime;
	if (_appearTime > _visibleTime) {
		self.opacity = 0;
		[self unschedule:@selector(update:)];
		isActive = FALSE;
		return;
	}
	float opp = 255 - _appearTime * 255.0 / _visibleTime;
	if (opp < 0) {
        opp = 0;
    }
	self.opacity = (int)opp;
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}
@end

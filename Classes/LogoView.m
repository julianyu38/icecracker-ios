//
//  LogoView.m
//  Insect_Samurai
//
//  Created by iPro on 6/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "LogoView.h"
#import "MainMenuScene.h"
#import "GameData.h"

@implementation LogoView

+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	LogoView *layer = [LogoView node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if ((self=[super init])) {
		self.isTouchEnabled = YES;
		
		CGSize size = [[CCDirector sharedDirector] winSize];
		CCSprite *background = [CCSprite spriteWithFile:@"Splash@2x.png"];
        if ([GameData isiPhone]) {
            [GameData setScaleIphone:background];
        }
        background.position = ccp(size.width / 2, size.height / 2);
		[self addChild:background];
        
        _timer = [NSTimer scheduledTimerWithTimeInterval:3. target:self selector:@selector(gotoMenuView) userInfo:nil repeats:NO];
	}
	return self;
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	// don't forget to call "super dealloc"
	[super dealloc];
}

-(void) gotoMenuView {
    [_timer invalidate];
    _timer = nil;
    
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.8 scene:[MainMenuScene scene]]];
}

@end

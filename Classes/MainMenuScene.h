//
//  MainMenuScene.h
//  Ice_Cracker
//
//  Created by mac on 5/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "GameSound.h"

@interface MainMenuScene : CCLayer 
{
    CCSprite *_background;
    CCSprite *_playSprite;
    CCSprite *_playPart1Sprite;
    CCSprite *_playPart2Sprite;
    CCSprite *_optionSprite;
    CCSprite *_infoSprite;
    CCSprite *_otherSprite;
    CCSprite *_optionSoundSprite;
    CCSprite *_optionMusicSprite;
    CCSprite *_optionVibrationSprite;

	bool _isPressed;
	int _menuSelected;
	
	int _menuState;
	
	CGPoint _prevPoint;
	ccTime _menuTime;
	ccTime _pastTime;
	int _selectKnife;
	BOOL _isEmptyKnife;

	ALuint _swardHissID;
}

+(id) scene;

-(void) updateNow:(ccTime)deltaTime;

@end

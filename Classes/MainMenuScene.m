//
//  MainMenuScene.m
//  Ice_Cracker
//
//  Created by mac on 5/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MainMenuScene.h"
#import "PlayScene.h"
#import "KnifeSprite.h"

#import "GameData.h"
#import "GameSound.h"

#import "cocos2d.h"


@implementation MainMenuScene

enum {
	enum_tag_background = 0,
	enum_tag_play,
	enum_tag_play_part1,
	enum_tag_play_part2,
	enum_tag_option,
	enum_tag_info,
	enum_tag_other,
	enum_tag_knifes,
};

enum {
	enum_tag_sound = 0,
	enum_tag_music,
	enum_tag_vibration,
	enum_tag_multiSplit,
};

enum {
	enum_state_menu = 0,
	enum_state_option,
	enum_state_temp,
};

enum {
	enum_press_none = 0,
	enum_press_play,
	enum_press_option,
	enum_press_info,
	enum_press_other,
	enum_press_double,
};

enum {
	enum_option_none = 0,
	enum_option_cancel,
	enum_option_sound,
	enum_option_music,
	enum_option_vibration,
	enum_option_multiSplit,
};

#define MAX_KNIFE_INSCREEN		50

#define MENU_PLAY_R           50
#define	MENU_PLAY_X           -2
#define MENU_PLAY_Y           -64

#define MENU_OTHER_R           30
#define	MENU_OTHER_X           -2
#define MENU_OTHER_Y           -130

#define MENU_OPTION_X			-60
#define MENU_OPTION_Y			-28
#define MENU_OPTION_WIDTH		112
#define MENU_OPTION_HEIGHT      139
#define OPTION_HEIGHT           139
#define OPTION_WIDTH			MENU_OPTION_WIDTH

#define OPTION_MOVE             95

#define OPTION_LABEL_X          8
#define OPTION_LABEL_Y          65
#define OPTION_LABEL_WIDTH      100
#define OPTION_LABEL_HEIGHT     32

#define OPTION_SOUND_X          1
#define OPTION_SOUND_Y          -18
#define OPTION_SOUND_WIDTH      69
#define OPTION_SOUND_HEIGHT     19

#define OPTION_MUSIC_X          -2
#define OPTION_MUSIC_Y          10
#define OPTION_MUSIC_WIDTH      62
#define OPTION_MUSIC_HEIGHT     20

#define OPTION_VIBRATION_X		2
#define OPTION_VIBRATION_Y		-48
#define OPTION_VIBRATION_WIDTH	99
#define OPTION_VIBRATION_HEIGHT	20

#define MENU_INFO_R             25
#define MENU_INFO_X             30
#define MENU_INFO_Y             29

#define PRESS_OPACITY           255
#define RELEASE_OPACITY         200
#define OPTION_OPACITY          80

#define TOUCH_MOVING_MIN		5
#define LIMIT_PAST_TIME			1.0f

#define MAX_STEP                30

+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	MainMenuScene *layer = [MainMenuScene node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init] )) {
		
		self.isTouchEnabled = YES;

		_menuState = enum_state_menu;
		_isPressed = FALSE;
		
		[GameData initGameData];
		[GameSound stopBG];
		[GameSound playMainBG];
		// ask director the the window size
		CGSize size = [[CCDirector sharedDirector] winSize];

		_background = [[CCSprite alloc] initWithFile:@"main_background@2x.png"];
		_playSprite = [[CCSprite alloc] initWithFile:@"main_menu_play@2x.png"];
        _playPart1Sprite = [[CCSprite alloc] initWithFile:@"main_menu_play_part1@2x.png"];
        _playPart2Sprite = [[CCSprite alloc] initWithFile:@"main_menu_play_part2@2x.png"];
		_optionSprite = [[CCSprite alloc] initWithFile:@"main_option_background@2x.png"];
		_infoSprite = [[CCSprite alloc] initWithFile:@"main_menu_info@2x.png"];
		_otherSprite = [[CCSprite alloc] initWithFile:@"main_menu_other@2x.png"];

		for (int i = 0; i < MAX_KNIFE_INSCREEN; i++) {
			KnifeSprite *knifeSprite = [KnifeSprite sharedKnifeSprite];
			[self addChild:knifeSprite z:enum_tag_knifes + i tag:enum_tag_knifes + i];
		}
        
		if ([GameData isiPhone]) {
			_background.position = ccp(size.width / 2, size.height / 2);
            [GameData setScaleIphone:_background];
			_playSprite.position = ccp(size.width / 2 + MENU_PLAY_X, size.height / 2 + MENU_PLAY_Y);
            _playSprite.scale = .5f;
            _playPart1Sprite.position = ccp(size.width / 2 + MENU_PLAY_X - (73./2.), size.height / 2 + MENU_PLAY_X);
            _playPart1Sprite.scale = .5f;
            _playPart2Sprite.position = ccp(size.width / 2 + MENU_PLAY_X + (83./2.), size.height / 2 + MENU_PLAY_X);
            _playPart2Sprite.scale = .5f;
			_optionSprite.position = ccp(size.width + MENU_OPTION_X, MENU_OPTION_Y);
            _optionSprite.scale = .5f;
			_infoSprite.position = ccp(MENU_INFO_X, MENU_INFO_Y);
            _infoSprite.scale = .5f;
			_otherSprite.position = ccp(size.width / 2 + MENU_OTHER_X, size.height / 2 + MENU_OTHER_Y);
            _otherSprite.scale = .5f;
		}
		else {
			_background.position = ccp(size.width / 2, size.height / 2);
			_playSprite.position = ccp(size.width / 2 + (float)SCALE_MIN_IPAD * MENU_PLAY_X, size.height / 2 + (float)SCALE_MIN_IPAD * MENU_PLAY_Y);
			_playPart1Sprite.position = ccp(size.width / 2 + (float)SCALE_MIN_IPAD * MENU_PLAY_X - 73, size.height / 2 + (float)SCALE_MIN_IPAD * MENU_PLAY_Y);
			_playPart2Sprite.position = ccp(size.width / 2 + (float)SCALE_MIN_IPAD * MENU_PLAY_X + 83, size.height / 2 + (float)SCALE_MIN_IPAD * MENU_PLAY_Y);
			_optionSprite.position = ccp(size.width + (float)SCALE_MIN_IPAD * MENU_OPTION_X, (float)SCALE_MIN_IPAD * MENU_OPTION_Y);
			_infoSprite.position = ccp((float)SCALE_MIN_IPAD * MENU_INFO_X, (float)SCALE_MIN_IPAD * MENU_INFO_Y);
			_otherSprite.position = ccp(size.width / 2 + (float)SCALE_MIN_IPAD * MENU_OTHER_X, size.height / 2 + (float)SCALE_MIN_IPAD * MENU_OTHER_Y);
		}
		
		_playSprite.opacity = RELEASE_OPACITY;
        _playPart1Sprite.opacity = RELEASE_OPACITY;
        _playPart2Sprite.opacity = RELEASE_OPACITY;
		_infoSprite.opacity = RELEASE_OPACITY;
		_otherSprite.opacity = RELEASE_OPACITY;

		[self addChild: _background z:enum_tag_background tag:enum_tag_background];
		[self addChild: _playSprite z:enum_tag_play tag:enum_tag_play];
		[self addChild: _playPart1Sprite z:enum_tag_play tag:enum_tag_play_part1];
		[self addChild: _playPart2Sprite z:enum_tag_play tag:enum_tag_play_part2];
		[self addChild: _optionSprite z:enum_tag_option tag:enum_tag_option];
		[self addChild: _infoSprite z:enum_tag_info tag:enum_tag_info];
		[self addChild: _otherSprite z:enum_tag_other tag:enum_tag_other];
		
		if ([GameData getSound]) {
            _optionSoundSprite = [[CCSprite alloc] initWithFile:@"main_option_sound_select@2x.png"];
        }
		else {
            _optionSoundSprite = [[CCSprite alloc] initWithFile:@"main_option_sound_unselect@2x.png"];
        }
        _optionSoundSprite.position = ccp(_optionSprite.contentSize.width / 2 + (float)SCALE_MIN_IPAD * OPTION_SOUND_X, _optionSprite.contentSize.height / 2 + (float)SCALE_MIN_IPAD * OPTION_SOUND_Y);

		if ([GameData getMusic]) {
            _optionMusicSprite = [[CCSprite alloc] initWithFile:@"main_option_music_select@2x.png"];
        }
		else {
            _optionMusicSprite = [[CCSprite alloc] initWithFile:@"main_option_music_unselect@2x.png"];
        }
        _optionMusicSprite.position = ccp(_optionSprite.contentSize.width / 2 + (float)SCALE_MIN_IPAD * OPTION_MUSIC_X, _optionSprite.contentSize.height / 2 + (float)SCALE_MIN_IPAD * OPTION_MUSIC_Y);
            
		if ([GameData getVibration]) {
            _optionVibrationSprite = [[CCSprite alloc] initWithFile:@"main_option_vibration_select@2x.png"];
        }
		else {
            _optionVibrationSprite = [[CCSprite alloc] initWithFile:@"main_option_vibration_unselect@2x.png"];
        }
        _optionVibrationSprite.position = ccp(_optionSprite.contentSize.width / 2 + (float)SCALE_MIN_IPAD * OPTION_VIBRATION_X, _optionSprite.contentSize.height / 2 + (float)SCALE_MIN_IPAD * OPTION_VIBRATION_Y);
        if (![[UIDevice currentDevice].model isEqualToString:@"iPhone"]) {
            _optionVibrationSprite.visible = FALSE;
        }
        else {
            _optionVibrationSprite.visible = TRUE;
        }
        
		[_optionSprite addChild:_optionSoundSprite z:enum_tag_sound tag:enum_tag_sound];
		[_optionSprite addChild:_optionMusicSprite z:enum_tag_music tag:enum_tag_music];
		[_optionSprite addChild:_optionVibrationSprite z:enum_tag_vibration tag:enum_tag_vibration];

        _playPart1Sprite.visible = FALSE;
        _playPart2Sprite.visible = FALSE;
		_isEmptyKnife = NO;
		_menuTime = 0;
		_isPressed = NO;
		[self schedule:@selector(updateNow:) interval:0.1];
	}
	return self;
}

- (void) updateNow:(ccTime)deltaTime
{
	_menuTime += deltaTime;
	
	if (_isPressed) {
		_pastTime += deltaTime;
	}
	else {
		_pastTime = 0;
	}
}

- (void) appearPanel
{
	id move;
	if ([GameData isiPhone]) {
		move = [CCMoveBy actionWithDuration: 1.0 position:ccp(0, OPTION_MOVE+5.0)];
		move = [CCEaseBounceOut actionWithAction:move];
	}
	else {
		move = [CCMoveBy actionWithDuration: 1.0 position:ccp(0, (float)SCALE_MIN_IPAD * OPTION_MOVE)];
		move = [CCEaseBounceOut actionWithAction:move];
	}
	[((CCSprite*)[self getChildByTag:enum_tag_option]) runAction:move];
}

- (void) changeState
{
	_menuState = enum_state_option;
}

- (void) disappearPanel
{
	id move;
	if ([GameData isiPhone]) {
		move = [CCMoveBy actionWithDuration: 1.0 position:ccp(0, -OPTION_MOVE-5.0)];
		move = [CCEaseBounceOut actionWithAction:move];
	}
	else {
		move = [CCMoveBy actionWithDuration: 1.0 position:ccp(0, -(float)SCALE_MIN_IPAD * OPTION_MOVE)];
		move = [CCEaseBounceOut actionWithAction:move];
	}
	[((CCSprite*)[self getChildByTag:enum_tag_option]) runAction:move];
	
}

- (void) restoreState
{
	_menuState = enum_state_menu;
}

- (void) pushOptionPanel
{
	_menuState = enum_state_temp;
	((CCSprite*)[self getChildByTag:enum_tag_background]).opacity = PRESS_OPACITY;
	((CCSprite*)[self getChildByTag:enum_tag_play]).opacity = RELEASE_OPACITY;
	((CCSprite*)[self getChildByTag:enum_tag_info]).opacity = RELEASE_OPACITY;
	[self runAction:[CCSequence actions:
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearPanel)],
					 [CCDelayTime actionWithDuration:.8],
					 [CCCallFunc actionWithTarget:self selector:@selector(restoreState)],
					 nil]];	
}

-(void) popOptionPanel
{
	_menuState = enum_state_temp;
	((CCSprite*)[self getChildByTag:enum_tag_background]).opacity = OPTION_OPACITY;
	((CCSprite*)[self getChildByTag:enum_tag_play]).opacity = OPTION_OPACITY;
	((CCSprite*)[self getChildByTag:enum_tag_info]).opacity = OPTION_OPACITY;
	[self runAction:[CCSequence actions:
					 [CCCallFunc actionWithTarget:self selector:@selector(appearPanel)],
					 [CCDelayTime actionWithDuration:.8],
					 [CCCallFunc actionWithTarget:self selector:@selector(changeState)],
					 nil]];
}

- (int) getMenu:(int)x Y:(int)y
{
	int dx, dy;
	CGSize winSize = [[CCDirector sharedDirector] winSize];
	if ([GameData isiPhone]) {
		dx = x - (winSize.width / 2 + MENU_PLAY_X);
		dy = y - (winSize.height / 2 + MENU_PLAY_Y);
		if (sqrt(dx * dx + dy * dy) < MENU_PLAY_R) {
            return enum_press_play;
        }
		dx = x - MENU_INFO_X;
		dy = y - MENU_INFO_Y;
		if (sqrt(dx * dx + dy * dy) < MENU_INFO_R) {
            return enum_press_info;
        }
		dx = x - (winSize.width / 2 + MENU_OTHER_X);
		dy = y - (winSize.height / 2 + MENU_OTHER_Y);
		if (sqrt(dx * dx + dy * dy) < MENU_OTHER_R) {
            return enum_press_other;
        }
		if ((winSize.width - OPTION_WIDTH) < x && y < OPTION_HEIGHT / 2 + MENU_OPTION_Y) {
            return enum_press_option;
        }
	}
	else {
		dx = x - (winSize.width / 2 + (float)SCALE_MIN_IPAD * MENU_PLAY_X);
		dy = y - (winSize.height / 2 + (float)SCALE_MIN_IPAD * MENU_PLAY_Y);
		if (sqrt(dx * dx + dy * dy) < (float)SCALE_MIN_IPAD * MENU_PLAY_R) {
            return enum_press_play;
        }
		dx = x - (float)SCALE_MIN_IPAD * MENU_INFO_X;
		dy = y - (float)SCALE_MIN_IPAD * MENU_INFO_Y;
		if (sqrt(dx * dx + dy * dy) < (float)SCALE_MIN_IPAD * MENU_INFO_R) {
            return enum_press_info;
        }
		dx = x - (winSize.width / 2 + (float)SCALE_MIN_IPAD * MENU_OTHER_X);
		dy = y - (winSize.height / 2 + (float)SCALE_MIN_IPAD * MENU_OTHER_Y);
		if (sqrt(dx * dx + dy * dy) < (float)SCALE_MIN_IPAD * MENU_OTHER_R) {
            return enum_press_other;
        }
		if ((winSize.width - (float)SCALE_MIN_IPAD * OPTION_WIDTH) < x && y < (float)SCALE_MIN_IPAD * (OPTION_HEIGHT / 2 + MENU_OPTION_Y)) {
            return enum_press_option;
        }
	}
	return enum_press_none;
}

- (int) getOption:(int)x Y:(int)y
{
	int dx, dy;
	CGSize winSize = [[CCDirector sharedDirector] winSize];
	CCSprite *optionSprite = (CCSprite*)[self getChildByTag:enum_tag_option];
	int refX = optionSprite.position.x;
	int refY = optionSprite.position.y;
	if ([GameData isiPhone]) {
		if (x < winSize.width - OPTION_WIDTH || y > OPTION_HEIGHT) {
            return enum_option_cancel;
        }
		dx = x - (refX + OPTION_LABEL_X);
		dy = y - (refY + OPTION_LABEL_Y);
		if (-OPTION_LABEL_WIDTH / 2 - 2 < dx && dx < OPTION_LABEL_WIDTH / 2 + 2 && -OPTION_LABEL_HEIGHT / 2 - 2 < dy && dy < OPTION_LABEL_HEIGHT / 2 + 2) {
            return enum_option_cancel;
        }
		dx = x - (refX + OPTION_SOUND_X);
		dy = y - (refY + OPTION_SOUND_Y);
		if (-OPTION_SOUND_WIDTH / 2 - 2 < dx && dx < OPTION_SOUND_WIDTH / 2 + 2 && -OPTION_SOUND_HEIGHT / 2 - 2 < dy && dy < OPTION_SOUND_HEIGHT / 2 + 2) {
            return enum_option_sound;
        }
		dx = x - (refX + OPTION_MUSIC_X);
		dy = y - (refY + OPTION_MUSIC_Y);
		if (-OPTION_MUSIC_WIDTH / 2 - 2 < dx && dx < OPTION_MUSIC_WIDTH / 2 + 2 && -OPTION_MUSIC_HEIGHT / 2 - 2 < dy && dy < OPTION_MUSIC_HEIGHT / 2 + 2) {
            return enum_option_music;
        }
		dx = x - (refX + OPTION_VIBRATION_X);
		dy = y - (refY + OPTION_VIBRATION_Y);
		if (-OPTION_VIBRATION_WIDTH / 2 - 2 < dx && dx < OPTION_VIBRATION_WIDTH / 2 + 2 && -OPTION_VIBRATION_HEIGHT / 2 + 2 < dy && dy < OPTION_VIBRATION_HEIGHT / 2) {
            return enum_option_vibration;
        }
	}
	else {
		if (x < winSize.width - (float)SCALE_MIN_IPAD * OPTION_WIDTH || y > (float)SCALE_MIN_IPAD * OPTION_HEIGHT) {
            return enum_option_cancel;
        }
		dx = x - (refX + (float)SCALE_MIN_IPAD * OPTION_LABEL_X);
		dy = y - (refY + (float)SCALE_MIN_IPAD * OPTION_LABEL_Y);
		if ((float)SCALE_MIN_IPAD * ((-OPTION_LABEL_WIDTH) / 2 - 2) < dx && dx < (float)SCALE_MIN_IPAD * (OPTION_LABEL_WIDTH / 2 + 2) 
			&& (float)SCALE_MIN_IPAD * ((-OPTION_LABEL_HEIGHT) / 2 - 2) < dy && dy < (float)SCALE_MIN_IPAD * (OPTION_LABEL_HEIGHT / 2 + 2)) {
            return enum_option_cancel;
        }
		dx = x - (refX + (float)SCALE_MIN_IPAD * OPTION_SOUND_X);
		dy = y - (refY + (float)SCALE_MIN_IPAD * OPTION_SOUND_Y);
		if ((float)SCALE_MIN_IPAD * ((-OPTION_SOUND_WIDTH) / 2 - 2) < dx && dx < (float)SCALE_MIN_IPAD * (OPTION_SOUND_WIDTH / 2 + 2) 
			&& (float)SCALE_MIN_IPAD * ((-OPTION_SOUND_HEIGHT) / 2 - 2) < dy && dy < (float)SCALE_MIN_IPAD * (OPTION_SOUND_HEIGHT / 2 + 2)) {
            return enum_option_sound;
        }
		dx = x - (refX + (float)SCALE_MIN_IPAD * OPTION_MUSIC_X);
		dy = y - (refY + (float)SCALE_MIN_IPAD * OPTION_MUSIC_Y);
		if ((float)SCALE_MIN_IPAD * ((-OPTION_MUSIC_WIDTH) / 2 - 2) < dx && dx < (float)SCALE_MIN_IPAD * (OPTION_MUSIC_WIDTH / 2 + 2) 
			&& (float)SCALE_MIN_IPAD * ((-OPTION_MUSIC_HEIGHT) / 2 - 2) < dy && dy < (float)SCALE_MIN_IPAD * (OPTION_MUSIC_HEIGHT / 2 + 2)) {
            return enum_option_music;
        }
		dx = x - (refX + (float)SCALE_MIN_IPAD * OPTION_VIBRATION_X);
		dy = y - (refY + (float)SCALE_MIN_IPAD * OPTION_VIBRATION_Y);
		if ((float)SCALE_MIN_IPAD * ((-OPTION_VIBRATION_WIDTH) / 2 - 2) < dx && dx < (float)SCALE_MIN_IPAD * (OPTION_VIBRATION_WIDTH / 2 + 2) 
			&& (float)SCALE_MIN_IPAD * ((-OPTION_VIBRATION_HEIGHT) / 2 - 2) < dy && dy < (float)SCALE_MIN_IPAD * (OPTION_VIBRATION_HEIGHT / 2 + 2)) {
            return enum_option_vibration;
        }
	}
	return enum_option_none;
}

- (void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	UITouch *touch = [touches anyObject];
	CGPoint location = [touch locationInView:[touch view]];
	location = [[CCDirector sharedDirector] convertToGL:location];
	
	_prevPoint = location;
	if (_menuState == enum_state_menu) {
		int menuType = [self getMenu:location.x Y:location.y];
		
		_selectKnife = MAX_KNIFE_INSCREEN;
		
		_isPressed = TRUE;
        _pastTime = LIMIT_PAST_TIME;
		_menuSelected = menuType;
		
		if (_menuSelected != enum_press_option) {
            return;
        }
		
		switch (_menuSelected) {
			case enum_press_play:
				((CCSprite*)[self getChildByTag:enum_tag_play]).opacity = PRESS_OPACITY;
				break;
			case enum_press_option:
				[GameSound effectPopOption];
				[self popOptionPanel];
				break;
			case enum_press_info:
				((CCSprite*)[self getChildByTag:enum_tag_info]).opacity = PRESS_OPACITY;
				break;
			case enum_press_other:
				((CCSprite*)[self getChildByTag:enum_tag_other]).opacity = PRESS_OPACITY;
				break;
			default:	
                break;
		}
	}
	else if (_menuState == enum_state_option) {
		int optionType  = [self getOption:location.x Y:location.y];
		if (optionType == enum_option_none) {
            return;
        }

		CCSprite *optionSprite = (CCSprite*)[self getChildByTag:enum_tag_option];
		switch (optionType) {
			case enum_option_cancel:	
				_menuState = enum_state_temp;
				[GameSound effectPushOption];
				[self pushOptionPanel];
				return;
				break;
			case enum_option_sound:
				if ([GameData getSound]) {
					[GameData setSound:NO];
					CCSprite *tmp = [[CCSprite alloc] initWithFile:@"main_option_sound_unselect@2x.png"];
					[((CCSprite*)[optionSprite getChildByTag:enum_tag_sound]) setTexture:tmp.texture];
					[((CCSprite*)[optionSprite getChildByTag:enum_tag_sound]) setTextureRect:tmp.textureRect];
					[tmp release];
				}
				else {
					[GameData setSound:YES];
					CCSprite *tmp = [[CCSprite alloc] initWithFile:@"main_option_sound_select@2x.png"];
					[((CCSprite*)[optionSprite getChildByTag:enum_tag_sound]) setTexture:tmp.texture];
					[((CCSprite*)[optionSprite getChildByTag:enum_tag_sound]) setTextureRect:tmp.textureRect];
					[GameSound effectBtnClick];
                    [tmp release];
				}
				break;
			case enum_option_music:
				if ([GameData getMusic]) {
					[GameData setMusic:NO];
					[GameSound stopBG];
					CCSprite *tmp = [[CCSprite alloc] initWithFile:@"main_option_music_unselect@2x.png"];
					[((CCSprite*)[optionSprite getChildByTag:enum_tag_music]) setTexture:tmp.texture];
                    [tmp release];
				}
				else {
					[GameData setMusic:YES];
					[GameSound playMainBG];
					CCSprite *tmp = [[CCSprite alloc] initWithFile:@"main_option_music_select@2x.png"];
					[((CCSprite*)[optionSprite getChildByTag:enum_tag_music]) setTexture:tmp.texture];
                    [tmp release];
				}
				break;
			case enum_option_vibration:
				if ([GameData getVibration]) {
					[GameData setVibration:NO];
					CCSprite *tmp = [[CCSprite alloc] initWithFile:@"main_option_vibration_unselect@2x.png"];
					[((CCSprite*)[optionSprite getChildByTag:enum_tag_vibration]) setTexture:tmp.texture];
					[GameSound effectBtnClick];
                    [tmp release];
				}
				else {
					[GameData setVibration:YES];
					[GameSound vib:1];
					CCSprite *tmp = [[CCSprite alloc] initWithFile:@"main_option_vibration_select@2x.png"];
					[((CCSprite*)[optionSprite getChildByTag:enum_tag_vibration]) setTexture:tmp.texture];
                    [tmp release];
					return;
				}
				break;
			default: return;
				break;
		}
	}
}

- (void) ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	UITouch *touch = [touches anyObject];
	CGPoint location = [touch locationInView:[touch view]];
	location = [[CCDirector sharedDirector] convertToGL:location];

	if (_menuState == enum_state_option) {
        return;
    }

	KnifeSprite *tmpKnife;
	for (int i = 0; i < MAX_KNIFE_INSCREEN; i++) {
		tmpKnife = (KnifeSprite*)[self getChildByTag:enum_tag_knifes + i];
		if (!tmpKnife._isActive) {
			_selectKnife = i;
			break;
		}
	}
	BOOL isDraw;
	if ((isDraw = [tmpKnife appearAction:_prevPoint.x Y1:_prevPoint.y X2:location.x Y2:location.y Color:enum_knife_0 Sound:YES])) {
        _prevPoint = location;
    }

	if (_pastTime > LIMIT_PAST_TIME && isDraw) {
		_pastTime = 0;
		_swardHissID = [GameSound effectSwardHiss];
	}
	_isEmptyKnife = YES;
	
	int menuType = [self getMenu:location.x Y:location.y];
	if (_menuSelected == enum_press_none) {
		_menuSelected = menuType;
	}
	else if (menuType != enum_press_none && menuType != _menuSelected) {
		_menuSelected = enum_press_double;
	}
	return;

	if (menuType != _menuSelected) {
		switch (_menuSelected) {
			case enum_press_play:	
                ((CCSprite*)[self getChildByTag:enum_tag_play]).opacity = RELEASE_OPACITY;	
                break;
			case enum_press_option:	
                ((CCSprite*)[self getChildByTag:enum_tag_option]).opacity = RELEASE_OPACITY;	
                break;
			case enum_press_info:		
                ((CCSprite*)[self getChildByTag:enum_tag_info]).opacity = RELEASE_OPACITY;	
                break;
			case enum_press_other:		
                ((CCSprite*)[self getChildByTag:enum_tag_other]).opacity = RELEASE_OPACITY;	
                break;
			default:			
                break;
		}
		_menuSelected = menuType;
		switch (_menuSelected) {
			case enum_press_play:
				((CCSprite*)[self getChildByTag:enum_tag_play]).opacity = PRESS_OPACITY;
                [GameSound effectBtnMenu];
				break;
			case enum_press_option:
				((CCSprite*)[self getChildByTag:enum_tag_option]).opacity = PRESS_OPACITY;
				[GameSound effectBtnClick];
				break;
			case enum_press_info:
				((CCSprite*)[self getChildByTag:enum_tag_info]).opacity = PRESS_OPACITY;
                [GameSound effectBtnMenu];
				break;
			case enum_press_other:
				((CCSprite*)[self getChildByTag:enum_tag_other]).opacity = PRESS_OPACITY;
                [GameSound effectBtnMenu];
				break;
			default:			
                break;
		}
	}
}

- (void) ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{	
    NSString *urlPageString = [NSString stringWithString:@"http://www.dreamapps.com.au/"];

	UITouch *touch = [touches anyObject];
	CGPoint location = [touch locationInView:[touch view]];
	location = [[CCDirector sharedDirector] convertToGL:location];
	
	if (_menuState == enum_state_option) {
        return;
    }

	KnifeSprite *tmpKnife;
	for (int i = 0; i < MAX_KNIFE_INSCREEN; i++) {
		tmpKnife = (KnifeSprite*)[self getChildByTag:enum_tag_knifes + i];
		if (!tmpKnife._isActive) {
			_selectKnife = i;
			break;
		}
	}
	[tmpKnife appearAction:_prevPoint.x Y1:_prevPoint.y X2:location.x Y2:location.y Color:enum_knife_0 Sound:YES];
	if (_isEmptyKnife == YES) {
        [GameSound stopSwardHiss:_swardHissID];
		[GameSound effectKnife];
		_isEmptyKnife = NO;
		_pastTime = 0;
	}
	
	if (_isPressed == 0) {
        return;
    }
	_isPressed = 0;

	int menuType = [self getMenu:location.x Y:location.y];
	if (_menuSelected == enum_press_none) {
		_menuSelected = menuType;
	}
	else if (menuType != enum_press_none && menuType != _menuSelected) {
		_menuSelected = enum_press_double;
	}
	switch (_menuSelected) {
		case enum_press_play:	
			((CCSprite*)[self getChildByTag:enum_tag_play]).visible = TRUE;
			((CCSprite*)[self getChildByTag:enum_tag_play]).opacity = PRESS_OPACITY;
//			((CCSprite*)[self getChildByTag:enum_tag_play_part1]).visible = TRUE;
//			((CCSprite*)[self getChildByTag:enum_tag_play_part1]).opacity = PRESS_OPACITY;
//			((CCSprite*)[self getChildByTag:enum_tag_play_part2]).visible = TRUE;
//			((CCSprite*)[self getChildByTag:enum_tag_play_part2]).opacity = PRESS_OPACITY;
            [GameSound effectBtnMenu];
			[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.6 scene:[PlayScene scene]]];
			break;
		case enum_press_option:	
            ((CCSprite*)[self getChildByTag:enum_tag_option]).opacity = RELEASE_OPACITY;	
            break;
		case enum_press_info:	
			((CCSprite*)[self getChildByTag:enum_tag_info]).opacity = RELEASE_OPACITY;
            [GameSound effectBtnMenu];

            if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlPageString]])
            {
                // there was an error trying to open the URL. for the moment we'll simply ignore it.
            }
			break;
		case enum_press_other:	
			((CCSprite*)[self getChildByTag:enum_tag_other]).opacity = RELEASE_OPACITY;
            [GameSound effectBtnMenu];
            
            if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlPageString]])
            {
                // there was an error trying to open the URL. for the moment we'll simply ignore it.
            }
			break;
		default:			
            break;
	}
	_menuSelected = enum_press_none;
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
    [GameSound unloadPopOption];
    [GameSound unloadPushOption];
    [GameSound unloadBtnClick];
    [GameSound unloadBtnMenu];
    [GameSound unloadSwardHiss];
    [GameSound unloadKnife];
    
    [_background release];
    [_playSprite release];
    [_playPart1Sprite release];
    [_playPart2Sprite release];
    [_optionSprite release];
    [_infoSprite release];
    [_otherSprite release];
    [_optionSoundSprite release];
    [_optionMusicSprite release];
    [_optionVibrationSprite release];
	[super dealloc];
}
@end


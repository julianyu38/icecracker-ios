//
//  OptionScene.h
//  Ice_Cracker
//
//  Created by mac on 5/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

enum {
	enum_pause_none = 0,
	enum_pause_continue,
	enum_pause_option,
	enum_pause_exit,
};

@interface PauseSprite : CCSprite {
    CCSprite *_btnContinueSprite;
    CCSprite *_btnOptionSprite;
    CCSprite *_btnExitSprite;
    CCSprite *_pauseLabelSprite;
    
	BOOL _isPressed;
	BOOL _isiPhone;
	int _btnSelected;
	float _appearTime;
}

+(id) sharedPauseSprite;

-(void) touchesBegan:(int)x Y:(int)y;
-(void) touchesMoved:(int)x Y:(int)y;
-(int) touchesEnded:(int)x Y:(int)y;
-(void) appearSprite:(float)duration;
-(void) disappearSprite:(float)duration;

@end

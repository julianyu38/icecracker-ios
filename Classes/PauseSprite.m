//
//  OptionScene.m
//  Ice_Cracker
//
//  Created by mac on 5/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PauseSprite.h"
#import "GameData.h"
#import "GameSound.h"
#import "IceSprite.h"

@implementation PauseSprite

#define BTN_CONTINUE_X			0
#define BTN_CONTINUE_Y			15
#define BTN_CONTINUE_WIDTH		154
#define BTN_CONTINUE_HEIGHT		30

#define BTN_OPTION_X			0
#define BTN_OPTION_Y			-35
#define BTN_OPTION_WIDTH		136
#define BTN_OPTION_HEIGHT		30

#define BTN_EXIT_X              0
#define BTN_EXIT_Y              -83
#define BTN_EXIT_WIDTH          194
#define BTN_EXIT_HEIGHT         30

#define PAUSE_LABEL_X			0
#define PAUSE_LABEL_Y			65

#define PRESS_OPACITY           255
#define RELEASE_OPACITY         200

enum {
	enum_tag_btn_continue = 0,
	enum_tag_btn_option,
	enum_tag_btn_exit,
	enum_tag_pause_label,
};

enum {
	enum_click_none = 0,
	enum_click_continue,
	enum_click_option,
	enum_click_exit,
};

+(id) sharedPauseSprite
{
	PauseSprite *sprite = [PauseSprite node];

	// return the scene
	return sprite;
}

-(id) init
{
	if( (self=[super init] )) {
		self.visible = FALSE;
		
		_isPressed = NO;
		_isiPhone = [GameData isiPhone];
		
		CCSprite *tmp = [[CCSprite alloc] initWithFile:@"panel_background@2x.png"];
		[self setTexture:tmp.texture];
		[self setTextureRect:tmp.textureRect];
        [tmp release];
        
		_btnContinueSprite = [[CCSprite alloc] initWithFile:@"panel_pause_continue_normal@2x.png"];
        _btnContinueSprite.position = ccp(self.contentSize.width / 2 + ((float)SCALE_MIN_IPAD * BTN_CONTINUE_X), self.contentSize.height / 2 + ((float)SCALE_MIN_IPAD * BTN_CONTINUE_Y));
		[self addChild:_btnContinueSprite z:enum_tag_btn_continue tag:enum_tag_btn_continue];
		
		_btnOptionSprite = [[CCSprite alloc] initWithFile:@"panel_pause_options_normal@2x.png"];
        _btnOptionSprite.position = ccp(self.contentSize.width / 2 + ((float)SCALE_MIN_IPAD * BTN_OPTION_X), self.contentSize.height / 2 + ((float)SCALE_MIN_IPAD * BTN_OPTION_Y));
		[self addChild:_btnOptionSprite z:enum_tag_btn_option tag:enum_tag_btn_option];
		
		_btnExitSprite = [[CCSprite alloc] initWithFile:@"panel_pause_mainmenu_normal@2x.png"];
        _btnExitSprite.position = ccp(self.contentSize.width / 2 + ((float)SCALE_MIN_IPAD * BTN_EXIT_X), self.contentSize.height / 2 + ((float)SCALE_MIN_IPAD * BTN_EXIT_Y));
		[self addChild:_btnExitSprite z:enum_tag_btn_exit tag:enum_tag_btn_exit];
		
		_pauseLabelSprite = [[CCSprite alloc] initWithFile:@"panel_title_pause@2x.png"];
        _pauseLabelSprite.position = ccp(self.contentSize.width / 2 + ((float)SCALE_MIN_IPAD * PAUSE_LABEL_X), self.contentSize.height / 2 + ((float)SCALE_MIN_IPAD * PAUSE_LABEL_Y));
		[self addChild:_pauseLabelSprite z:enum_tag_pause_label tag:enum_tag_pause_label];
		
		self.opacity = 0;
		_btnContinueSprite.opacity = 0;
		_btnOptionSprite.opacity = 0;
		_btnExitSprite.opacity = 0;
		_pauseLabelSprite.opacity = 0;
	}
	return self;
}

-(int) getClick:(int)x Y:(int)y
{
	int dx, dy;
	dx = x - BTN_CONTINUE_X;
	dy = y - BTN_CONTINUE_Y;
	if (-BTN_CONTINUE_WIDTH / 2 < dx && dx < BTN_CONTINUE_WIDTH / 2 && -BTN_CONTINUE_HEIGHT / 2 < dy && dy < BTN_CONTINUE_HEIGHT / 2) {
        return enum_click_continue;
    }
	dx = x - BTN_OPTION_X;
	dy = y - BTN_OPTION_Y;
	if (-BTN_OPTION_WIDTH / 2 < dx && dx < BTN_OPTION_WIDTH / 2 && -BTN_OPTION_HEIGHT / 2 < dy && dy < BTN_OPTION_HEIGHT / 2) {
        return enum_click_option;
    }
	dx = x - BTN_EXIT_X;
	dy = y - BTN_EXIT_Y;
	if (-BTN_EXIT_WIDTH / 2 < dx && dx < BTN_EXIT_WIDTH / 2 && -BTN_EXIT_HEIGHT / 2 < dy && dy < BTN_EXIT_HEIGHT / 2) {
        return enum_click_exit;
    }
	
	return enum_click_none;
}

-(void) touchesBegan:(int)x Y:(int)y
{
    CCSprite *tmp;
	x -= self.position.x;
	y -= self.position.y;
	if (!_isiPhone) {
		x = (float)x / SCALE_MIN_IPAD;
		y = (float)y / SCALE_MIN_IPAD;
	}
	
	int touchClick = [self getClick:x Y:y];
	
	if (touchClick == enum_click_none) {
        return;
    }
	
	[GameSound effectBtnClick];
	_btnSelected = touchClick;
	switch (_btnSelected) {
		case enum_click_continue:
            tmp = [[CCSprite alloc] initWithFile:@"panel_pause_continue_press@2x.png"];
            [((CCSprite*)[self getChildByTag:enum_tag_btn_continue]) setTexture:tmp.texture];
            [((CCSprite*)[self getChildByTag:enum_tag_btn_continue]) setTextureRect:tmp.textureRect];
            [tmp release];
            ((CCSprite*)[self getChildByTag:enum_tag_btn_continue]).opacity = PRESS_OPACITY;
            break;
		case enum_click_option:	
            tmp = [[CCSprite alloc] initWithFile:@"panel_pause_options_press@2x.png"];
            [((CCSprite*)[self getChildByTag:enum_tag_btn_option]) setTexture:tmp.texture];
            [((CCSprite*)[self getChildByTag:enum_tag_btn_option]) setTextureRect:tmp.textureRect];
            [tmp release];
            ((CCSprite*)[self getChildByTag:enum_tag_btn_option]).opacity = PRESS_OPACITY;
            break;
		case enum_click_exit:		
            tmp = [[CCSprite alloc] initWithFile:@"panel_pause_mainmenu_press@2x.png"];
            [((CCSprite*)[self getChildByTag:enum_tag_btn_exit]) setTexture:tmp.texture];
            [((CCSprite*)[self getChildByTag:enum_tag_btn_exit]) setTextureRect:tmp.textureRect];
            [tmp release];
            ((CCSprite*)[self getChildByTag:enum_tag_btn_exit]).opacity = PRESS_OPACITY;
            break;
		default:			
            break;
	}
	_isPressed = YES;
}

-(void) touchesMoved:(int)x Y:(int)y
{
	if (!_isPressed) return;
	x -= self.position.x;
	y -= self.position.y;
	if (!_isiPhone) {
		x = (float)x / SCALE_MIN_IPAD;
		y = (float)y / SCALE_MIN_IPAD;
	}
	
	int touchClick = [self getClick:x Y:y];
	
	if (touchClick == _btnSelected) {
        return;
    }
	
	if (touchClick != enum_click_none) {
        [GameSound effectBtnClick];
    }
	switch (_btnSelected) {
		case enum_click_continue:	
            ((CCSprite*)[self getChildByTag:enum_tag_btn_continue]).opacity = RELEASE_OPACITY;	
            break;
		case enum_click_option:	
            ((CCSprite*)[self getChildByTag:enum_tag_btn_option]).opacity = RELEASE_OPACITY;	
            break;
		case enum_click_exit:		
            ((CCSprite*)[self getChildByTag:enum_tag_btn_exit]).opacity = RELEASE_OPACITY;		
            break;
		default:			
            break;
	}
	_btnSelected = touchClick;
	switch (_btnSelected) {
		case enum_click_continue:	
            ((CCSprite*)[self getChildByTag:enum_tag_btn_continue]).opacity = PRESS_OPACITY;	
            break;
		case enum_click_option:	
            ((CCSprite*)[self getChildByTag:enum_tag_btn_option]).opacity = PRESS_OPACITY;		
            break;
		case enum_click_exit:		
            ((CCSprite*)[self getChildByTag:enum_tag_btn_exit]).opacity = PRESS_OPACITY;		
            break;
		default:			
            break;
	}
}

-(int) touchesEnded:(int)x Y:(int)y
{
    CCSprite *tmp;
	if (!_isPressed) return enum_pause_none;
	_isPressed = NO;
	x -= self.position.x;
	y -= self.position.y;
	if (!_isiPhone) {
		x = (float)x / SCALE_MIN_IPAD;
		y = (float)y / SCALE_MIN_IPAD;
	}
	
	int touchClick = [self getClick:x Y:y];
	
	switch (touchClick) {
		case enum_click_continue:	
            tmp = [[CCSprite alloc] initWithFile:@"panel_pause_continue_normal@2x.png"];
            [((CCSprite*)[self getChildByTag:enum_tag_btn_continue]) setTexture:tmp.texture];
            [((CCSprite*)[self getChildByTag:enum_tag_btn_continue]) setTextureRect:tmp.textureRect];
            [tmp release];
            ((CCSprite*)[self getChildByTag:enum_tag_btn_continue]).opacity = RELEASE_OPACITY;
            return enum_pause_continue;
		case enum_click_option:
            tmp = [[CCSprite alloc] initWithFile:@"panel_pause_options_normal@2x.png"];
            [((CCSprite*)[self getChildByTag:enum_tag_btn_option]) setTexture:tmp.texture];
            [((CCSprite*)[self getChildByTag:enum_tag_btn_option]) setTextureRect:tmp.textureRect];
            [tmp release];
            ((CCSprite*)[self getChildByTag:enum_tag_btn_option]).opacity = RELEASE_OPACITY;
            return enum_pause_option;
		case enum_click_exit:		
            tmp = [[CCSprite alloc] initWithFile:@"panel_pause_mainmenu_normal@2x.png"];
            [((CCSprite*)[self getChildByTag:enum_tag_btn_exit]) setTexture:tmp.texture];
            [((CCSprite*)[self getChildByTag:enum_tag_btn_exit]) setTextureRect:tmp.textureRect];
            [tmp release];
            ((CCSprite*)[self getChildByTag:enum_tag_btn_exit]).opacity = RELEASE_OPACITY;
            return enum_pause_exit;
		default:			
            break;
	}
	return enum_pause_none;
}

-(void) appearSelf
{
	[self runAction:[CCFadeTo actionWithDuration:_appearTime opacity:PRESS_OPACITY]];
}

-(void) appearContinue
{
	[((CCSprite*)[self getChildByTag:enum_tag_btn_continue]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:RELEASE_OPACITY]];	
}

-(void) appearOption
{
	[((CCSprite*)[self getChildByTag:enum_tag_btn_option]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:RELEASE_OPACITY]];	
}

-(void) appearExit
{
	[((CCSprite*)[self getChildByTag:enum_tag_btn_exit]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:RELEASE_OPACITY]];	
}

-(void) appearLabel
{
	[((CCSprite*)[self getChildByTag:enum_tag_pause_label]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:PRESS_OPACITY]];	
}

-(void) appearSprite:(float)duration
{
	_appearTime = duration;
	[self runAction:[CCSequence actions:
					 [CCCallFunc actionWithTarget:self selector:@selector(appearSelf)],
					 [CCCallFunc actionWithTarget:self selector:@selector(appearLabel)],
					 [CCCallFunc actionWithTarget:self selector:@selector(appearContinue)],
					 [CCCallFunc actionWithTarget:self selector:@selector(appearOption)],
					 [CCCallFunc actionWithTarget:self selector:@selector(appearExit)],
					 nil]];
}

-(void) disappearSelf
{
	[self runAction:[CCFadeTo actionWithDuration:_appearTime opacity:0]];
}

-(void) disappearContinue
{
	[((CCSprite*)[self getChildByTag:enum_tag_btn_continue]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:0]];	
}

-(void) disappearOption
{
	[((CCSprite*)[self getChildByTag:enum_tag_btn_option]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:0]];	
}

-(void) disappearExit
{
	[((CCSprite*)[self getChildByTag:enum_tag_btn_exit]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:0]];	
}

-(void) disappearLabel
{
	[((CCSprite*)[self getChildByTag:enum_tag_pause_label]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:0]];	
}

-(void) disappearSprite:(float)duration
{
	_appearTime = duration;
	[self runAction:[CCSequence actions:
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearLabel)],
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearContinue)],
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearOption)],
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearExit)],
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearSelf)],
					 nil]];
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
    [GameSound unloadBtnClick];
    
    [_btnContinueSprite release];
    [_btnOptionSprite release];
    [_btnExitSprite release];
    [_pauseLabelSprite release];
	[super dealloc];
}
@end

//
//  OptionScene.h
//  Ice_Cracker
//
//  Created by mac on 5/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface PlayLifeSprite : CCSprite {
	int _life;
	int _lifeDetail;
	BOOL _isiPhone;
}

+(id) sharedPlayLifeSprite;

-(void) reset;
-(void) decLife;
-(void) decDetail;
-(BOOL) isFinish;
-(void)settingOpacity:(int)value;

@property(nonatomic) int _life;
@end

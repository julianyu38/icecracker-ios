//
//  OptionScene.m
//  Ice_Cracker
//
//  Created by mac on 5/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PlayLifeSprite.h"
#import "GameData.h"
#import "GameSound.h"

@implementation PlayLifeSprite

@synthesize _life = life;

enum {
	enum_tag_life1 = 0,
	enum_tag_life2,
	enum_tag_life3,
};

#define POS_1	-24
#define POS_2	0
#define POS_3	28

+(id) sharedPlayLifeSprite
{
	PlayLifeSprite *sprite = [PlayLifeSprite node];

	return sprite;
}

-(id) init
{
	if( (self=[super init] )) {
        _isiPhone = [GameData isiPhone];

		CCSprite *life1Sprite = [[CCSprite alloc] initWithFile:@"play_life_100@2x.png"];
		CCSprite *life2Sprite = [[CCSprite alloc] initWithFile:@"play_life_200@2x.png"];
		CCSprite *life3Sprite = [[CCSprite alloc] initWithFile:@"play_life_300@2x.png"];

        life1Sprite.position = ccp((float)SCALE_MIN_IPAD * POS_1, 0);
        life2Sprite.position = ccp((float)SCALE_MIN_IPAD * POS_2, 0);
        life3Sprite.position = ccp((float)SCALE_MIN_IPAD * POS_3, 0);

		[self addChild:life1Sprite z:enum_tag_life1 tag:enum_tag_life1];
		[self addChild:life2Sprite z:enum_tag_life2 tag:enum_tag_life2];
		[self addChild:life3Sprite z:enum_tag_life3 tag:enum_tag_life3];
        
        [life1Sprite release];
        [life2Sprite release];
        [life3Sprite release];
	}
	return self;
}

-(void) refresh
{
	CCSprite *tmp;
	CCSprite *tmp1;
	CCSprite *tmp2;
    NSString *str;
	switch (life) {
		case 3:
            str = [NSString stringWithFormat:@"play_life_10%d", _lifeDetail];
            tmp = [[CCSprite alloc] initWithFile:[NSString stringWithFormat:@"%@@2x.%@", str, @"png"]];
			[((CCSprite*)[self getChildByTag:enum_tag_life1]) setTexture:tmp.texture];
			tmp1 = [[CCSprite alloc] initWithFile:@"play_life_203@2x.png"];
			[((CCSprite*)[self getChildByTag:enum_tag_life2]) setTexture:tmp1.texture];
			tmp2 = [[CCSprite alloc] initWithFile:@"play_life_303@2x.png"];
			[((CCSprite*)[self getChildByTag:enum_tag_life3]) setTexture:tmp2.texture];
            [tmp release];
            [tmp1 release];
            [tmp2 release];
			break;
		case 2:
			tmp = [[CCSprite alloc] initWithFile:@"play_life_100@2x.png"];
			[((CCSprite*)[self getChildByTag:enum_tag_life1]) setTexture:tmp.texture];
            str = [NSString stringWithFormat:@"play_life_20%d", _lifeDetail];
            tmp1 = [[CCSprite alloc] initWithFile:[NSString stringWithFormat:@"%@@2x.%@", str, @"png"]];
			[((CCSprite*)[self getChildByTag:enum_tag_life2]) setTexture:tmp1.texture];
			tmp2 = [[CCSprite alloc] initWithFile:@"play_life_303@2x.png"];
			[((CCSprite*)[self getChildByTag:enum_tag_life3]) setTexture:tmp2.texture];
            [tmp release];
            [tmp1 release];
            [tmp2 release];
			break;
		case 1:
			tmp = [[CCSprite alloc] initWithFile:@"play_life_100@2x.png"];
			[((CCSprite*)[self getChildByTag:enum_tag_life1]) setTexture:tmp.texture];
			tmp1 = [[CCSprite alloc] initWithFile:@"play_life_200@2x.png"];
			[((CCSprite*)[self getChildByTag:enum_tag_life2]) setTexture:tmp1.texture];
            str = [NSString stringWithFormat:@"play_life_30%d", _lifeDetail];
            tmp2 = [[CCSprite alloc] initWithFile:[NSString stringWithFormat:@"%@@2x.%@", str, @"png"]];
			[((CCSprite*)[self getChildByTag:enum_tag_life3]) setTexture:tmp2.texture];
            [tmp release];
            [tmp1 release];
            [tmp2 release];
			break;
		default:
			tmp = [[CCSprite alloc] initWithFile:@"play_life_100@2x.png"];
			[((CCSprite*)[self getChildByTag:enum_tag_life1]) setTexture:tmp.texture];
			tmp1 = [[CCSprite alloc] initWithFile:@"play_life_200@2x.png"];
			[((CCSprite*)[self getChildByTag:enum_tag_life2]) setTexture:tmp1.texture];
			tmp2 = [[CCSprite alloc] initWithFile:@"play_life_300@2x.png"];
			[((CCSprite*)[self getChildByTag:enum_tag_life3]) setTexture:tmp2.texture];
            [tmp release];
            [tmp1 release];
            [tmp2 release];
			break;
	}	
}

-(void) reset
{
	life = 3;
	_lifeDetail = 3;
	CCSprite *tmp = [[CCSprite alloc] initWithFile:@"play_life_103@2x.png"];
	[((CCSprite*)[self getChildByTag:enum_tag_life1]) setTexture:tmp.texture];
	CCSprite *tmp1 = [[CCSprite alloc] initWithFile:@"play_life_203@2x.png"];
	[((CCSprite*)[self getChildByTag:enum_tag_life2]) setTexture:tmp1.texture];
	CCSprite *tmp2 = [[CCSprite alloc] initWithFile:@"play_life_303@2x.png"];
	[((CCSprite*)[self getChildByTag:enum_tag_life3]) setTexture:tmp2.texture];
    [tmp release];
    [tmp1 release];
    [tmp2 release];
}

-(void) decLife
{
	life--;
	if (life < 0) {
		life = 0;
	}
	if (life == 0) {
        _lifeDetail = 0; 
    }
	[self refresh];
}

-(void) decDetail
{
	_lifeDetail--;
	if (_lifeDetail == 0) {
		_lifeDetail = 3;
		life--;
		if (life <= 0) {
			life = 0;
			_lifeDetail = 0;
		}
	}
	[self refresh];
}

-(BOOL) isFinish
{
	if (life == 0) {
        return TRUE;
    }
	else {
        return FALSE;
    }
}

-(void)settingOpacity:(int)value
{
	self.opacity = value;
	((CCSprite*)[self getChildByTag:enum_tag_life1]).opacity = value;
	((CCSprite*)[self getChildByTag:enum_tag_life2]).opacity = value;
	((CCSprite*)[self getChildByTag:enum_tag_life3]).opacity = value;
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)

	// don't forget to call "super dealloc"
	[super dealloc];
}
@end

//
//  OptionScene.h
//  Ice_Cracker
//
//  Created by mac on 5/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

enum {
	enum_play_option_none = 0,
	enum_play_option_exit,
};


@interface PlayOptionSprite : CCSprite {
    CCSprite *_btnSoundSprite;
    CCSprite *_btnMusicSprite;
    CCSprite *_btnVibrationSprite;
    CCSprite *_btnExitSprite;
    CCSprite *_playOptionLabelSprite;
    
	BOOL _isPressed;
	BOOL _isiPhone;
	int _btnSelected;
	float _appearTime;
}

+(id) sharedPlayOptionSprite;

-(void) touchesBegan:(int)x Y:(int)y;
-(void) touchesMoved:(int)x Y:(int)y;
-(int) touchesEnded:(int)x Y:(int)y;

-(void) appearSprite:(float)duration;
-(void) disappearSprite:(float)duration;
@end

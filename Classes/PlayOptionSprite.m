//
//  OptionScene.m
//  Ice_Cracker
//
//  Created by mac on 5/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PlayOptionSprite.h"
#import "GameData.h"
#import "GameSound.h"
#import "IceSprite.h"

@implementation PlayOptionSprite

#define BTN_SOUND_X				0
#define BTN_SOUND_Y				15
#define BTN_SOUND_WIDTH			110
#define BTN_SOUND_HEIGHT		27

#define BTN_MUSIC_X				0
#define BTN_MUSIC_Y				-35
#define BTN_MUSIC_WIDTH			100
#define BTN_MUSIC_HEIGHT		29

#define BTN_VIBRATION_X			0
#define BTN_VIBRATION_Y			-85
#define BTN_VIBRATION_WIDTH		168
#define BTN_VIBRATION_HEIGHT	29

#define BTN_EXIT_X				0
#define BTN_EXIT_Y				-138
#define BTN_EXIT_R				40

#define PLAY_OPTION_LABEL_X		0
#define PLAY_OPTION_LABEL_Y		58

#define PRESS_OPACITY           255
#define RELEASE_OPACITY         200

enum {
	enum_tag_btn_sound = 0,
	enum_tag_btn_music,
	enum_tag_btn_vibration,
	enum_tag_btn_exit,
	enum_tag_play_option_label,
};

enum {
	enum_click_none = 0,
	enum_click_sound,
	enum_click_music,
	enum_click_vibration,
	enum_click_exit,
};

+(id) sharedPlayOptionSprite
{
	PlayOptionSprite *sprite = [PlayOptionSprite node];

	// return the scene
	return sprite;
}

-(id) init
{
	if( (self=[super init] )) {
		self.visible = FALSE;
		
		_isPressed = NO;
		_isiPhone = [GameData isiPhone];
		
		CCSprite *tmp = [[CCSprite alloc] initWithFile:@"panel_background@2x.png"];
		[self setTexture:tmp.texture];
		[self setTextureRect:tmp.textureRect];
        [tmp release];

		if ([GameData getSound]) {
            _btnSoundSprite = [[CCSprite alloc] initWithFile:@"panel_option_sound_select@2x.png"];
        }
		else {
            _btnSoundSprite = [[CCSprite alloc] initWithFile:@"panel_option_sound_unselect@2x.png"];
        }
        _btnSoundSprite.position = ccp(self.contentSize.width / 2 + ((float)SCALE_MIN_IPAD * BTN_SOUND_X), self.contentSize.height / 2 + ((float)SCALE_MIN_IPAD * BTN_SOUND_Y));
		[self addChild:_btnSoundSprite z:enum_tag_btn_sound tag:enum_tag_btn_sound];
		
		if ([GameData getMusic]) {
            _btnMusicSprite = [[CCSprite alloc] initWithFile:@"panel_option_music_select@2x.png"];
        }
		else {
            _btnMusicSprite = [[CCSprite alloc] initWithFile:@"panel_option_music_unselect@2x.png"];
        }
        _btnMusicSprite.position = ccp(self.contentSize.width / 2 + ((float)SCALE_MIN_IPAD * BTN_MUSIC_X), self.contentSize.height / 2 + ((float)SCALE_MIN_IPAD * BTN_MUSIC_Y));
		[self addChild:_btnMusicSprite z:enum_tag_btn_music tag:enum_tag_btn_music];
		
		if ([GameData getVibration]) {
            _btnVibrationSprite = [[CCSprite alloc] initWithFile:@"panel_option_vibration_select@2x.png"];
        }
		else {
            _btnVibrationSprite = [[CCSprite alloc] initWithFile:@"panel_option_vibration_unselect@2x.png"];
        }
        _btnVibrationSprite.position = ccp(self.contentSize.width / 2 + ((float)SCALE_MIN_IPAD * BTN_VIBRATION_X), self.contentSize.height / 2 + ((float)SCALE_MIN_IPAD * BTN_VIBRATION_Y));
        if (![[UIDevice currentDevice].model isEqualToString:@"iPhone"]) {
            _btnVibrationSprite.visible = FALSE;
        }
        else {
            _btnVibrationSprite.visible = TRUE;
        }
		[self addChild:_btnVibrationSprite z:enum_tag_btn_vibration tag:enum_tag_btn_vibration];
		
		_btnExitSprite = [[CCSprite alloc] initWithFile:@"panel_btn_back_normal@2x.png"];
        _btnExitSprite.position = ccp(self.contentSize.width / 2 + ((float)SCALE_MIN_IPAD * BTN_EXIT_X), self.contentSize.height / 2 + ((float)SCALE_MIN_IPAD * BTN_EXIT_Y));
		[self addChild:_btnExitSprite z:enum_tag_btn_exit tag:enum_tag_btn_exit];
		
		_playOptionLabelSprite = [[CCSprite alloc] initWithFile:@"panel_title_option@2x.png"];
        _playOptionLabelSprite.position = ccp(self.contentSize.width / 2 + ((float)SCALE_MIN_IPAD * PLAY_OPTION_LABEL_X), self.contentSize.height / 2 + ((float)SCALE_MIN_IPAD * PLAY_OPTION_LABEL_Y));
		[self addChild:_playOptionLabelSprite z:enum_tag_play_option_label tag:enum_tag_play_option_label];
		
		self.opacity = 0;
		_btnSoundSprite.opacity = 0;
		_btnMusicSprite.opacity = 0;
		_btnVibrationSprite.opacity = 0;
		_btnExitSprite.opacity = 0;
		_playOptionLabelSprite.opacity = 0;
	}
	return self;
}

-(int) getClick:(int)x Y:(int)y
{
	int dx, dy;

	dx = x - BTN_SOUND_X;
	dy = y - BTN_SOUND_Y;
	if (-BTN_SOUND_WIDTH / 2 < dx && dx < BTN_SOUND_WIDTH / 2 && -BTN_SOUND_HEIGHT / 2 < dy && dy < BTN_SOUND_HEIGHT / 2) {
        return enum_click_sound;
    }

	dx = x - BTN_MUSIC_X;
	dy = y - BTN_MUSIC_Y;
	if (-BTN_MUSIC_WIDTH / 2 < dx && dx < BTN_MUSIC_WIDTH / 2 && -BTN_MUSIC_HEIGHT / 2 < dy && dy < BTN_MUSIC_HEIGHT / 2) {
        return enum_click_music;
    }
	
	dx = x - BTN_VIBRATION_X;
	dy = y - BTN_VIBRATION_Y;
	if (-BTN_VIBRATION_WIDTH / 2 < dx && dx < BTN_VIBRATION_WIDTH / 2 && -BTN_VIBRATION_HEIGHT / 2 < dy && dy < BTN_VIBRATION_HEIGHT / 2) {
        return enum_click_vibration;
    }
	
	dx = x - BTN_EXIT_X;
	dy = y - BTN_EXIT_Y;
	if (sqrt(dx * dx + dy * dy) < BTN_EXIT_R) {
        return enum_click_exit;
    }
	
	return enum_click_none;
}

-(void) touchesBegan:(int)x Y:(int)y
{
	x -= self.position.x;
	y -= self.position.y;
	if (!_isiPhone) {
		x = (float)x / SCALE_MIN_IPAD;
		y = (float)y / SCALE_MIN_IPAD;
	}
	
	int touchClick = [self getClick:x Y:y];
	
	if (touchClick == enum_click_none) {
        return;
    }
	
	if (touchClick == enum_click_exit)	{
        _btnSelected = enum_click_exit;
    }
	
	switch (touchClick) {
		case enum_click_sound:	
			if ([GameData getSound]) {
				[GameData setSound:NO];
				CCSprite *tmp = [[CCSprite alloc] initWithFile:@"panel_option_sound_unselect@2x.png"];
				[((CCSprite*)[self getChildByTag:enum_tag_btn_sound]) setTexture:tmp.texture];
				[((CCSprite*)[self getChildByTag:enum_tag_btn_sound]) setTextureRect:tmp.textureRect];
                [tmp release];
			}
			else {
				[GameData setSound:YES];
				CCSprite *tmp = [[CCSprite alloc] initWithFile:@"panel_option_sound_select@2x.png"];
				[((CCSprite*)[self getChildByTag:enum_tag_btn_sound]) setTexture:tmp.texture];
				[((CCSprite*)[self getChildByTag:enum_tag_btn_sound]) setTextureRect:tmp.textureRect];
                [tmp release];
			}
			break;
		case enum_click_music:	
			if ([GameData getMusic]) {
				[GameData setMusic:NO];
				[GameSound stopBG];
				CCSprite *tmp = [[CCSprite alloc] initWithFile:@"panel_option_music_unselect@2x.png"];
				[((CCSprite*)[self getChildByTag:enum_tag_btn_music]) setTexture:tmp.texture];
				[((CCSprite*)[self getChildByTag:enum_tag_btn_music]) setTextureRect:tmp.textureRect];
                [tmp release];
			}
			else {
				[GameData setMusic:YES];
                [GameSound playGameBG];
				CCSprite *tmp = [[CCSprite alloc] initWithFile:@"panel_option_music_select@2x.png"];
				[((CCSprite*)[self getChildByTag:enum_tag_btn_music]) setTexture:tmp.texture];
				[((CCSprite*)[self getChildByTag:enum_tag_btn_music]) setTextureRect:tmp.textureRect];
                [tmp release];
			}
			break;
		case enum_click_vibration:
			if ([GameData getVibration]) {
				[GameData setVibration:NO];
				CCSprite *tmp = [[CCSprite alloc] initWithFile:@"panel_option_vibration_unselect@2x.png"];
				[((CCSprite*)[self getChildByTag:enum_tag_btn_vibration]) setTexture:tmp.texture];
				[((CCSprite*)[self getChildByTag:enum_tag_btn_vibration]) setTextureRect:tmp.textureRect];
                [tmp release];
			}
			else {
				[GameData setVibration:YES];
				[GameSound vib:1];
				CCSprite *tmp = [[CCSprite alloc] initWithFile:@"panel_option_vibration_select@2x.png"];
				[((CCSprite*)[self getChildByTag:enum_tag_btn_vibration]) setTexture:tmp.texture];
				[((CCSprite*)[self getChildByTag:enum_tag_btn_vibration]) setTextureRect:tmp.textureRect];
                [tmp release];
			}
			break;
		case enum_click_exit:	
			((CCSprite*)[self getChildByTag:enum_tag_btn_exit]).opacity = PRESS_OPACITY;
			_isPressed = YES;
			break;
		default:			
            break;
	}
	[GameSound effectBtnClick];
}

-(void) touchesMoved:(int)x Y:(int)y
{
	if (!_isPressed) return;
	x -= self.position.x;
	y -= self.position.y;
	if (!_isiPhone) {
		x = (float)x / SCALE_MIN_IPAD;
		y = (float)y / SCALE_MIN_IPAD;
	}
	
	int touchClick = [self getClick:x Y:y];
	
	if (touchClick != enum_click_exit)	{
        touchClick = enum_click_none;
    }
	if (touchClick == _btnSelected) {
        return;
    }
	
	if (touchClick != enum_click_none) {
        [GameSound effectBtnClick];
    }
	
	switch (_btnSelected) {
		case enum_click_exit:		
            ((CCSprite*)[self getChildByTag:enum_tag_btn_exit]).opacity = RELEASE_OPACITY;		
            break;
		default:			
            break;
	}
	
	_btnSelected = enum_click_exit;

	switch (_btnSelected) {
		case enum_click_exit:		
            ((CCSprite*)[self getChildByTag:enum_tag_btn_exit]).opacity = PRESS_OPACITY;		
            break;
		default:			
            break;
	}
}

-(int) touchesEnded:(int)x Y:(int)y
{
	if (!_isPressed) return enum_play_option_none;
	_isPressed = NO;
	x -= self.position.x;
	y -= self.position.y;
	if (!_isiPhone) {
		x = (float)x / SCALE_MIN_IPAD;
		y = (float)y / SCALE_MIN_IPAD;
	}
	
	int touchClick = [self getClick:x Y:y];
	
	switch (touchClick) {
		case enum_click_exit:		
            ((CCSprite*)[self getChildByTag:enum_tag_btn_exit]).opacity = RELEASE_OPACITY;		
            return enum_play_option_exit;
		default:			
            break;
	}
	return enum_play_option_none;
}

-(void) appearSelf
{
	[self runAction:[CCFadeTo actionWithDuration:_appearTime opacity:PRESS_OPACITY]];
}

-(void) appearSound
{
	[((CCSprite*)[self getChildByTag:enum_tag_btn_sound]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:PRESS_OPACITY]];	
}

-(void) appearMusic
{
	[((CCSprite*)[self getChildByTag:enum_tag_btn_music]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:PRESS_OPACITY]];	
}

-(void) appearVibration
{
	[((CCSprite*)[self getChildByTag:enum_tag_btn_vibration]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:PRESS_OPACITY]];	
}

-(void) appearExit
{
	[((CCSprite*)[self getChildByTag:enum_tag_btn_exit]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:RELEASE_OPACITY]];	
}

-(void) appearLabel
{
	[((CCSprite*)[self getChildByTag:enum_tag_play_option_label]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:PRESS_OPACITY]];	
}

-(void) appearSprite:(float)duration
{
	_appearTime = duration;
	[self runAction:[CCSequence actions:
					 [CCCallFunc actionWithTarget:self selector:@selector(appearSelf)],
					 [CCCallFunc actionWithTarget:self selector:@selector(appearLabel)],
					 [CCCallFunc actionWithTarget:self selector:@selector(appearExit)],
					 [CCCallFunc actionWithTarget:self selector:@selector(appearSound)],
					 [CCCallFunc actionWithTarget:self selector:@selector(appearMusic)],
					 [CCCallFunc actionWithTarget:self selector:@selector(appearVibration)],
					 nil]];
}

-(void) disappearSelf
{
	[self runAction:[CCFadeTo actionWithDuration:_appearTime opacity:0]];
}

-(void) disappearSound
{
	[((CCSprite*)[self getChildByTag:enum_tag_btn_sound]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:0]];	
}

-(void) disappearMusic
{
	[((CCSprite*)[self getChildByTag:enum_tag_btn_music]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:0]];	
}

-(void) disappearVibration
{
	[((CCSprite*)[self getChildByTag:enum_tag_btn_vibration]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:0]];	
}

-(void) disappearExit
{
	[((CCSprite*)[self getChildByTag:enum_tag_btn_exit]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:0]];	
}

-(void) disappearLabel
{
	[((CCSprite*)[self getChildByTag:enum_tag_play_option_label]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:0]];	
}

-(void) disappearSprite:(float)duration
{
	_appearTime = duration;
	[self runAction:[CCSequence actions:
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearExit)],
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearLabel)],
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearSound)],
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearMusic)],
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearVibration)],
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearSelf)],
					 nil]];
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
    [GameSound unloadBtnClick];

    [_btnSoundSprite release];
    [_btnMusicSprite release];
    [_btnVibrationSprite release];
    [_btnExitSprite release];
    [_playOptionLabelSprite release];
	[super dealloc];
}
@end

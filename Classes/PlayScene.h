//
//  GameScene.h
//  Ice_Cracker
//
//  Created by mac on 5/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

#define MAX_KNIFE_COLOR		4

@interface PlayScene : CCLayer {
    CCSprite *_backSprite;
    CCSprite *_pauseBtnSprite;
    CCSprite *_pengiunLife1;
    CCSprite *_pengiunLife2;
    CCSprite *_pengiunLife3;

	int _gameState;
	BOOL _isiPhone;
	
	BOOL _isPressed;
	int _pressedState;
	int _pressedStateShadow;
	
	BOOL _isClicked;
	
	BOOL _isValidKnifeColor[MAX_KNIFE_COLOR];
	CGPoint _prevPoint[MAX_KNIFE_COLOR];
	CGPoint _prevDrawPoint[MAX_KNIFE_COLOR];
	float _pastTime[MAX_KNIFE_COLOR];
	
	int _score;
    int _decreasePenguinLifeCount;
	
	ccTime _intervalThresholdTime;
	ccTime _intervalTime;
	ccTime _gameTime;
	ccTime _restTime;
	BOOL _isEmptySlash[MAX_KNIFE_COLOR];
	int _iceComboCount[MAX_KNIFE_COLOR];
	int _iceComboX;
	int _iceComboY;
    
    /* Tune Difficulty level*/
    /* if increase bombAppearNum then difficult */
    /* if bombAppearTime is small then difficult, bombAppearTime is big then easy*/
    int _penguinAppearNum;
    float _penguinAppearTime;
}

+(id) scene;

- (void) appearPauseSprite;
- (void) appearOptionSprite;
- (void) appearScoreSprite;
- (void) pauseGame;
- (void) resumeGame;
- (void) pausing;
- (void) resuming;
- (void) stopGame;
- (void) disappearPauseSprite;
- (void) disappearOptionSprite;
- (void) disappearScoreSprite;

- (void) changeStatePlay;
- (void) changeStatePause;
- (void) changeStateOption;
- (void) changeStateScore;

-(void) createExplosion:(int)x Y:(int)y;
- (void) slashIces:(int)x1 Y1:(int)y1 X2:(int)x2 Y2:(int)y2 Color:(int)knifeColor;
- (void) throwIces;

- (void) reset;

- (void) initGame;
- (int) getRandomValue:(int)val_1 Last:(int)val_2;

- (void)updateNow:(ccTime)deltaTime;

- (void) addScore:(int)addedScore;
- (void) decreasePenguinLife;
@end

//
//  GameScene.m
//  Ice_Cracker
//
//  Created by mac on 5/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PlayScene.h"

#import "GameData.h"
#import "GameSound.h"
#import "IceSprite.h"
#import "KnifeSprite.h"
#import "PlayScoreSprite.h"
#import "PlayHighScoreSprite.h"
#import "PlayTimerSprite.h"
#import "PlayLifeSprite.h"
#import "ScoreSprite.h"
#import "PauseSprite.h"
#import "PlayOptionSprite.h"
#import "ComboSprite.h"
#import "MainMenuScene.h"

BOOL GLOBAL_gamePlaying;

@implementation PlayScene

enum {
	enum_tag_play_background = 0,
	enum_tag_play_score,
	enum_tag_play_high_score,
	enum_tag_play_timer,
	enum_tag_play_pause_btn,
    enum_tag_play_penguin_life_1,
    enum_tag_play_penguin_life_2,
    enum_tag_play_penguin_life_3,
	
	enum_tag_pause,
	enum_tag_score,
	enum_tag_option,
	
	enum_tag_effect = 20,
	enum_tag_ices = 120,
	enum_tag_knifes = 200,
	enum_tag_knife_trace = 300,
	enum_tag_combo = 400,
};

enum {
	enum_game_state_play = 0,
	enum_game_state_pause,
	enum_game_state_option,
	enum_game_state_score,
	enum_game_state_tmp,
};

enum {
	enum_play_press_none = 0,
	enum_play_press_pause_btn,
	enum_play_press_board,
};

enum {
	enum_pause_press_none = 0,
	enum_pause_press_continue_btn,
	enum_pause_press_option_btn,
	enum_pause_press_exit_btn,
};

enum {
	enum_option_press_none = 0,
	enum_option_press_back_btn,
	enum_option_press_sound_btn,
	enum_option_press_music_btn,
	enum_option_press_vibration_btn,
};

enum {
	enum_score_press_replay_btn = 0,
	enum_score_press_exit_btn,
};

#define MAX_ICE_INGAME          20
#define MAX_KNIFE_INSCREEN		50
#define MAX_ICE_INSCREEN		5
#define MAX_BONUS_INSCREEN		5

#define PANEL_X                  0.0f
#define PANEL_Y                  16.0f

#define PLAY_TIMER_X			-42
#define PLAY_TIMER_Y			-18

#define PLAY_LIFE_X				PLAY_TIMER_X
#define PLAY_LIFE_Y				PLAY_TIMER_Y

#define PLAY_SCORE_X			41
#define PLAY_SCORE_Y			-18

#define PLAY_HIGHSCORE_X		30
#define PLAY_HIGHSCORE_Y		-39

#define PAUSE_BTN_X				22
#define PAUSE_BTN_Y				18
#define PAUSE_BTN_R				35

#define PENGUIN_LIFE_1_X        316
#define PENGUIN_LIFE_1_Y        305

#define PENGUIN_LIFE_2_X        346
#define PENGUIN_LIFE_2_Y        305

#define PENGUIN_LIFE_3_X        376
#define PENGUIN_LIFE_3_Y        305

#define PRESS_OPACITY			255
#define RELEASE_OPACITY			200
#define SHADOW_OPACITY			80

#define GAME_TIME				120
#define APPEAR_TIME				(0.8)

#define TOUCH_MOVING_MIN		5
#define LIMIT_PAST_TIME			1.0f
#define ALPHA_CHANGE_MIN		25

#define INTERVAL_MAX			2.0f
#define INTERVAL_MIN			1.5f

#define DELAY_VARIENCE			0.8f

#define KNIFE_WIDTH				12


+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	PlayScene *layer = [PlayScene node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

-(id) init
{
	if ((self=[super init])) {
		
		self.isTouchEnabled = YES;
		
		// ask director the the window size
		_isiPhone = [GameData isiPhone];
		CGSize size = [[CCDirector sharedDirector] winSize];
		
		_backSprite = [[CCSprite alloc] initWithFile:@"play_background@2x.png"];
		_pauseBtnSprite = [[CCSprite alloc] initWithFile:@"play_btn_pause_normal@2x.png"];
        _pengiunLife1 = [[CCSprite alloc] initWithFile:@"penguin_life@2x.png"];
        _pengiunLife2 = [[CCSprite alloc] initWithFile:@"penguin_life@2x.png"];
        _pengiunLife3 = [[CCSprite alloc] initWithFile:@"penguin_life@2x.png"];
		PlayTimerSprite *playTimerSprite = [PlayTimerSprite sharedPlayTimerSprite];
		PlayScoreSprite *playScoreSprite = [PlayScoreSprite sharedPlayScoreSprite];
		PlayHighScoreSprite *playHighScoreSprite = [PlayHighScoreSprite sharedPlayHighScoreSprite];
		
		PauseSprite *pauseSprite = [PauseSprite sharedPauseSprite];
		PlayOptionSprite *playOptionSprite = [PlayOptionSprite sharedPlayOptionSprite];
		ScoreSprite *scoreSprite = [ScoreSprite sharedScoreSprite];
		
		for (int i = 0; i < MAX_KNIFE_INSCREEN; i++) {
			KnifeSprite *knifeSprite = [KnifeSprite sharedKnifeSprite];
			[self addChild:knifeSprite z:enum_tag_knifes + i tag:enum_tag_knifes + i];
		}
		
		for (int i = 0; i < MAX_ICE_INGAME; i++) {
			IceSprite *ice = [IceSprite sharedIceSprite];
			[self addChild:ice z:enum_tag_ices + i tag:enum_tag_ices + i];
            if (_isiPhone) {
                ice.scale = .5f;
            }
		}
		
		for (int i = 0; i < MAX_BONUS_INSCREEN; i++) {
			ComboSprite *comboSprite = [ComboSprite sharedComboSprite];
			[self addChild:comboSprite z:enum_tag_combo + i tag:enum_tag_combo + i];
			if (_isiPhone) {
                comboSprite.scale = .5f;
            }
		}
		
		if (_isiPhone) {
			_backSprite.position = ccp(size.width / 2, size.height / 2);
            [GameData setScaleIphone:_backSprite];
			_pauseBtnSprite.position = ccp(PAUSE_BTN_X, PAUSE_BTN_Y);
            _pauseBtnSprite.scale = .5f;
            _pengiunLife1.position = ccp(PENGUIN_LIFE_1_X, PENGUIN_LIFE_1_Y-3);
            _pengiunLife1.scale = .5f;
            _pengiunLife2.position = ccp(PENGUIN_LIFE_2_X, PENGUIN_LIFE_2_Y-3);
            _pengiunLife2.scale = .5f;
            _pengiunLife3.position = ccp(PENGUIN_LIFE_3_X, PENGUIN_LIFE_3_Y-3);
            _pengiunLife3.scale = .5f;
			playTimerSprite.position = ccp(size.width + PLAY_TIMER_X, size.height + PLAY_TIMER_Y);
            playTimerSprite.scale = .5f;
			playScoreSprite.position = ccp(PLAY_SCORE_X, size.height + PLAY_SCORE_Y);
            playScoreSprite.scale = .5f;
			playHighScoreSprite.position = ccp(PLAY_HIGHSCORE_X, size.height + PLAY_HIGHSCORE_Y);
            playHighScoreSprite.scale = .5f;
			pauseSprite.position = ccp(size.width / 2 + PANEL_X, size.height / 2 + PANEL_Y);
            pauseSprite.scale = .5f;
			playOptionSprite.position = ccp(size.width / 2 + PANEL_X, size.height / 2 + PANEL_Y);
            playOptionSprite.scale = .5f;
			scoreSprite.position = ccp(size.width / 2 + PANEL_X, size.height / 2 + PANEL_Y);
            scoreSprite.scale = .5f;
		}
		else {
			_backSprite.position = ccp(size.width / 2, size.height / 2);
			_pauseBtnSprite.position = ccp((float)PAUSE_BTN_X * (float)SCALE_MIN_IPAD, (float)PAUSE_BTN_Y * (float)SCALE_MIN_IPAD);
            _pengiunLife1.position = ccp((float)PENGUIN_LIFE_1_X * (float)SCALE_MIN_IPAD, (float)PENGUIN_LIFE_1_Y * (float)SCALE_MAX_IPAD);
            _pengiunLife2.position = ccp((float)PENGUIN_LIFE_2_X * (float)SCALE_MIN_IPAD, (float)PENGUIN_LIFE_2_Y * (float)SCALE_MAX_IPAD);
            _pengiunLife3.position = ccp((float)PENGUIN_LIFE_3_X * (float)SCALE_MIN_IPAD, (float)PENGUIN_LIFE_3_Y * (float)SCALE_MAX_IPAD);
			playTimerSprite.position = ccp(size.width + (float)SCALE_MIN_IPAD * (float)PLAY_TIMER_X, size.height + (float)SCALE_MIN_IPAD * (float)PLAY_TIMER_Y);
			playScoreSprite.position = ccp((float)SCALE_MIN_IPAD * (float)PLAY_SCORE_X, size.height + (float)SCALE_MIN_IPAD * (float)PLAY_SCORE_Y);
			playHighScoreSprite.position = ccp((float)SCALE_MIN_IPAD * (float)PLAY_HIGHSCORE_X, size.height + (float)SCALE_MIN_IPAD * (float)PLAY_HIGHSCORE_Y);
			pauseSprite.position = ccp(size.width / 2 + (float)SCALE_MIN_IPAD * PANEL_X, size.height / 2 + (float)SCALE_MIN_IPAD * PANEL_Y);
			playOptionSprite.position = ccp(size.width / 2 + (float)SCALE_MIN_IPAD * PANEL_X, size.height / 2 + (float)SCALE_MIN_IPAD * PANEL_Y);
			scoreSprite.position = ccp(size.width / 2 + (float)SCALE_MIN_IPAD * PANEL_X, size.height / 2 + (float)SCALE_MIN_IPAD * PANEL_Y);
		}
		
		[self addChild:_backSprite z:enum_tag_play_background tag:enum_tag_play_background];
		[self addChild:_pauseBtnSprite z:enum_tag_play_pause_btn tag:enum_tag_play_pause_btn];
        [self addChild:_pengiunLife1 z:enum_tag_play_penguin_life_1 tag:enum_tag_play_penguin_life_1];
        [self addChild:_pengiunLife2 z:enum_tag_play_penguin_life_2 tag:enum_tag_play_penguin_life_2];
        [self addChild:_pengiunLife3 z:enum_tag_play_penguin_life_3 tag:enum_tag_play_penguin_life_3];
		[self addChild:playTimerSprite z:enum_tag_play_timer tag:enum_tag_play_timer];
		[self addChild:playScoreSprite z:enum_tag_play_score tag:enum_tag_play_score];
		[self addChild:playHighScoreSprite z:enum_tag_play_high_score tag:enum_tag_play_high_score];
		[self addChild:pauseSprite z:enum_tag_pause tag:enum_tag_pause];
		[self addChild:playOptionSprite z:enum_tag_option tag:enum_tag_option];
		[self addChild:scoreSprite z:enum_tag_score tag:enum_tag_score];
		
		_pauseBtnSprite.opacity = RELEASE_OPACITY;
		[self reset];
	}
	return self;
}

-(void) reset
{
	[self initGame];
	[self resumeGame];
}

-(void) initGame
{
	_gameTime = 0;
	_intervalTime = 0;
	_intervalThresholdTime = INTERVAL_MAX;
	_isPressed = NO;
	_score = 0;
    _penguinAppearNum = 1;
    _penguinAppearTime = 10.0f;
	PlayTimerSprite *playTimerSprite = (PlayTimerSprite*)[self getChildByTag:enum_tag_play_timer];
	PlayScoreSprite *playScoreSprite = (PlayScoreSprite*)[self getChildByTag:enum_tag_play_score];
	PlayHighScoreSprite *playHighScoreSprite = (PlayHighScoreSprite*)[self getChildByTag:enum_tag_play_high_score];
	
	for (int i = 0; i < MAX_KNIFE_COLOR; i++) {
		_isValidKnifeColor[i] = FALSE;
		_isEmptySlash[i] = TRUE;
		_iceComboCount[i] = 0;
	}

    _decreasePenguinLifeCount = 0;
    _pengiunLife1.visible = YES;
    _pengiunLife2.visible = YES;
    _pengiunLife3.visible = YES;

    playTimerSprite.visible = YES;
    [playTimerSprite initTimer];
    [playTimerSprite setTime:GAME_TIME];
    [GameSound stopBG];
    [GameSound playGameBG];

	[playHighScoreSprite setScore:[GameData getBestScore]];
	[playScoreSprite clearScore];
}

- (void) updateNow:(ccTime)deltaTime
{
    if (GLOBAL_displayPauseMenu) {
        if (_gameState != enum_game_state_tmp) {
			_gameState = enum_game_state_tmp;
            [self runAction:[CCSequence actions:
                             [CCCallFunc actionWithTarget:self selector:@selector(pauseGame)],
                             [CCCallFunc actionWithTarget:self selector:@selector(appearPauseSprite)],
                             [CCDelayTime actionWithDuration:APPEAR_TIME],
                             [CCCallFunc actionWithTarget:self selector:@selector(changeStatePause)],
                             nil]];
        }
        GLOBAL_displayPauseMenu = NO;
    }

	_gameTime += deltaTime;

	for (int i = 0; i < MAX_KNIFE_COLOR; i++) {
		if (!_isValidKnifeColor[i]) {
            continue;
        }
		_pastTime[i] += deltaTime;
		if (_pastTime[i] > LIMIT_PAST_TIME * 1.2f && _isEmptySlash[i] == FALSE ) {
			_pastTime[i] = LIMIT_PAST_TIME;
			_isEmptySlash[i] = TRUE;
			if (_iceComboCount[i] > 2) {
				ComboSprite *tmpComboSprite;
				for (int j = 0; j < MAX_BONUS_INSCREEN; j++) {
					tmpComboSprite = (ComboSprite*)[self getChildByTag:enum_tag_combo + j];
					if (!tmpComboSprite._isActive) {
                        break;
                    }
				}
				[tmpComboSprite appearEffect:(_iceComboCount[i] - 2) * 10 X:_iceComboX Y:_iceComboY];
				[self addScore:(_iceComboCount[i] - 2) * 10];
			}
			_iceComboCount[i] = 0;
		}
	}
	
    _intervalTime += deltaTime;
    _restTime = GAME_TIME - _gameTime;
    if (_restTime < 0) _restTime = 0;
    [((PlayTimerSprite*)[self getChildByTag:enum_tag_play_timer]) setTime:_restTime];
    if (_intervalTime >= _intervalThresholdTime) {
        _intervalTime -= _intervalThresholdTime;
        _intervalThresholdTime = (INTERVAL_MAX - INTERVAL_MIN) * (_penguinAppearTime - _gameTime) / _penguinAppearTime;
        if (_intervalThresholdTime < 0) {
            _intervalThresholdTime = 0;
        }
        _intervalThresholdTime += INTERVAL_MIN;
        
        [self throwIces];
    }
    
    if (_restTime == 0) {
        GLOBAL_gamePlaying = NO;
        if ([GameData getBestScore] < _score) {
            [GameData setBestScore:_score];
        }
        _gameState = enum_game_state_tmp;
        [((ScoreSprite*)[self getChildByTag:enum_tag_score]) setScore:_score];
        [GameSound effectGameEnd];
        [self runAction:[CCSequence actions:
                         [CCCallFunc actionWithTarget:self selector:@selector(pausing)],
                         [CCDelayTime actionWithDuration:1],
                         [CCCallFunc actionWithTarget:self selector:@selector(stopGame)],
                         [CCCallFunc actionWithTarget:self selector:@selector(appearScoreSprite)],
                         [CCDelayTime actionWithDuration:APPEAR_TIME],
                         [CCCallFunc actionWithTarget:self selector:@selector(changeStateScore)],
                         nil]];
        
    }
    else {
        GLOBAL_gamePlaying = YES;
    }
}

-(void) createExplosion:(int)x Y:(int)y
{
	CGSize size = [[CCDirector sharedDirector] winSize];
	
	CCParticleSystemQuad *emitter = [[CCParticleSystemQuad alloc] initWithTotalParticles:3000];
	emitter.texture = [[CCTextureCache sharedTextureCache] addImage:@"play_fire.png"];
	emitter.duration = 0.5f;
	emitter.emitterMode = kCCParticleModeRadius;
	
	if (_isiPhone) {
		emitter.startRadius = 0;
		emitter.startRadiusVar = 30;
		emitter.endRadius = size.width / 2;
		emitter.endRadiusVar = size.width / 4;
	}
	else {
		emitter.startRadius = 0.0f * (float)SCALE_MIN_IPAD;
		emitter.startRadiusVar = 30.0f * (float)SCALE_MIN_IPAD;
		emitter.endRadius = size.width / 2;
		emitter.endRadiusVar = size.width / 4;
	}

	emitter.rotatePerSecond = 0;
	emitter.rotatePerSecondVar = 0;
	
	emitter.angle = 90;
	emitter.angleVar = 360;
	
	emitter.life = 0.8f;
	emitter.lifeVar = 0.5f;
	
	emitter.startSpin = 0;
	emitter.startSpinVar = 0;
	emitter.endSpin = 0;
	emitter.endSpinVar = 0;
	
	emitter.startColor = (ccColor4F){1.0f, 0.8f, 0.2f, 1.0f};
	emitter.startColorVar = (ccColor4F){0.0f, 0.2f, 0.0f, 0.1f};
	emitter.endColor = (ccColor4F){1.0f, 0.6f, 0.2f, 0.5f};
	emitter.endColorVar = (ccColor4F){0.0f, 0.2f, 0.0f, 0.1f};
	
	if (_isiPhone) {
		emitter.startSize = 20.0f;
		emitter.startSizeVar = 5.0f;
	}
	else {
		emitter.startSize = 20.0f * (float)SCALE_MIN_IPAD;
		emitter.startSizeVar = 5.0f * (float)SCALE_MIN_IPAD;
	}
	emitter.endSize = kCCParticleStartSizeEqualToEndSize;
	
	emitter.emissionRate = emitter.totalParticles / emitter.duration;
	emitter.position = ccp(x, y);
	emitter.posVar = ccp(5, 5);
	[self addChild:emitter];
	emitter.blendAdditive = NO;
	emitter.autoRemoveOnFinish = YES;
    [emitter release];
}

- (void) disappearing:(ccTime)deltaTime
{
	IceSprite *tmpIce;
	int ccount = 0;
	for (int i = 0; i < MAX_ICE_INGAME; i++) {
		tmpIce = (IceSprite*)[self getChildByTag:enum_tag_ices + i];
		if (!tmpIce._isActive) {
            continue;
        }
		ccount++;
		if (tmpIce.opacity < 15) {
            [tmpIce setOpacity:0];
        }
		else {
            [tmpIce setOpacity: tmpIce.opacity - 5];
        }
		if (tmpIce.opacity == 0) {
			if (_isiPhone) {
                tmpIce.position = ccp(0, Y_START);
            }
			else {
                tmpIce.position = ccp(0, (float)Y_START * (float)SCALE_MIN_IPAD);
            }
			tmpIce._isActive = FALSE;
			tmpIce.visible = FALSE;
		}
	}
	if (ccount == 0) {
        [self unschedule:@selector(disappearing:)];
    }
}

- (void) slashIces:(int)x1 Y1:(int)y1 X2:(int)x2 Y2:(int)y2 Color:(int)knifeColor
{
	if (x1 == x2 && y1 == y2) {
        return;
    }

	int combo = 0;
	int refX, refY, minR = 100000;
	BOOL isFail = FALSE;
	refX = _prevPoint[knifeColor].x;
	refY = _prevPoint[knifeColor].y;
	IceSprite * tmpIce;
	for (int i = 0; i < MAX_ICE_INGAME; i++) {
		tmpIce = (IceSprite*)[self getChildByTag:enum_tag_ices + i];
		int splitType = 0;
		if ((splitType = [tmpIce splitAction:x1 Y1:y1 X2:x2 Y2:y2]) != 0) {
			if (sqrt((tmpIce.position.x - refX) * (tmpIce.position.x - refX) + (tmpIce.position.y - refY) * (tmpIce.position.y - refY)) < minR) {
				_iceComboX = tmpIce.position.x;
				_iceComboY = tmpIce.position.y;
			}
			if (tmpIce._iceKind == MAX_ICE) {
                [GameSound effectPenguin];
                [GameSound vib:1];
				[self createExplosion:tmpIce.position.x Y:tmpIce.position.y];
				isFail = TRUE;
			}
			else {
                combo++;
				if (splitType & enum_first_split) {
					[self addScore:4];
				}
				if (splitType & enum_second_first_split) {
                    [self addScore:6];
                }
				if (splitType & enum_second_second_split) {
                    [self addScore:6];
                }
				if ([tmpIce isFullSplit]) {
					[self addScore: 10];
					ComboSprite *tmpComboSprite;
					for (int j = 0; j < MAX_BONUS_INSCREEN; j++) {
						tmpComboSprite = (ComboSprite*)[self getChildByTag:enum_tag_combo + j];
						if (!tmpComboSprite._isActive) {
                            break;
                        }
					}
					[tmpComboSprite appearEffect:10 X:tmpIce.position.x Y:tmpIce.position.y];
				}
			}
		}
	}
	if (isFail) {
		_iceComboCount[knifeColor] = 0;
		_gameState = enum_game_state_play;
		for (int i = 0; i < MAX_KNIFE_COLOR; i++) {
			_isValidKnifeColor[i] = NO;
		}
		[self runAction:[CCSequence actions:
						 [CCCallFunc actionWithTarget:self selector:@selector(pausing)],
						 [CCDelayTime actionWithDuration:2],
						 [CCCallFunc actionWithTarget:self selector:@selector(resuming)],
						 [CCCallFunc actionWithTarget:self selector:@selector(changeStatePlay)],
						 nil]];
		return;
	}
	if (combo != 0) {
		_iceComboCount[knifeColor] += combo;
		KnifeSprite *knife;
		for (int i = 0; i < MAX_KNIFE_INSCREEN; i++) {
			KnifeSprite *tmp = (KnifeSprite*)[self getChildByTag:enum_tag_knifes + i];
			if (!tmp._isActive) {
				knife = tmp;
				break;
			}
		}
		[knife splitAction:x1 Y1:y1 X2:x2 Y2:y2 Color:knifeColor];
	}
}
    
-(void) addScore:(int)addedScore
{
    if (_restTime != 0) {
        _score += addedScore;
        
        [((PlayScoreSprite*)[self getChildByTag:enum_tag_play_score]) addScore:addedScore];
        [((PlayHighScoreSprite*)[self getChildByTag:enum_tag_play_high_score]) updateScore:_score];	
    }
}

- (void) decreasePenguinLife
{
    _decreasePenguinLifeCount++;
    if (_decreasePenguinLifeCount == 1) {
        _pengiunLife1.visible = FALSE;
    }
    if (_decreasePenguinLifeCount == 2) {
        _pengiunLife2.visible = FALSE;
    }
    if (_decreasePenguinLifeCount == 3) {
        _pengiunLife3.visible = FALSE;
        
        GLOBAL_gamePlaying = NO;
        if ([GameData getBestScore] < _score) {
            [GameData setBestScore:_score];
        }
        _gameState = enum_game_state_tmp;
        [((ScoreSprite*)[self getChildByTag:enum_tag_score]) setScore:_score];
        [GameSound effectGameEnd];
        [self runAction:[CCSequence actions:
                         [CCCallFunc actionWithTarget:self selector:@selector(pausing)],
                         [CCDelayTime actionWithDuration:1],
                         [CCCallFunc actionWithTarget:self selector:@selector(stopGame)],
                         [CCCallFunc actionWithTarget:self selector:@selector(appearScoreSprite)],
                         [CCDelayTime actionWithDuration:APPEAR_TIME],
                         [CCCallFunc actionWithTarget:self selector:@selector(changeStateScore)],
                         nil]];
    }
}

-(void) throwIces
{
	int throwCount = 0;
	IceSprite *ices;
	int maxInScreen = _gameTime / 10;
	if (maxInScreen > MAX_ICE_INSCREEN) {
        maxInScreen = MAX_ICE_INSCREEN;
    }
	maxInScreen = 1 + [self getRandomValue:0 Last:maxInScreen];
	
	while (throwCount < maxInScreen) {
		for (int i = 0; i < MAX_ICE_INGAME; i++) {
			ices = (IceSprite*)[self getChildByTag:enum_tag_ices + i];
			if (!ices._isActive) {
				if (_intervalThresholdTime < (INTERVAL_MIN + INTERVAL_MAX) / 2) {
                    [ices startFlyingWithPenguin:DELAY_VARIENCE rate:_penguinAppearNum];
				}
				else {
                    [ices startFlying:DELAY_VARIENCE];
                }
				throwCount++;
				if (throwCount >= maxInScreen) {
                    break;
                }
			}
		}
	}
}

- (void) playTouchesBegan:(NSArray*)allTouches
{
	int dx, dy, x, y;
	UITouch *touch;
	CGPoint location, prevLocation;
	
	if ([allTouches count] == 1) {
		BOOL valid = YES;
		for (int i = 0; i < MAX_KNIFE_COLOR; i++) {
			if (_isValidKnifeColor[i]) {
				valid = NO;
				break;
			}
		}
		if (valid) {
			touch = [allTouches objectAtIndex:0];
			location = [touch locationInView:[touch view]];
			location = [[CCDirector sharedDirector] convertToGL:location];
			x = location.x;
			y = location.y;

            if (_isiPhone) {
				dx = x - PAUSE_BTN_X;
				dy = y - PAUSE_BTN_Y;
				if (sqrt(dx * dx + dy * dy) < PAUSE_BTN_R) {
					_isPressed = YES;
					[GameSound effectBtnClick];
					_pressedState = enum_play_press_pause_btn;
					_pressedStateShadow = _pressedState;
                    CCSprite *tmp = [[CCSprite alloc] initWithFile:@"play_btn_pause_press@2x.png"];
                    [((CCSprite*)[self getChildByTag:enum_tag_play_pause_btn]) setTexture:tmp.texture];
                    [((CCSprite*)[self getChildByTag:enum_tag_play_pause_btn]) setTextureRect:tmp.textureRect];
                    [tmp release];
					((CCSprite*)[self getChildByTag:enum_tag_play_pause_btn]).opacity = PRESS_OPACITY;
				}
			}
			else {
				dx = x - (float)SCALE_MIN_IPAD * (float)PAUSE_BTN_X;
				dy = y - (float)SCALE_MIN_IPAD * (float)PAUSE_BTN_Y;
				if (sqrt(dx * dx + dy * dy) < (float)SCALE_MIN_IPAD * (float)PAUSE_BTN_R) {
					_isPressed = YES;
					[GameSound effectBtnClick];
					_pressedState = enum_play_press_pause_btn;
					_pressedStateShadow = _pressedState;
                    CCSprite *tmp = [[CCSprite alloc] initWithFile:@"play_btn_pause_press@2x.png"];
                    [((CCSprite*)[self getChildByTag:enum_tag_play_pause_btn]) setTexture:tmp.texture];
                    [((CCSprite*)[self getChildByTag:enum_tag_play_pause_btn]) setTextureRect:tmp.textureRect];
                    [tmp release];
					((CCSprite*)[self getChildByTag:enum_tag_play_pause_btn]).opacity = PRESS_OPACITY;
				}
			}
		}
	}
	if (_isPressed) {
        return;
    }
	
	for (int i = 0; i < [allTouches count]; i++) {
		touch = [allTouches objectAtIndex:i];
		location = [touch locationInView:[touch view]];
		prevLocation = [touch previousLocationInView:[touch view]];
		location = [[CCDirector sharedDirector] convertToGL:location];
		prevLocation = [[CCDirector sharedDirector] convertToGL:prevLocation];
		
		for (int j = 0;  j < MAX_KNIFE_COLOR; j++) {
			if (!_isValidKnifeColor[j]) {
				_isValidKnifeColor[j] = YES;
				_pastTime[j] = LIMIT_PAST_TIME;
				_prevPoint[j].x = location.x;
				_prevPoint[j].y = location.y;
				_prevDrawPoint[j].x = location.x;
				_prevDrawPoint[j].y = location.y;
				_iceComboCount[j] = 0;
				return;
			}
		}
	}
}
				 
- (void) pauseTouchesBegan:(int)x Y:(int)y
{
	[((PauseSprite*)[self getChildByTag:enum_tag_pause]) touchesBegan:x Y:y];
}

- (void) optionTouchesBegan:(int)x Y:(int)y
{
	[((PlayOptionSprite*)[self getChildByTag:enum_tag_option]) touchesBegan:x Y:y];
}

- (void) scoreTouchesBegan:(int)x Y:(int)y
{
	[((ScoreSprite*)[self getChildByTag:enum_tag_score]) touchesBegan:x Y:y];
}

- (void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	NSArray *allTouches = [touches allObjects];
	UITouch *touch = [allTouches objectAtIndex:0];
	CGPoint location = [touch locationInView:[touch view]];
	location = [[CCDirector sharedDirector] convertToGL:location];
	
	switch (_gameState) {
		case enum_game_state_play:		
            [self playTouchesBegan:allTouches];	
            break;
		case enum_game_state_pause:	
            [self pauseTouchesBegan:location.x Y:location.y];	
            break;
		case enum_game_state_option:
            [self optionTouchesBegan:location.x Y:location.y];
            break;
		case enum_game_state_score:
            [self scoreTouchesBegan:location.x Y:location.y];
            break;
		default:	
            break;
	}
}

- (void) playTouchesMoved:(NSArray*)allTouches
{
	int dx, dy, x, y;
	UITouch *touch;
	CGPoint location, prevLocation;
	
	if (_isPressed) {
		touch = [allTouches objectAtIndex:0];
		location = [touch locationInView:[touch view]];
		location = [[CCDirector sharedDirector] convertToGL:location];
		x = location.x;
		y = location.y;
		if (_isiPhone) {
			dx = x - PAUSE_BTN_X;
			dy = y - PAUSE_BTN_Y;
			if (sqrt(dx * dx + dy * dy) < PAUSE_BTN_R) {
				if (_pressedStateShadow != enum_play_press_pause_btn) {
					[GameSound effectBtnClick];
					_pressedStateShadow = enum_play_press_pause_btn;
					((CCSprite*)[self getChildByTag:enum_tag_play_pause_btn]).opacity = PRESS_OPACITY;
				}
			}
			else {
				if (_pressedStateShadow == enum_play_press_pause_btn) {
					_pressedStateShadow = enum_play_press_none;
					((CCSprite*)[self getChildByTag:enum_tag_play_pause_btn]).opacity = RELEASE_OPACITY;
				}
			}
		}
		else {
			dx = x - (float)SCALE_MIN_IPAD * (float)PAUSE_BTN_X;
			dy = y - (float)SCALE_MIN_IPAD * (float)PAUSE_BTN_Y;
			if (sqrt(dx * dx + dy * dy) < (float)SCALE_MIN_IPAD * (float)PAUSE_BTN_R) {
				if (_pressedStateShadow != enum_play_press_pause_btn) {
					_pressedStateShadow = enum_play_press_pause_btn;
					((CCSprite*)[self getChildByTag:enum_tag_play_pause_btn]).opacity = PRESS_OPACITY;
				}
			}
			else {
				if (_pressedStateShadow == enum_play_press_pause_btn) {
					_pressedStateShadow = enum_play_press_none;
					((CCSprite*)[self getChildByTag:enum_tag_play_pause_btn]).opacity = RELEASE_OPACITY;
				}
			}
		}
		return;
	}
	BOOL isHandled[MAX_KNIFE_COLOR];
	for (int i = 0; i < MAX_KNIFE_COLOR; i++) {
		isHandled[i] = FALSE;
	}
	for (int i = 0; i < [allTouches count]; i++) {
		touch = [allTouches objectAtIndex:i];
		location = [touch locationInView:[touch view]];
		prevLocation = [touch previousLocationInView:[touch view]];
		location = [[CCDirector sharedDirector] convertToGL:location];
		prevLocation = [[CCDirector sharedDirector] convertToGL:prevLocation];
		
		BOOL handle = NO;
		for (int j = 0;  j < MAX_KNIFE_COLOR; j++) {
			if (_isValidKnifeColor[j] && _prevPoint[j].x == prevLocation.x && _prevPoint[j].y == prevLocation.y) {
				handle = YES;
				isHandled[j] = YES;
				_prevPoint[j].x = location.x;
				_prevPoint[j].y = location.y;
                
				BOOL validKnife = NO;
				KnifeSprite *tmpKnife;
				for (int i = 0; i < MAX_KNIFE_INSCREEN; i++) {
					tmpKnife = (KnifeSprite*)[self getChildByTag:enum_tag_knifes + i];
					if (!tmpKnife._isActive) {
						validKnife = YES;
						break;
					}
				}
				_isEmptySlash[j] = FALSE;
				if (validKnife == YES) {
					if ([tmpKnife appearAction:_prevDrawPoint[j].x Y1:_prevDrawPoint[j].y X2:location.x Y2:location.y Color:j Sound:NO]) {
						_prevDrawPoint[j].x = location.x;
						_prevDrawPoint[j].y = location.y;
						if (_pastTime[j] > LIMIT_PAST_TIME) {
							if (_iceComboCount[i] == 0) {
                                [GameSound effectKnife];
                            }
							_pastTime[j] = 0;
						}
						_isEmptySlash[j] = TRUE;
					}
				}
				[self slashIces:prevLocation.x Y1:prevLocation.y X2:location.x Y2:location.y Color:j];
				break;
			}
		}
		if (handle == NO) {
			for (int k = 0;  k < MAX_KNIFE_COLOR; k++) {
				if (!_isValidKnifeColor[k]) {
					_isValidKnifeColor[k] = YES;
					_pastTime[k] = 0;
					_prevPoint[k].x = location.x;
					_prevPoint[k].y = location.y;
					_prevDrawPoint[k].x = location.x;
					_prevDrawPoint[k].y = location.y;
					_iceComboCount[k] = 0;
					isHandled[k] = YES;
					break;
				}
			}
		}
	}
}

- (void) pauseTouchesMoved:(int)x Y:(int)y
{
	[((PauseSprite*)[self getChildByTag:enum_tag_pause]) touchesMoved:x Y:y];
}

- (void) optionTouchesMoved:(int)x Y:(int)y
{
	[((PlayOptionSprite*)[self getChildByTag:enum_tag_option]) touchesMoved:x Y:y];
}

- (void) scoreTouchesMoved:(int)x Y:(int)y
{
	[((ScoreSprite*)[self getChildByTag:enum_tag_score]) touchesMoved:x Y:y];
}

- (void) ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	NSArray *allTouches = [touches allObjects];
	UITouch *touch = [allTouches objectAtIndex:0];
	CGPoint location = [touch locationInView:[touch view]];
	location = [[CCDirector sharedDirector] convertToGL:location];
	
	switch (_gameState) {
		case enum_game_state_play:
            [self playTouchesMoved:allTouches];	
            break;
		case enum_game_state_pause:
            [self pauseTouchesMoved:location.x Y:location.y];
            break;
		case enum_game_state_option:
            [self optionTouchesMoved:location.x Y:location.y];
            break;
		case enum_game_state_score:
            [self scoreTouchesMoved:location.x Y:location.y];
            break;
		default:	
            break;
	}
}

- (void) playTouchesEnded:(NSArray*)allTouches
{
	int dx, dy, x, y;
	float dr;
	UITouch *touch;
	CGPoint location, prevLocation;
	
	if (_isPressed) {
		touch = [allTouches objectAtIndex:0];
		location = [touch locationInView:[touch view]];
		location = [[CCDirector sharedDirector] convertToGL:location];
		x = location.x;
		y = location.y;
		if (_isiPhone) {
			dx = x - PAUSE_BTN_X;
			dy = y - PAUSE_BTN_Y;
			dr = PAUSE_BTN_R;
		}
		else {
			dx = x - (float)SCALE_MIN_IPAD * (float)PAUSE_BTN_X;
			dy = y - (float)SCALE_MIN_IPAD * (float)PAUSE_BTN_Y;
			dr = (float)SCALE_MIN_IPAD * (float)PAUSE_BTN_R;
		}
		if (sqrt(dx * dx + dy * dy) < dr) {
            CCSprite *tmp = [[CCSprite alloc] initWithFile:@"play_btn_pause_normal@2x.png"];
            [((CCSprite*)[self getChildByTag:enum_tag_play_pause_btn]) setTexture:tmp.texture];
            [((CCSprite*)[self getChildByTag:enum_tag_play_pause_btn]) setTextureRect:tmp.textureRect];
            [tmp release];
			((CCSprite*)[self getChildByTag:enum_tag_play_pause_btn]).opacity = RELEASE_OPACITY;
			_gameState = enum_game_state_tmp;
			[self runAction:[CCSequence actions:
							 [CCCallFunc actionWithTarget:self selector:@selector(pauseGame)],
							 [CCCallFunc actionWithTarget:self selector:@selector(appearPauseSprite)],
							 [CCDelayTime actionWithDuration:APPEAR_TIME],
							 [CCCallFunc actionWithTarget:self selector:@selector(changeStatePause)],
							 nil]];
		}
		else {
            CCSprite *tmp = [[CCSprite alloc] initWithFile:@"play_btn_pause_normal@2x.png"];
            [((CCSprite*)[self getChildByTag:enum_tag_play_pause_btn]) setTexture:tmp.texture];
            [((CCSprite*)[self getChildByTag:enum_tag_play_pause_btn]) setTextureRect:tmp.textureRect];
            [tmp release];
			((CCSprite*)[self getChildByTag:enum_tag_play_pause_btn]).opacity = RELEASE_OPACITY;
		}
		_isPressed = NO;
		_pressedState = enum_play_press_none;
		return;
	}
	for (int i = 0; i < [allTouches count]; i++) {
		touch = [allTouches objectAtIndex:i];
		location = [touch locationInView:[touch view]];
		prevLocation = [touch previousLocationInView:[touch view]];
		location = [[CCDirector sharedDirector] convertToGL:location];
		prevLocation = [[CCDirector sharedDirector] convertToGL:prevLocation];
		
		for (int j = 0;  j < MAX_KNIFE_COLOR; j++) {
			if (_isValidKnifeColor[j] && _prevPoint[j].x == prevLocation.x && _prevPoint[j].y == prevLocation.y) {
				BOOL validKnife = NO;
				KnifeSprite *tmpKnife;
				for (int i = 0; i < MAX_KNIFE_INSCREEN; i++) {
					tmpKnife = (KnifeSprite*)[self getChildByTag:enum_tag_knifes + i];
					if (!tmpKnife._isActive) {
						validKnife = YES;
						break;
					}
				}
				
				if (validKnife == YES) {
					BOOL isKnifeSound = NO;
					if (_iceComboCount[j] == 0) {
                        isKnifeSound = YES;
                    }
					[tmpKnife appearAction:_prevDrawPoint[j].x Y1:_prevDrawPoint[j].y X2:location.x Y2:location.y Color:j Sound:isKnifeSound];				
				}
			}
			_isValidKnifeColor[j] = NO;
		}
	}
	for (int i = 0; i < MAX_KNIFE_COLOR; i++) {
		_isEmptySlash[i] = TRUE;
		if (_iceComboCount[i] > 2) {
			ComboSprite *tmpComboSprite;
			for (int j = 0; j < MAX_BONUS_INSCREEN; j++) {
				tmpComboSprite = (ComboSprite*)[self getChildByTag:enum_tag_combo + j];
				if (!tmpComboSprite._isActive) {
                    break;
                }
			}
            if (_isiPhone) {
                [tmpComboSprite appearEffect:(_iceComboCount[i] - 2) * 10 X:_iceComboX+5 Y:_iceComboY+5];
            }
            else {
                [tmpComboSprite appearEffect:(_iceComboCount[i] - 2) * 10 X:_iceComboX+5.*(float)SCALE_MIN_IPAD Y:_iceComboY+5.*(float)SCALE_MIN_IPAD];
            }
			[self addScore:(_iceComboCount[i] - 2) * 10];
		}
		if (_iceComboCount[i] != 0) {
            [GameSound effectKnifeSlashed];
        }
		_iceComboCount[i] = 0;
	}
}

- (void) pauseTouchesEnded:(int)x Y:(int)y
{
	switch ([((PauseSprite*)[self getChildByTag:enum_tag_pause]) touchesEnded:x Y:y]) {
		case enum_pause_continue:
			_gameState = enum_game_state_tmp;
			[self runAction:[CCSequence actions:
							 [CCCallFunc actionWithTarget:self selector:@selector(disappearPauseSprite)],
							 [CCCallFunc actionWithTarget:self selector:@selector(resumeGame)],
							 [CCDelayTime actionWithDuration:APPEAR_TIME],
							 [CCCallFunc actionWithTarget:self selector:@selector(changeStatePlay)],
							 nil]];
			break;
		case enum_pause_option:
			_gameState = enum_game_state_tmp;
			[self runAction:[CCSequence actions:
							 [CCCallFunc actionWithTarget:self selector:@selector(disappearPauseSprite)],
							 [CCCallFunc actionWithTarget:self selector:@selector(appearOptionSprite)],
							 [CCDelayTime actionWithDuration:APPEAR_TIME],
							 [CCCallFunc actionWithTarget:self selector:@selector(changeStateOption)],
							 nil]];
			break;
		case enum_pause_exit:
            GLOBAL_gamePlaying = NO;
			[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.8 scene:[MainMenuScene scene]]];
			break;
		default:	
            break;
	}
}

- (void) optionTouchesEnded:(int)x Y:(int)y
{
	switch ([((PlayOptionSprite*)[self getChildByTag:enum_tag_option]) touchesEnded:x Y:y]) {
		case enum_play_option_exit:
			_gameState = enum_game_state_tmp;
			[self runAction:[CCSequence actions:
							 [CCCallFunc actionWithTarget:self selector:@selector(disappearOptionSprite)],
							 [CCCallFunc actionWithTarget:self selector:@selector(appearPauseSprite)],
							 [CCDelayTime actionWithDuration:APPEAR_TIME],
							 [CCCallFunc actionWithTarget:self selector:@selector(changeStatePause)],
							 nil]];
			break;
		default:
            break;
	}
}

- (void) scoreTouchesEnded:(int)x Y:(int)y
{
	switch ([((ScoreSprite*)[self getChildByTag:enum_tag_score]) touchesEnded:x Y:y]) {
		case enum_score_reply:
			_gameState = enum_game_state_tmp;
			[self runAction:[CCSequence actions:
							 [CCCallFunc actionWithTarget:self selector:@selector(disappearScoreSprite)],
							 [CCDelayTime actionWithDuration:APPEAR_TIME],
							 [CCCallFunc actionWithTarget:self selector:@selector(reset)],
							 [CCCallFunc actionWithTarget:self selector:@selector(changeStatePlay)],
							 nil]];
			break;
		case enum_score_exit:
            GLOBAL_gamePlaying = NO;
			[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.8 scene:[MainMenuScene scene]]];
			break;
		default:
			break;
	}
}

- (void) ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{	
	NSArray *allTouches = [touches allObjects];
	UITouch *touch = [allTouches objectAtIndex:0];
	CGPoint location = [touch locationInView:[touch view]];
	location = [[CCDirector sharedDirector] convertToGL:location];
	
	switch (_gameState) {
		case enum_game_state_play:
            [self playTouchesEnded:allTouches];
            break;
		case enum_game_state_pause:
            [self pauseTouchesEnded:location.x Y:location.y];
            break;
		case enum_game_state_option:
            [self optionTouchesEnded:location.x Y:location.y];
            break;
		case enum_game_state_score:
            [self scoreTouchesEnded:location.x Y:location.y];
            break;
		default:
            break;
	}
}

- (void) pausing
{
	[self unschedule:@selector(updateNow:)];
	
	IceSprite *tmpIce;
	for (int i = 0; i < MAX_ICE_INGAME; i++) {
		tmpIce = (IceSprite*)[self getChildByTag:enum_tag_ices + i];
		if (tmpIce._isActive) {
			[tmpIce unscheduleAllSelectors];
		}
	}
}

- (void) pauseGame
{
	[self unschedule:@selector(updateNow:)];
	
	((CCSprite*)[self getChildByTag:enum_tag_play_background]).opacity = SHADOW_OPACITY;
	[((PlayScoreSprite*)[self getChildByTag:enum_tag_play_score]) settingOpacity:SHADOW_OPACITY];
	[((PlayHighScoreSprite*)[self getChildByTag:enum_tag_play_high_score]) settingOpacity:SHADOW_OPACITY];
    [((PlayTimerSprite*)[self getChildByTag:enum_tag_play_timer]) settingOpacity:SHADOW_OPACITY];
	((CCSprite*)[self getChildByTag:enum_tag_play_pause_btn]).opacity = SHADOW_OPACITY;
	
	IceSprite *tmpIce;
	for (int i = 0; i < MAX_ICE_INGAME; i++) {
		tmpIce = (IceSprite*)[self getChildByTag:enum_tag_ices + i];
		if (tmpIce._isActive) {
			[tmpIce pauseSchedulerAndActions];
			[tmpIce settingOpacity:SHADOW_OPACITY];
		}
	}
	KnifeSprite *tmpKnife;
	for (int i = 0; i < MAX_KNIFE_INSCREEN; i++) {
		tmpKnife = (KnifeSprite*)[self getChildByTag:enum_tag_knifes + i];
		if (tmpKnife._isActive) {
			[tmpKnife pauseSchedulerAndActions];
			tmpKnife.opacity = tmpKnife.opacity * SHADOW_OPACITY / 256;
		}
	}
}

- (void) stopGame
{
	[self unschedule:@selector(updateNow:)];
	
	((CCSprite*)[self getChildByTag:enum_tag_play_background]).opacity = SHADOW_OPACITY;
	[((PlayScoreSprite*)[self getChildByTag:enum_tag_play_score]) settingOpacity:SHADOW_OPACITY];
	[((PlayHighScoreSprite*)[self getChildByTag:enum_tag_play_high_score]) settingOpacity:SHADOW_OPACITY];
    [((PlayTimerSprite*)[self getChildByTag:enum_tag_play_timer]) settingOpacity:SHADOW_OPACITY];
	((CCSprite*)[self getChildByTag:enum_tag_play_pause_btn]).opacity = SHADOW_OPACITY;
	
	IceSprite *tmpIce;
	for (int i = 0; i < MAX_ICE_INGAME; i++) {
		tmpIce = (IceSprite*)[self getChildByTag:enum_tag_ices + i];
		if (tmpIce._isActive) {
			[tmpIce unscheduleAllSelectors];
			[tmpIce initIce];
		}
	}
	KnifeSprite *tmpKnife;
	for (int i = 0; i < MAX_KNIFE_INSCREEN; i++) {
		tmpKnife = (KnifeSprite*)[self getChildByTag:enum_tag_knifes + i];
		if (tmpKnife._isActive) {
			[tmpKnife unscheduleAllSelectors];
			[tmpKnife initKnife];
		}
	}
}

- (void) resuming
{
	[self schedule:@selector(updateNow:)];
}

- (void) resumeGame
{
	((CCSprite*)[self getChildByTag:enum_tag_play_background]).opacity = PRESS_OPACITY;
	[((PlayScoreSprite*)[self getChildByTag:enum_tag_play_score]) settingOpacity:PRESS_OPACITY];
	[((PlayHighScoreSprite*)[self getChildByTag:enum_tag_play_high_score]) settingOpacity:PRESS_OPACITY];
    [((PlayTimerSprite*)[self getChildByTag:enum_tag_play_timer]) settingOpacity:PRESS_OPACITY];
	((CCSprite*)[self getChildByTag:enum_tag_play_pause_btn]).opacity = RELEASE_OPACITY;
	
	IceSprite *tmpIce;
	for (int i = 0; i < MAX_ICE_INGAME; i++) {
		tmpIce = (IceSprite*)[self getChildByTag:enum_tag_ices + i];
		if (tmpIce._isActive) {
			[tmpIce resumeSchedulerAndActions];
			[tmpIce settingOpacity:PRESS_OPACITY];
		}
	}
	KnifeSprite *tmpKnife;
	for (int i = 0; i < MAX_KNIFE_INSCREEN; i++) {
		tmpKnife = (KnifeSprite*)[self getChildByTag:enum_tag_knifes + i];
		if (tmpKnife._isActive) {
            [tmpKnife resumeSchedulerAndActions];
        }
	}
	[self schedule:@selector(updateNow:)];
}

- (void) appearPauseSprite
{
	PauseSprite* tmp = (PauseSprite*)[self getChildByTag:enum_tag_pause];
	tmp.visible = TRUE;
	[tmp appearSprite:APPEAR_TIME];	
}

- (void) appearOptionSprite
{
	PlayOptionSprite* tmp = (PlayOptionSprite*)[self getChildByTag:enum_tag_option];
	tmp.visible = TRUE;
	[tmp appearSprite:APPEAR_TIME];	
}

- (void) appearScoreSprite
{
	ScoreSprite* tmp = (ScoreSprite*)[self getChildByTag:enum_tag_score];
	tmp.visible = TRUE;
	[tmp appearSprite:APPEAR_TIME];	
}

- (void) disappearPauseSprite
{
	PauseSprite* tmp = (PauseSprite*)[self getChildByTag:enum_tag_pause];
	[tmp disappearSprite:APPEAR_TIME];
}

- (void) disappearOptionSprite
{
	PlayOptionSprite* tmp = (PlayOptionSprite*)[self getChildByTag:enum_tag_option];
	[tmp disappearSprite:APPEAR_TIME];
}

- (void) disappearScoreSprite
{
	ScoreSprite* tmp = (ScoreSprite*)[self getChildByTag:enum_tag_score];
	[tmp disappearSprite:APPEAR_TIME];
}

-(void) changeStatePause
{
	_gameState = enum_game_state_pause;
}

-(void) changeStateOption
{
	_gameState = enum_game_state_option;
}

-(void) changeStateScore
{
	_gameState = enum_game_state_score;
}

-(void) changeStatePlay
{
	_gameState = enum_game_state_play;
}

- (int) getRandomValue:(int)val_1 Last:(int)val_2
{
	int a;
	if (val_2 - val_1 != 0) {
        a = rand() % (val_2 - val_1);
    }
	else {
        a = 1;
    }
	return val_1 + a;
}
	
// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
    [GameSound unloadPenguin];
    [GameSound unloadGameEnd];
    [GameSound unloadKnifeSlashed];
    [GameSound unloadBtnClick];
    [GameSound unloadKnife];
    
    [_backSprite release];
    [_pauseBtnSprite release];
    [_pengiunLife1 release];
    [_pengiunLife2 release];
    [_pengiunLife3 release];
	[super dealloc];
}

@end

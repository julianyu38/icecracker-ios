//
//  OptionScene.h
//  Ice_Cracker
//
//  Created by mac on 5/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface PlayScoreSprite : CCSprite {
    CCSprite *_iceSprite;
    CCSprite *_digit[5];
    
	int _score;
	int _nWidth;
	int _nHeight;
    BOOL _isiPhone;
	CCSprite *_numberSet;
}

+(id) sharedPlayScoreSprite;

-(void) clearScore;
-(void) addScore:(int)addValue;
-(void) settingOpacity:(int)value;

@property(nonatomic) int _score;
@end

//
//  OptionScene.m
//  Ice_Cracker
//
//  Created by mac on 5/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PlayScoreSprite.h"
#import "GameData.h"
#import "GameSound.h"

@implementation PlayScoreSprite

@synthesize _score = score;

enum {
	enum_tag_ice = 0,
	enum_tag_digit,
};

#define ICE_X        -20
#define ICE_Y        3

#define DIGIT_WIDTH     14
#define DIGIT_X         24
#define DIGIT_Y         3

#define MAX_DIGIT       5

+(id) sharedPlayScoreSprite
{
	PlayScoreSprite *sprite = [PlayScoreSprite node];

	// return the scene
	return sprite;
}

-(id) init
{
	if( (self=[super init] )) {
		score = 0;
        _isiPhone = [GameData isiPhone];

		_numberSet = [[CCSprite alloc] initWithFile:@"play_count_number@2x.png"];
		_nWidth = (_numberSet.contentSize.width) / 11;
		_nHeight = _numberSet.contentSize.height;
		[self setTexture:_numberSet.texture];
        [self setTextureRect:CGRectMake(10 * _nWidth, (float)SCALE_MIN_IPAD * 3, _nWidth, _nHeight)];
		
		_iceSprite = [[CCSprite alloc] initWithFile:@"play_ic_count@2x.png"];
        _iceSprite.position = ccp(_nWidth / 2 + ((float)SCALE_MIN_IPAD * ICE_X), _nHeight / 2 + ((float)SCALE_MIN_IPAD * ICE_Y));
		[self addChild:_iceSprite z:enum_tag_ice tag:enum_tag_ice];
		
		for (int i = 0; i < MAX_DIGIT; i++) {
			_digit[i] = [[CCSprite alloc] initWithTexture:_numberSet.texture rect:CGRectMake(i * _nWidth, 0, _nWidth, _nHeight)];
			_digit[i].visible = FALSE;
            _digit[i].position = ccp(self.contentSize.width / 2 + ((float)SCALE_MIN_IPAD * (DIGIT_X + i * DIGIT_WIDTH)), self.contentSize.height / 2 + ((float)SCALE_MIN_IPAD * DIGIT_Y));
			[self addChild:_digit[i] z:enum_tag_digit + i tag:enum_tag_digit + i];
		}
        [_numberSet release];
	}
	return self;
}

- (void) refresh
{
	int num;
	CCSprite *tmp1 = (CCSprite*)[self getChildByTag:enum_tag_digit];
	CCSprite *tmp2 = (CCSprite*)[self getChildByTag:enum_tag_digit + 1];
	CCSprite *tmp3 = (CCSprite*)[self getChildByTag:enum_tag_digit + 2];
	CCSprite *tmp4 = (CCSprite*)[self getChildByTag:enum_tag_digit + 3];
	CCSprite *tmp5 = (CCSprite*)[self getChildByTag:enum_tag_digit + 4];
	tmp1.visible = FALSE;
	tmp2.visible = FALSE;
	tmp3.visible = FALSE;
	tmp4.visible = FALSE;
	tmp5.visible = FALSE;
	if (score >= 10000) {
		num = score / 10000;
		[tmp1 setTextureRect:CGRectMake(num * _nWidth, 0, _nWidth, _nHeight)];
		tmp1.visible = TRUE;
		num = (score / 1000) % 10;
		[tmp2 setTextureRect:CGRectMake(num * _nWidth, 0, _nWidth, _nHeight)];
		tmp2.visible = TRUE;
		num = (score / 100) % 10;
		[tmp3 setTextureRect:CGRectMake(num * _nWidth, 0, _nWidth, _nHeight)];
		tmp3.visible = TRUE;
		num = (score / 10) % 10;
		[tmp4 setTextureRect:CGRectMake(num * _nWidth, 0, _nWidth, _nHeight)];
		tmp4.visible = TRUE;
		num = score % 10;
		[tmp5 setTextureRect:CGRectMake(num * _nWidth, 0, _nWidth, _nHeight)];
		tmp5.visible = TRUE;	
	}
	else if (score >= 1000) {
		num = score / 1000;
		[tmp1 setTextureRect:CGRectMake(num * _nWidth, 0, _nWidth, _nHeight)];
		tmp1.visible = TRUE;
		num = (score / 100) % 10;
		[tmp2 setTextureRect:CGRectMake(num * _nWidth, 0, _nWidth, _nHeight)];
		tmp2.visible = TRUE;
		num = (score / 10) % 10;
		[tmp3 setTextureRect:CGRectMake(num * _nWidth, 0, _nWidth, _nHeight)];
		tmp3.visible = TRUE;
		num = score % 10;
		[tmp4 setTextureRect:CGRectMake(num * _nWidth, 0, _nWidth, _nHeight)];
		tmp4.visible = TRUE;
	}
	else if (score >= 100) {
		num = score / 100;
		[tmp1 setTextureRect:CGRectMake(num * _nWidth, 0, _nWidth, _nHeight)];
		tmp1.visible = TRUE;
		num = (score / 10) % 10;
		[tmp2 setTextureRect:CGRectMake(num * _nWidth, 0, _nWidth, _nHeight)];
		tmp2.visible = TRUE;
		num = score % 10;
		[tmp3 setTextureRect:CGRectMake(num * _nWidth, 0, _nWidth, _nHeight)];
		tmp3.visible = TRUE;
	}
	else if (score >= 10) {
		num = score / 10;
		[tmp1 setTextureRect:CGRectMake(num * _nWidth, 0, _nWidth, _nHeight)];
		tmp1.visible = TRUE;
		num = score % 10;
		[tmp2 setTextureRect:CGRectMake(num * _nWidth, 0, _nWidth, _nHeight)];
		tmp2.visible = TRUE;
	}
	else if (score >= 0) {
		num = score % 10;
		[tmp1 setTextureRect:CGRectMake(num * _nWidth, 0, _nWidth, _nHeight)];
		tmp1.visible = TRUE;
	}	
}

-(void) clearScore
{
	score = 0;
	[self refresh];
}

-(void) addScore:(int)addValue 
{
	if (addValue == 0) {
        return; 
    }

	score += addValue;
	if (score < 0) {
        score = 0;
    }
	[self refresh];
}

-(void) settingOpacity:(int)value
{
	self.opacity = value;
	((CCSprite*)[self getChildByTag:enum_tag_ice]).opacity = value;
	for (int i = 0; i < MAX_DIGIT; i++) {
		((CCSprite*)[self getChildByTag:enum_tag_digit + i]).opacity = value;
    }
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
    [_iceSprite release];
    for (int i = 0; i < MAX_DIGIT; i++) {
        [_digit[i] release];
    }
	[super dealloc];
}
@end

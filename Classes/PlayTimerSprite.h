//
//  OptionScene.h
//  Ice_Cracker
//
//  Created by mac on 5/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface PlayTimerSprite : CCSprite {
    CCSprite *_digit10;
    CCSprite *_digit1;
    CCSprite *_digitdot1;
    CCSprite *_digitdot10;
    
	ccTime _time;
	int _nWidth;
	int _nHeight;
	BOOL _isClassic;
	BOOL _isiPhone;
}

+(id) sharedPlayTimerSprite;

-(void) clearTimer;
-(void) initTimer;
-(void) setTime:(ccTime)value;
-(void) settingOpacity:(int)value;

-(void) refresh;

@property(nonatomic) ccTime _time;
@end

//
//  OptionScene.m
//  Ice_Cracker
//
//  Created by mac on 5/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PlayTimerSprite.h"
#import "GameData.h"
#import "GameSound.h"

@implementation PlayTimerSprite

@synthesize _time = time;

enum {
	enum_tag_digit = 0,
};

#define DIGIT_WIDTH     14
#define DIGIT_X         -28
#define DIGIT_Y         0

+(id) sharedPlayTimerSprite
{
	PlayTimerSprite *sprite = [PlayTimerSprite node];

	// return the scene
	return sprite;
}

-(id) init
{
	if( (self=[super init] )) {
		time = 0;
        _isiPhone = [GameData isiPhone];

		CCSprite *numberSet = [[CCSprite alloc] initWithFile:@"play_count_number@2x.png"];
		_nWidth = numberSet.contentSize.width / 11;
		_nHeight = numberSet.contentSize.height;
		[self setTexture:numberSet.texture];
		[self setTextureRect:CGRectMake(10 * _nWidth, 0, _nWidth, _nHeight)];
		
		_digit10 = [[CCSprite alloc] initWithTexture:numberSet.texture rect:CGRectMake(0, 0, _nWidth, _nHeight)];
        _digit10.position = ccp(_nWidth / 2 + (float)SCALE_MIN_IPAD * DIGIT_X, _nHeight / 2 + (float)SCALE_MIN_IPAD * DIGIT_Y);
		[self addChild:_digit10 z:enum_tag_digit tag:enum_tag_digit];

		_digit1 = [[CCSprite alloc] initWithTexture:numberSet.texture rect:CGRectMake(0, 0, _nWidth, _nHeight)];
        _digit1.position = ccp(_nWidth / 2 + (float)SCALE_MIN_IPAD * (DIGIT_X + DIGIT_WIDTH), _nHeight / 2 + (float)SCALE_MIN_IPAD * DIGIT_Y);
		[self addChild:_digit1 z:enum_tag_digit + 1 tag:enum_tag_digit + 1];

		_digitdot1 = [[CCSprite alloc] initWithTexture:numberSet.texture rect:CGRectMake(0, 0, _nWidth, _nHeight)];
        _digitdot1.position = ccp(_nWidth / 2 + (float)SCALE_MIN_IPAD * (DIGIT_X + 3 * DIGIT_WIDTH), _nHeight / 2 + (float)SCALE_MIN_IPAD * DIGIT_Y);
		[self addChild:_digitdot1 z:enum_tag_digit + 2 tag:enum_tag_digit + 2];

		_digitdot10 = [[CCSprite alloc] initWithTexture:numberSet.texture rect:CGRectMake(0, 0, _nWidth, _nHeight)];
        _digitdot10.position = ccp(_nWidth / 2 + (float)SCALE_MIN_IPAD * (DIGIT_X + 4 * DIGIT_WIDTH), _nHeight / 2 + (float)SCALE_MIN_IPAD * DIGIT_Y);
		[self addChild:_digitdot10 z:enum_tag_digit + 3 tag:enum_tag_digit + 3];
        [numberSet release];
		
		[self refresh];
		
	}
	return self;
}

- (void) settingOpacity:(int)value
{
	self.opacity = value;
	for (int i = 0; i < 4; i++) 
    {
		((CCSprite*)[self getChildByTag:enum_tag_digit + i]).opacity = value;
    }
}
- (void) refresh
{
	if (time >= 200) {
        return;
    }
		
	int num;
	CCSprite *tmp1 = (CCSprite*)[self getChildByTag:enum_tag_digit];
	CCSprite *tmp2 = (CCSprite*)[self getChildByTag:enum_tag_digit + 1];
	CCSprite *tmp3 = (CCSprite*)[self getChildByTag:enum_tag_digit + 2];
	CCSprite *tmp4 = (CCSprite*)[self getChildByTag:enum_tag_digit + 3];
	if (time >= 600) {
        tmp1.visible = TRUE;
    }
	else {
        tmp1.visible = FALSE;
    }

	num = ((int)(time / 600.0)) % 10;
	[tmp1 setTextureRect:CGRectMake(num * _nWidth, 0, _nWidth, _nHeight)];
	num = ((int)(time / 60)) % 10;
	[tmp2 setTextureRect:CGRectMake(num * _nWidth, 0, _nWidth, _nHeight)];
	num = ((int)(time / 10)) % 6;
	[tmp3 setTextureRect:CGRectMake(num * _nWidth, 0, _nWidth, _nHeight)];
	num = ((int)time) % 10;
	[tmp4 setTextureRect:CGRectMake(num * _nWidth, 0, _nWidth, _nHeight)];
}

-(void) initTimer
{
	time = 0;
	CCSprite *tmp = [[CCSprite alloc] initWithFile:@"play_count_number@2x.png"];
	CCSprite *tmp1 = (CCSprite*)[self getChildByTag:enum_tag_digit];
	[tmp1 setTexture:tmp.texture];
	[tmp1 setTextureRect:tmp.textureRect];
	CCSprite *tmp2 = (CCSprite*)[self getChildByTag:enum_tag_digit + 1];
	[tmp2 setTexture:tmp.texture];
	[tmp2 setTextureRect:tmp.textureRect];
	CCSprite *tmp3 = (CCSprite*)[self getChildByTag:enum_tag_digit + 2];
	[tmp3 setTexture:tmp.texture];
	[tmp3 setTextureRect:tmp.textureRect];
	CCSprite *tmp4 = (CCSprite*)[self getChildByTag:enum_tag_digit + 3];
	[tmp4 setTexture:tmp.texture];
	[tmp4 setTextureRect:tmp.textureRect];
    [tmp release];
	[self refresh];
}

-(void) clearTimer
{
	time = 0;
	[self refresh];
}

-(void) bitLarger
{
	float scalee = 1.25;
	if (_isiPhone) {
        scalee *= (float)SCALE_MIN_IPHONE;
    }
	id larger = [CCScaleTo actionWithDuration:0.2f scale:scalee];	
	[self runAction:larger];
}

-(void) bitSmaller
{
	float scalee = 1;
	if (_isiPhone) {
        scalee *= (float)SCALE_MIN_IPHONE;
    }
	id smaller = [CCScaleTo actionWithDuration:0.2f scale:scalee];
	[self runAction:smaller];
}

-(void) setTime:(ccTime)value 
{
	if (_isClassic) {
        return;
    }

	if ((int)time != (int)value) {
		time = (int)value;
		if (time == 20.0f) {
			CCSprite *tmp = [[CCSprite alloc] initWithFile:@"play_count_number_red@2x.png"];
			CCSprite *tmp1 = (CCSprite*)[self getChildByTag:enum_tag_digit];
			[tmp1 setTexture:tmp.texture];
			[tmp1 setTextureRect:tmp.textureRect];
			CCSprite *tmp2 = (CCSprite*)[self getChildByTag:enum_tag_digit + 1];
			[tmp2 setTexture:tmp.texture];
			[tmp2 setTextureRect:tmp.textureRect];
			CCSprite *tmp3 = (CCSprite*)[self getChildByTag:enum_tag_digit + 2];
			[tmp3 setTexture:tmp.texture];
			[tmp3 setTextureRect:tmp.textureRect];
			CCSprite *tmp4 = (CCSprite*)[self getChildByTag:enum_tag_digit + 3];
			[tmp4 setTexture:tmp.texture];
			[tmp4 setTextureRect:tmp.textureRect];
            [tmp release];
		}
		[self refresh];
		if (time <= 30.0) {
			if (time <= 5.0f) {
                [GameSound effectBeep];
            }
			[self runAction:[CCSequence actions:
							 [CCCallFunc actionWithTarget:self selector:@selector(bitLarger)],
							 [CCDelayTime actionWithDuration:.2f],
							 [CCCallFunc actionWithTarget:self selector:@selector(bitSmaller)],
							 nil]];
		}
	}
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
    [GameSound unloadBeep];
    
    [_digit10 release];
    [_digit1 release];
    [_digitdot1 release];
    [_digitdot10 release];
	[super dealloc];
}
@end

//
//  OptionScene.h
//  Ice_Cracker
//
//  Created by mac on 5/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

enum {
	enum_score_none = 0,
	enum_score_reply,
	enum_score_exit,
};

@interface ScoreSprite : CCSprite {
    CCSprite *_btnReplaySprite;
    CCSprite *_btnExitSprite;
    CCSprite *_scorePanelSprite;
    CCSprite *_scoreLabelSprite;
    CCSprite *_scoreNum[5];
    CCSprite *_star[5];
    
	BOOL _isPressed;
	int _btnSelected;
	int _score;
	BOOL _isiPhone;
	float _appearTime;
}

+(id) sharedScoreSprite;

-(void) touchesBegan:(int)x Y:(int)y;
-(void) touchesMoved:(int)x Y:(int)y;
-(int) touchesEnded:(int)x Y:(int)y;

-(void) setScore:(int)value;
-(void) refresh;

-(void) appearSprite:(float)duration;
-(void) disappearSprite:(float)duration;
@end

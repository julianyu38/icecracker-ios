//
//  OptionScene.m
//  Ice_Cracker
//
//  Created by mac on 5/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ScoreSprite.h"
#import "GameData.h"
#import "GameSound.h"
#import "IceSprite.h"

@implementation ScoreSprite

#define BTN_REPLAY_X			6
#define BTN_REPLAY_Y			-35
#define BTN_REPLAY_WIDTH		182
#define BTN_REPLAY_HEIGHT		30

#define BTN_EXIT_X              6
#define BTN_EXIT_Y              -83
#define BTN_EXIT_WIDTH          182
#define BTN_EXIT_HEIGHT         30

#define STAR_X                  -84
#define STAR_Y                  63
#define STAR_SPAN               45

#define SCORE_PANEL_X			6
#define SCORE_PANEL_Y			15

#define SCORE_LABEL_X			-57
#define SCORE_LABEL_Y			SCORE_PANEL_Y

#define SCORE_NUMBER_X          8
#define SCORE_NUMBER_Y          SCORE_PANEL_Y
#define SCORE_NUMBER_SPAN		25

#define MAX_SCORE_NUM           5
#define MAX_STAR_NUM            5

#define PRESS_OPACITY           255
#define RELEASE_OPACITY         200

enum {
	enum_tag_btn_replay = 0,
	enum_tag_btn_exit,
	enum_tag_score_panel,
	enum_tag_score_label,
	enum_tag_score_num = 10,
	enum_tag_star = 20,
};

enum {
	enum_click_none = 0,
	enum_click_replay,
	enum_click_exit,
};

+(id) sharedScoreSprite
{
	ScoreSprite *sprite = [ScoreSprite node];

	// return the scene
	return sprite;
}

-(id) init
{
	if( (self=[super init] )) {
		self.visible = FALSE;
		
		_score = 0;
		_isPressed = NO;
		_isiPhone = [GameData isiPhone];

		CCSprite *tmp = [[CCSprite alloc] initWithFile:@"panel_background@2x.png"];
		[self setTexture:tmp.texture];
        [self setTextureRect:tmp.textureRect];
        [tmp release];

		_btnReplaySprite = [[CCSprite alloc] initWithFile:@"result_menu_replay@2x.png"];
        _btnReplaySprite.position = ccp(self.contentSize.width / 2 + (float)SCALE_MIN_IPAD * BTN_REPLAY_X, self.contentSize.height / 2 + (float)SCALE_MIN_IPAD * BTN_REPLAY_Y);
		[self addChild:_btnReplaySprite z:enum_tag_btn_replay tag:enum_tag_btn_replay];
		
		_btnExitSprite = [[CCSprite alloc] initWithFile:@"result_menu_quit@2x.png"];
        _btnExitSprite.position = ccp(self.contentSize.width / 2 + (float)SCALE_MIN_IPAD * BTN_EXIT_X, self.contentSize.height / 2 + (float)SCALE_MIN_IPAD * BTN_EXIT_Y);
		[self addChild:_btnExitSprite z:enum_tag_btn_exit tag:enum_tag_btn_exit];
		
		_scorePanelSprite = [[CCSprite alloc] initWithFile:@"result_view@2x.png"];
        _scorePanelSprite.position = ccp(self.contentSize.width / 2 + (float)SCALE_MIN_IPAD * SCORE_PANEL_X, self.contentSize.height / 2 + (float)SCALE_MIN_IPAD * SCORE_PANEL_Y);
		[self addChild:_scorePanelSprite z:enum_tag_score_panel tag:enum_tag_score_panel];
		
		_scoreLabelSprite = [[CCSprite alloc] initWithFile:@"result_score@2x.png"];
        _scoreLabelSprite.position = ccp(self.contentSize.width / 2 + (float)SCALE_MIN_IPAD * SCORE_LABEL_X, self.contentSize.height / 2 + (float)SCALE_MIN_IPAD * SCORE_LABEL_Y);
		[self addChild:_scoreLabelSprite z:enum_tag_score_label tag:enum_tag_score_label];
		
		for (int i = 0; i < MAX_SCORE_NUM; i++) {
			_scoreNum[i] = [[CCSprite alloc] initWithFile:@"result_score_number@2x.png"];
			[_scoreNum[i] setTextureRect:CGRectMake(0, 0, _scoreNum[i].contentSize.width / 10, _scoreNum[i].contentSize.height)];
            _scoreNum[i].position = ccp(self.contentSize.width / 2 + ((float)SCALE_MIN_IPAD * (SCORE_NUMBER_X + i * SCORE_NUMBER_SPAN)), self.contentSize.height / 2 + (float)SCALE_MIN_IPAD * SCORE_NUMBER_Y);
			_scoreNum[i].opacity = 0;
			[self addChild:_scoreNum[i] z:enum_tag_score_num + i tag:enum_tag_score_num + i];
		}
		
		for (int i = 0; i < MAX_STAR_NUM; i++) {
			_star[i] = [[CCSprite alloc] initWithFile:@"result_ranking_off@2x.png"];
            _star[i].position = ccp(self.contentSize.width / 2 + (float)SCALE_MIN_IPAD * (STAR_X + i * STAR_SPAN), self.contentSize.height / 2 + (float)SCALE_MIN_IPAD * STAR_Y);
			_star[i].opacity = 0;
			[self addChild:_star[i] z:enum_tag_star + i tag:enum_tag_star + i];
		}
		
		self.opacity = 0;
		_btnReplaySprite.opacity = 0;
		_btnExitSprite.opacity = 0;
		_scorePanelSprite.opacity = 0;
        _scorePanelSprite.visible = NO;
		_scoreLabelSprite.opacity = 0;
		[self refresh];
	}
	return self;
}

-(int) getClick:(int)x Y:(int)y
{
	int dx, dy;
	dx = x - BTN_REPLAY_X;
	dy = y - BTN_REPLAY_Y;
	if (-BTN_REPLAY_WIDTH / 2 < dx && dx < BTN_REPLAY_WIDTH / 2 && -BTN_REPLAY_HEIGHT / 2 < dy && dy < BTN_REPLAY_HEIGHT / 2) {
        return enum_click_replay;
    }
	dx = x - BTN_EXIT_X;
	dy = y - BTN_EXIT_Y;
	if (-BTN_EXIT_WIDTH / 2 < dx && dx < BTN_EXIT_WIDTH / 2 && -BTN_EXIT_HEIGHT / 2 < dy && dy < BTN_EXIT_HEIGHT / 2) {
        return enum_click_exit;
    }
	
	return enum_click_none;
}

-(void) touchesBegan:(int)x Y:(int)y
{
	x -= self.position.x;
	y -= self.position.y;
	if (!_isiPhone) {
		x = (float)x / SCALE_MIN_IPAD;
		y = (float)y / SCALE_MIN_IPAD;
	}
	
	int touchClick = [self getClick:x Y:y];
	
	if (touchClick == enum_click_none) {
        return;
    }
	
	[GameSound effectBtnClick];
	_btnSelected = touchClick;
	switch (_btnSelected) {
		case enum_click_replay:	
            ((CCSprite*)[self getChildByTag:enum_tag_btn_replay]).opacity = PRESS_OPACITY;	
            break;
		case enum_click_exit:		
            ((CCSprite*)[self getChildByTag:enum_tag_btn_exit]).opacity = PRESS_OPACITY;	
            break;
		default:			
            break;
	}
	_isPressed = YES;
}

-(void) touchesMoved:(int)x Y:(int)y
{
	if (!_isPressed) {
        return;
    }

	x -= self.position.x;
	y -= self.position.y;
	if (!_isiPhone) {
		x = (float)x / SCALE_MIN_IPAD;
		y = (float)y / SCALE_MIN_IPAD;
	}
	
	int touchClick = [self getClick:x Y:y];
	
	if (touchClick == _btnSelected) {
        return;
    }
	
	if (touchClick != enum_click_none) {
        [GameSound effectBtnClick];
    }
	switch (_btnSelected) {
		case enum_click_replay:	
            ((CCSprite*)[self getChildByTag:enum_tag_btn_replay]).opacity = RELEASE_OPACITY;	
            break;
		case enum_click_exit:		
            ((CCSprite*)[self getChildByTag:enum_tag_btn_exit]).opacity = RELEASE_OPACITY;		
            break;
		default:			
            break;
	}
	_btnSelected = touchClick;
	switch (_btnSelected) {
		case enum_click_replay:	
            ((CCSprite*)[self getChildByTag:enum_tag_btn_replay]).opacity = PRESS_OPACITY;	
            break;
		case enum_click_exit:		
            ((CCSprite*)[self getChildByTag:enum_tag_btn_exit]).opacity = PRESS_OPACITY;		
            break;
		default:			
            break;
	}	
}

-(int) touchesEnded:(int)x Y:(int)y
{
	if (!_isPressed) {
        return enum_score_none;
    }

	_isPressed = NO;
	x -= self.position.x;
	y -= self.position.y;
	if (!_isiPhone) {
		x = (float)x / SCALE_MIN_IPAD;
		y = (float)y / SCALE_MIN_IPAD;
	}
	
	int touchClick = [self getClick:x Y:y];
	
	switch (touchClick) {
		case enum_click_replay:	
            ((CCSprite*)[self getChildByTag:enum_tag_btn_replay]).opacity = RELEASE_OPACITY;	
            return enum_score_reply;
		case enum_click_exit:
            ((CCSprite*)[self getChildByTag:enum_tag_btn_exit]).opacity = RELEASE_OPACITY;		
            return enum_score_exit;
		default:			
            break;
	}
	return enum_score_none;
}


-(void) setScore:(int)value
{
	_score = value;
	[self refresh];
}

-(void) refresh
{
	int starnum;
    
    if (_score != 0) {
        if (_score < 1000) {
            starnum = 1;
        }
        else if (_score >= 1000 && _score < 3000 ) {
            starnum = 2;
        }
        else if (_score >= 3000 && _score < 5000 ) {
            starnum = 3;
        }
        else if (_score >= 5000 && _score < 10000 ) {
            starnum = 4;
        }
        else if (_score >= 10000) {
            starnum = 5;
        }
    }
    else {
        starnum = 0;
    }

	CCSprite *tmp;
	CCSprite *tmpref = [[CCSprite alloc] initWithFile:@"result_ranking_on@2x.png"];
	for (int i = 0; i < starnum; i++) {
		tmp = (CCSprite*)[self getChildByTag:enum_tag_star + i];
		[tmp setTexture:tmpref.texture];
		[tmp setTextureRect:tmpref.textureRect];
	}
	CCSprite *tmpref1 = [[CCSprite alloc] initWithFile:@"result_ranking_off@2x.png"];
	for (int i = starnum; i < MAX_STAR_NUM; i++) {
		tmp = (CCSprite*)[self getChildByTag:enum_tag_star + i];
		[tmp setTexture:tmpref1.texture];
		[tmp setTextureRect:tmpref1.textureRect];
	}
	[tmpref release];
    [tmpref1 release];
    
	int num;
	CCSprite *tmp1 = (CCSprite*)[self getChildByTag:enum_tag_score_num];
	CCSprite *tmp2 = (CCSprite*)[self getChildByTag:enum_tag_score_num + 1];
	CCSprite *tmp3 = (CCSprite*)[self getChildByTag:enum_tag_score_num + 2];
	CCSprite *tmp4 = (CCSprite*)[self getChildByTag:enum_tag_score_num + 3];
	CCSprite *tmp5 = (CCSprite*)[self getChildByTag:enum_tag_score_num + 4];

	int nwidth, nheight;
    nwidth = 26 * (int)SCALE_MIN_IPAD;
    nheight = 28 * (int)SCALE_MIN_IPAD;

	tmp1.visible = FALSE;
	tmp2.visible = FALSE;
	tmp3.visible = FALSE;
	tmp4.visible = FALSE;
	tmp5.visible = FALSE;
	if (_score >= 10000) {
		num = _score / 10000;
		[tmp1 setTextureRect:CGRectMake(num * nwidth, 0, nwidth, nheight)];
		tmp1.visible = TRUE;
		num = (_score / 1000) % 10;
		[tmp2 setTextureRect:CGRectMake(num * nwidth, 0, nwidth, nheight)];
		tmp2.visible = TRUE;
		num = (_score / 100) % 10;
		[tmp3 setTextureRect:CGRectMake(num * nwidth, 0, nwidth, nheight)];
		tmp3.visible = TRUE;
		num = (_score / 10) % 10;
		[tmp4 setTextureRect:CGRectMake(num * nwidth, 0, nwidth, nheight)];
		tmp4.visible = TRUE;
		num = _score % 10;
		[tmp5 setTextureRect:CGRectMake(num * nwidth, 0, nwidth, nheight)];
		tmp5.visible = TRUE;	
	}
	else if (_score >= 1000) {
		num = _score / 1000;
		[tmp1 setTextureRect:CGRectMake(num * nwidth, 0, nwidth, nheight)];
		tmp1.visible = TRUE;
		num = (_score / 100) % 10;
		[tmp2 setTextureRect:CGRectMake(num * nwidth, 0, nwidth, nheight)];
		tmp2.visible = TRUE;
		num = (_score / 10) % 10;
		[tmp3 setTextureRect:CGRectMake(num * nwidth, 0, nwidth, nheight)];
		tmp3.visible = TRUE;
		num = _score % 10;
		[tmp4 setTextureRect:CGRectMake(num * nwidth, 0, nwidth, nheight)];
		tmp4.visible = TRUE;
	}
	else if (_score >= 100) {
		num = _score / 100;
		[tmp1 setTextureRect:CGRectMake(num * nwidth, 0, nwidth, nheight)];
		tmp1.visible = TRUE;
		num = (_score / 10) % 10;
		[tmp2 setTextureRect:CGRectMake(num * nwidth, 0, nwidth, nheight)];
		tmp2.visible = TRUE;
		num = _score % 10;
		[tmp3 setTextureRect:CGRectMake(num * nwidth, 0, nwidth, nheight)];
		tmp3.visible = TRUE;
	}
	else if (_score >= 10) {
		num = _score / 10;
		[tmp1 setTextureRect:CGRectMake(num * nwidth, 0, nwidth, nheight)];
		tmp1.visible = TRUE;
		num = _score % 10;
		[tmp2 setTextureRect:CGRectMake(num * nwidth, 0, nwidth, nheight)];
		tmp2.visible = TRUE;
	}
	else if (_score >= 0) {
		num = _score % 10;
		[tmp1 setTextureRect:CGRectMake(num * nwidth, 0, nwidth, nheight)];
		tmp1.visible = TRUE;
	}
	
}
-(void) appearSelf
{
	[self runAction:[CCFadeTo actionWithDuration:_appearTime opacity:PRESS_OPACITY]];
}

-(void) appearReplay
{
	[((CCSprite*)[self getChildByTag:enum_tag_btn_replay]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:RELEASE_OPACITY]];	
}

-(void) appearExit
{
	[((CCSprite*)[self getChildByTag:enum_tag_btn_exit]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:RELEASE_OPACITY]];	
}

-(void) appearPanel
{
	[((CCSprite*)[self getChildByTag:enum_tag_score_panel]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:PRESS_OPACITY]];	
}

-(void) appearLabel
{
	[((CCSprite*)[self getChildByTag:enum_tag_score_label]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:PRESS_OPACITY]];	
}

-(void) appearStar1
{
	[((CCSprite*)[self getChildByTag:enum_tag_star]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:PRESS_OPACITY]];
}

-(void) appearStar2
{
	[((CCSprite*)[self getChildByTag:enum_tag_star + 1]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:PRESS_OPACITY]];
}

-(void) appearStar3
{
	[((CCSprite*)[self getChildByTag:enum_tag_star + 2]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:PRESS_OPACITY]];
}

-(void) appearStar4
{
	[((CCSprite*)[self getChildByTag:enum_tag_star + 3]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:PRESS_OPACITY]];
}

-(void) appearStar5
{
	[((CCSprite*)[self getChildByTag:enum_tag_star + 4]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:PRESS_OPACITY]];
}

-(void) appearNum1
{
	[((CCSprite*)[self getChildByTag:enum_tag_score_num]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:PRESS_OPACITY]];
}

-(void) appearNum2
{
	[((CCSprite*)[self getChildByTag:enum_tag_score_num + 1]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:PRESS_OPACITY]];
}

-(void) appearNum3
{
	[((CCSprite*)[self getChildByTag:enum_tag_score_num + 2]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:PRESS_OPACITY]];
}

-(void) appearNum4
{
	[((CCSprite*)[self getChildByTag:enum_tag_score_num + 3]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:PRESS_OPACITY]];
}

-(void) appearNum5
{
	[((CCSprite*)[self getChildByTag:enum_tag_score_num + 4]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:PRESS_OPACITY]];
}

-(void) appearSprite:(float)duration
{
	_appearTime = duration;
	[self runAction:[CCSequence actions:
					 [CCCallFunc actionWithTarget:self selector:@selector(appearSelf)],
					 [CCCallFunc actionWithTarget:self selector:@selector(appearLabel)],
					 [CCCallFunc actionWithTarget:self selector:@selector(appearPanel)],
					 [CCCallFunc actionWithTarget:self selector:@selector(appearReplay)],
					 [CCCallFunc actionWithTarget:self selector:@selector(appearExit)],
					 [CCCallFunc actionWithTarget:self selector:@selector(appearPanel)],
					 [CCCallFunc actionWithTarget:self selector:@selector(appearLabel)],
					 [CCCallFunc actionWithTarget:self selector:@selector(appearStar1)],
					 [CCCallFunc actionWithTarget:self selector:@selector(appearStar2)],
					 [CCCallFunc actionWithTarget:self selector:@selector(appearStar3)],
					 [CCCallFunc actionWithTarget:self selector:@selector(appearStar4)],
					 [CCCallFunc actionWithTarget:self selector:@selector(appearStar5)],
					 [CCCallFunc actionWithTarget:self selector:@selector(appearNum1)],
					 [CCCallFunc actionWithTarget:self selector:@selector(appearNum2)],
					 [CCCallFunc actionWithTarget:self selector:@selector(appearNum3)],
					 [CCCallFunc actionWithTarget:self selector:@selector(appearNum4)],
					 [CCCallFunc actionWithTarget:self selector:@selector(appearNum5)],
					 nil]];
}

-(void) disappearSelf
{
	[self runAction:[CCFadeTo actionWithDuration:_appearTime opacity:0]];
}

-(void) disappearReplay
{
	[((CCSprite*)[self getChildByTag:enum_tag_btn_replay]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:0]];	
}

-(void) disappearExit
{
	[((CCSprite*)[self getChildByTag:enum_tag_btn_exit]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:0]];	
}

-(void) disappearPanel
{
	[((CCSprite*)[self getChildByTag:enum_tag_score_panel]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:0]];	
}

-(void) disappearLabel
{
	[((CCSprite*)[self getChildByTag:enum_tag_score_label]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:0]];	
}

-(void) disappearStar1
{
	[((CCSprite*)[self getChildByTag:enum_tag_star]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:0]];
}

-(void) disappearStar2
{
	[((CCSprite*)[self getChildByTag:enum_tag_star + 1]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:0]];
}

-(void) disappearStar3
{
	[((CCSprite*)[self getChildByTag:enum_tag_star + 2]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:0]];
}

-(void) disappearStar4
{
	[((CCSprite*)[self getChildByTag:enum_tag_star + 3]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:0]];
}

-(void) disappearStar5
{
	[((CCSprite*)[self getChildByTag:enum_tag_star + 4]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:0]];
}

-(void) disappearNum1
{
	[((CCSprite*)[self getChildByTag:enum_tag_score_num]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:0]];
}

-(void) disappearNum2
{
	[((CCSprite*)[self getChildByTag:enum_tag_score_num + 1]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:0]];
}

-(void) disappearNum3
{
	[((CCSprite*)[self getChildByTag:enum_tag_score_num + 2]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:0]];
}

-(void) disappearNum4
{
	[((CCSprite*)[self getChildByTag:enum_tag_score_num + 3]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:0]];
}

-(void) disappearNum5
{
	[((CCSprite*)[self getChildByTag:enum_tag_score_num + 4]) runAction:[CCFadeTo actionWithDuration:_appearTime opacity:0]];
}

-(void) disappearSprite:(float)duration
{
	_appearTime = duration;
	[self runAction:[CCSequence actions:
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearStar1)],
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearStar2)],
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearStar3)],
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearStar4)],
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearStar5)],
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearNum1)],
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearNum2)],
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearNum3)],
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearNum4)],
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearNum5)],
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearReplay)],
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearExit)],
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearPanel)],
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearLabel)],
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearSelf)],
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearPanel)],
					 [CCCallFunc actionWithTarget:self selector:@selector(disappearLabel)],
					 nil]];
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
    [GameSound unloadBtnClick];

    [_btnReplaySprite release];
    [_btnExitSprite release];
    [_scorePanelSprite release];
    [_scoreLabelSprite release];
    for (int i = 0; i < MAX_SCORE_NUM; i++) {
        [_scoreNum[i] release];
    }
    for (int i = 0; i < MAX_STAR_NUM; i++) {
        [_star[i] release];
    }
	[super dealloc];
}
@end

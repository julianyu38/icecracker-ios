//
//  main.m
//  Ice_Cracker
//
//  Created by mac on 7/23/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
	NSAutoreleasePool *pool = [NSAutoreleasePool new];
	int retVal = UIApplicationMain(argc, argv, nil, @"Ice_CrackerAppDelegate");
	[pool release];
	return retVal;
}
